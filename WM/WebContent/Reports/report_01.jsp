<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@page import="myData.Profile"%>

<h2 style='color: white;'><U>1.	Today's watchers</U></h2>
<br>
		<% 
			List temp = null;
			Profile p = null;
			int total = myData.CommonFunctions.totalProfiles();
			temp = myData.CommonFunctions.reportTodaysWatchers();

			out.println("<h3 align='center' style='color: white'>There are "+temp.size()+" ("+(temp.size()*100/total)+"%) watchers out of "+total+" today!</h3>");
			
			if (temp.size()>0 && false) // false blocks table showing
			{ 
				%>
				<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
					<thead style="background: gray; color: white">
					<tr>
					<td align='center'><h3>&nbsp;#&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Email&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Gender&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Age&nbsp;</h3></td>
					</tr>
				</thead>		
				<%
				for(int i=0;i<temp.size();i++){
				p = (Profile)temp.get(i);
				out.println("<tr style='color: white' align='center'><td><b>&nbsp;"+(i+1)+"&nbsp;</b></td><td align='left'>"+p.getProfileId()+"</td><td>"+p.getGender()+"</td><td>"+p.getAge()+"</td></tr>");
				}	
				out.println("</table>");
			} 
			%>
