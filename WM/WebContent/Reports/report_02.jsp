<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@page import="myData.ComplexQuery"%>

<br>
<h2 style='color: white;'><U>2.	Participants with all the genres in future interest</U></h2>
<br> 
		<% 
			List temp = null;
			ComplexQuery cq = null;
			temp = myData.CommonFunctions.abusiveTotals();

			int total = myData.CommonFunctions.totalProfiles();
			int totalInfuture = myData.CommonFunctions. totalUsers_in_future();

			out.println("<h3 align='center' style='color: white'>There are "+totalInfuture+" out of "+total+" participants ("+(totalInfuture*100/total)+"%) with all the genres in future interest</h3>");
			
			if (temp.size()>0)
			{ 
				%>
				<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
					<thead style="background: gray; color: white">
					<tr>
					<td align='center'><h3>&nbsp;No of users&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;No of times transfered to future &nbsp;</h3></td>
					</tr>
				</thead>		
				<%
				for(int i=0;i<temp.size();i++){
				cq = (ComplexQuery)temp.get(i);
				out.println("<tr style='color: white' align='center'><td>&nbsp;"+cq.getInt_field_1() +"&nbsp;</td><td>"+cq.getInt_field_0()+"</td></tr>");
				}	
				out.println("</table>");
			} 
			%>
