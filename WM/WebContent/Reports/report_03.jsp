<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import="myData.Profile"%>


<h2 style='color: white;'><U>3.	Non active users</U></h2>
<br>
<br>
		<% 
		
		List temp = null;
		Profile p = null;

		int total= myData.CommonFunctions.totalProfiles();
		
		
		temp = myData.CommonFunctions.allProfiles();

		int[] tableAll = new int[10];
		int totalNo =  temp.size();
	
		for(int i=0;i<totalNo;i++){
			p = (Profile)temp.get(i);
			     if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==1) tableAll[0]++;
			else if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==2) tableAll[1]++;
			else if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==3) tableAll[2]++;
			else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==1) tableAll[3]++;
			else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==2) tableAll[4]++;
			else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==3) tableAll[5]++;
			else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==1) tableAll[6]++;
			else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==2) tableAll[7]++;
			else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==3) tableAll[8]++;
			else tableAll[9]++;
		}

		
		
		
		
		
		temp = myData.CommonFunctions.reportNotVisitingUsers();
		out.println("<h2 align='center' style='color: white'>There are "+temp.size()+" users out of "+total+"("+(temp.size()*100/total)+"%) that didn't enter </h2>");
		out.println("<h2 align='center' style='color: white'>the system more than 3 days</h2>");
		out.println("<h4 align='center' style='color: white'>Table: Percentage of not visiting users in each group</h4>");
		
		
		
		if (temp.size()>0){
			
			int[] table = new int[10];
			totalNo =  temp.size();
		
			for(int i=0;i<totalNo;i++){
				p = (Profile)temp.get(i);
				     if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==1) table[0]++;
				else if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==2) table[1]++;
				else if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==3) table[2]++;
				else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==1) table[3]++;
				else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==2) table[4]++;
				else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==3) table[5]++;
				else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==1) table[6]++;
				else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==2) table[7]++;
				else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==3) table[8]++;
				else table[9]++;
			}
			 
			%>
			<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
				<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;*&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Dynamic&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Stable&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Indifferent&nbsp;</h3></td>
			
				</tr>
			</thead>
				<tr style='color: white' align='center'><td>Group 0 - static</td>		 <td><%=table[0] %>(<%=table[0]*100/tableAll[0] %>%)</td><td><%=table[1] %>(<%=table[1]*100/tableAll[1] %>%)</td><td><%=table[2] %>(<%=table[2]*100/tableAll[2] %>%)</td></tr>
				<tr style='color: white' align='center'><td>Group 1 - single genre</td>	 <td><%=table[3] %>(<%=table[3]*100/tableAll[3] %>%)</td><td><%=table[4] %>(<%=table[4]*100/tableAll[4] %>%)</td><td><%=table[5] %>(<%=table[5]*100/tableAll[5] %>%)</td></tr>
				<tr style='color: white' align='center'><td>Group 2 - multiple genre</td><td><%=table[6] %>(<%=table[6]*100/tableAll[6] %>%)</td><td><%=table[7] %>(<%=table[7]*100/tableAll[7] %>%)</td><td><%=table[8] %>(<%=table[8]*100/tableAll[8] %>%)</td></tr>
				<tr style='color: white' align='center'><td>Others (with undefined strategy or group)</td><td colspan=5> <%=table[9] %>(<%=table[9]*100/tableAll[9] %>%)</td></tr>
			</table>
			<br><br>

			<% 
			
			int[] arr = new int[6];
			for(int i=0;i<temp.size();i++){
				p = (Profile)temp.get(i);
				
	 				 if (p.getCurrentStrategyId()<1) arr[5]++;
				else if (p.getDaysMissed()>13)  arr[4]++;
				else  switch (p.getDaysMissed()){
						case  3:
						case  4: arr[0]++; break;
						case  5:
						case  6: 
						case  7: arr[1]++; break;
						case  8: 
						case  9: 
						case 10: arr[2]++; break;
						case 11: 
						case 12: 
						case 13: arr[3]++; break;

					};
				}
			
			%>

			<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
				<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;Days&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Users number&nbsp;</h3></td>
				</tr>
			</thead>
				<tr style='color: white' align='center'><td>&nbsp;&nbsp;3-4</td><td><%=arr[0] %></td></tr>
				<tr style='color: white' align='center'><td>&nbsp;&nbsp;5-7</td><td><%=arr[1] %></td></tr>
				<tr style='color: white' align='center'><td>&nbsp;8-10</td><td><%=arr[2] %></td></tr>
				<tr style='color: white' align='center'><td>11-13</td><td><%=arr[3] %></td></tr>
				<tr style='color: white' align='center'><td>14+ (didn't finish Shalit Questionnaire)</td><td><%=arr[4] %>(<%=arr[5] %>)</td></tr>
				<tr style='color: white;background-color: gray;' align='center'><td>Total:</td><td><%=temp.size() %></td></tr>
			</table>
			<br><br>

			<% if (false) { %>
	  		<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
				<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;#&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Email&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Amount of missing days&nbsp;</h3></td>
				</tr>
			</thead>
			<%
			for(int i=0;i<temp.size();i++){
			p = (Profile)temp.get(i);
			out.println("<tr style='color: white' align='center'><td>"+(i+1)+"</td><td>"+p.getProfileId()+"</td><td>"+p.getDaysMissed()+"</td></tr>");
			} 
			out.println("</table>");
			}
			
			
		}
		%>
