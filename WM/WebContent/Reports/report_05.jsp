<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@page import="myData.Profile"%>

<br>
<br>
<h2 style='color: white;'><U>5.	Users per strategy and group</U></h2>

		<% 
		List temp = null;
		Profile p = null;
		
		temp = myData.CommonFunctions.allProfiles();
		out.println("<h2 align='center' style='color: white'>There are "+temp.size()+" registered users!</h2>");
		out.println("<h4 align='center' style='color: white'>Table: Number of users in each group and percentage from total number of participants.</h4>");
		
		if (temp.size()>0){
			
			int[] table = new int[10];
			int totalNo =  temp.size();
		
			for(int i=0;i<totalNo;i++){
				p = (Profile)temp.get(i);
				     if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==1) table[0]++;
				else if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==2) table[1]++;
				else if (p.getExperimentGroupId()==0 && p.getCurrentStrategyId()==3) table[2]++;
				else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==1) table[3]++;
				else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==2) table[4]++;
				else if (p.getExperimentGroupId()==1 && p.getCurrentStrategyId()==3) table[5]++;
				else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==1) table[6]++;
				else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==2) table[7]++;
				else if (p.getExperimentGroupId()==2 && p.getCurrentStrategyId()==3) table[8]++;
				else table[9]++;
			}
			%>
			<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
				<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;*&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Dynamic&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Stable&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Indifferent&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Total&nbsp;</h3></td>
				</tr>
			</thead>
				<tr style='color: white' align='center'><td>Group 0 - static</td>		<td><%=table[0] %>(<%=table[0]*100/totalNo %>%)</td><td><%=table[1] %>(<%=table[1]*100/totalNo %>%)</td><td><%=table[2] %>(<%=table[2]*100/totalNo %>%)</td><td><%=table[0]+table[1]+table[2] %>(<%=(table[0]+table[1]+table[2])*100/totalNo %>%)</td></tr>
				<tr style='color: white' align='center'><td>Group 1 - single genre</td>	<td><%=table[3] %>(<%=table[3]*100/totalNo %>%)</td><td><%=table[4] %>(<%=table[4]*100/totalNo %>%)</td><td><%=table[5] %>(<%=table[5]*100/totalNo %>%)</td><td><%=table[3]+table[4]+table[5] %>(<%=(table[3]+table[4]+table[5])*100/totalNo %>%)</td></tr>
				<tr style='color: white' align='center'><td>Group 2 - multiple genre</td><td><%=table[6] %>(<%=table[6]*100/totalNo %>%)</td><td><%=table[7] %>(<%=table[7]*100/totalNo %>%)</td><td><%=table[8] %>(<%=table[8]*100/totalNo %>%)</td><td><%=table[6]+table[7]+table[8] %>(<%=(table[6]+table[7]+table[8])*100/totalNo %>%)</td></tr>
				<tr style='color: white' align='center'><td>Total:</td><td><%=table[0]+table[3]+table[6] %>(<%=(table[0]+table[3]+table[6])*100/totalNo %>%)</td><td><%=table[1]+table[4]+table[7] %>(<%=(table[1]+table[4]+table[7])*100/totalNo %>%)</td><td><%=table[2]+table[5]+table[8] %>(<%=(table[2]+table[5]+table[8])*100/totalNo %>%)</td><td><%=temp.size()-table[9] %>(<%=(temp.size()-table[9])*100/totalNo %>%)</td></tr>
				<tr style='color: white' align='center'><td>Others (with undefined strategy or group)</td><td colspan=3> <%=table[9] %>(<%=table[9]*100/totalNo %>%)</td><td><%=totalNo %>(100%)</td></tr>
		</table>
		<%} %>
