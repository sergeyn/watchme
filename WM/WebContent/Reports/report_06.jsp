<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>

<br>
<br>
<h2 style='color: white;'><U>6. Active users per specific day</U></h2>

		<% 
			List temp = null;

			int total= myData.CommonFunctions.totalProfiles();
			String date = request.getParameter("date").trim();

			
			temp = myData.CommonFunctions.reportActiveUsersPerDay(date);

//			temp= myData.CommonFunctions.allProfiles();
			out.println("<h3 align='center' style='color: white'>There were "+temp.size()+" ("+(temp.size()*100/total)+"%) active users out of "+total+" on "+date+"!</h3>");
			
			if (temp.size()>0 && false)
			{ 
				%>
				<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
					<thead style="background: gray; color: white">
					<tr>
					<td align='center'><h3>&nbsp;#&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Email&nbsp;</h3></td>
					</tr>
				</thead>		
				<%
				for(int i=0;i<temp.size();i++)
					out.println("<tr style='color: white' align='center'><td><b>&nbsp;"+(i+1)+"&nbsp;</b></td><td align='left'>"+(String)temp.get(i)+"</td></tr>");
				out.println("</table>");
			} 
			%>
