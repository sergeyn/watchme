<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "myData.Profile" %>

<br>
<br>
<h2 style='color: white;'><U>7. Inactive users per specific day</U></h2>

		<% 
			List temp = null;
			int total= myData.CommonFunctions.totalProfiles();
			String date = request.getParameter("date").trim();
			temp = myData.CommonFunctions.getBadUsersPerDay(date);
			out.println("<h3 align='center' style='color: white'>There were "+temp.size()+" ("+(temp.size()*100/total)+"%) inactive users of "+total+" on "+date+"!</h3>");
			
			if (temp.size()>0 && false)
			{ 
				%>
				<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
					<thead style="background: gray; color: white">
					<tr>
					<td align='center'><h3>&nbsp;#&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Email&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Missed days until today &nbsp;</h3></td>
					</tr>
				</thead>		
				<%
				Profile p = null;
				for(int i=0;i<temp.size();i++){
					p = (Profile)temp.get(i);
					out.println("<tr style='color: white' align='center'><td>"+(i+1)+"</td><td>"+p.getProfileId()+"</td><td>"+p.getDaysMissed()+"</td></tr>");
				}
				out.println("</table>");
			} 
			%>
