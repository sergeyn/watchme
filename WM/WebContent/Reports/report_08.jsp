<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import="myData.ComplexQuery"%>
<br>
<br>
<% 
int wNo = 17;
try{wNo=Integer.valueOf(request.getParameter("weekNo"));}
catch(Exception e){}
%>

<h2 style='color: white;'><U>8. Percentage of visiting users in a specific week per group</U></h2>
<br><h3 style='color: white;'><U>Week number <%=wNo-16 %></U></h3>

		<% 
		List temp = null;
		int totalNo= myData.CommonFunctions.totalProfiles();
		temp = myData.CommonFunctions.profileEntriesGSWeekly(wNo);
		
		if (temp.size()>0){
			int[] options = new int[10];
			String[] optNames = new String[10];
			optNames[0]="Strategy 1, Group 0";
			optNames[1]="Strategy 1, Group 1";
			optNames[2]="Strategy 1, Group 2";
			optNames[3]="Strategy 2, Group 0";
			optNames[4]="Strategy 2, Group 1";
			optNames[5]="Strategy 2, Group 2";
			optNames[6]="Strategy 3, Group 0";
			optNames[7]="Strategy 3, Group 1";
			optNames[8]="Strategy 3, Group 2";
			optNames[9]="Undefined!";
			
			int counter = 0;
			for(int i=0;i<temp.size();i++){
				ComplexQuery cc = (ComplexQuery)temp.get(i);
				
				if (cc.getInt_field_0()==0) continue;
				counter++;
		 			 if (cc.getInt_field_1()==1 && cc.getInt_field_2()==0) 	 options[0]++;
				else if (cc.getInt_field_1()==2 && cc.getInt_field_2()==0) 	 options[1]++;
				else if (cc.getInt_field_1()==3 && cc.getInt_field_2()==0) 	 options[2]++;
				else if (cc.getInt_field_1()==1 && cc.getInt_field_2()==1) 	 options[3]++;
				else if (cc.getInt_field_1()==2 && cc.getInt_field_2()==1)	 options[4]++;
				else if (cc.getInt_field_1()==3 && cc.getInt_field_2()==1) 	 options[5]++;
				else if (cc.getInt_field_1()==1 && cc.getInt_field_2()==2)	 options[6]++;
				else if (cc.getInt_field_1()==2 && cc.getInt_field_2()==2)	 options[7]++;
				else if (cc.getInt_field_1()==3 && cc.getInt_field_2()==2)	 options[8]++;
				else 														 options[9]++;
			}
			%>
			<h3 style='color: white;'>All users distribution</h3>
			<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
				<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;Visiting users&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Not visiting users&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Total&nbsp;</h3></td>
				</tr>
			</thead>
				<tr style='color: white' align='center'><td><%=(counter) %>(<%=(counter)*100/totalNo %>%)</td><td><%=(totalNo-counter) %>(<%=(totalNo-counter)*100/totalNo %>%)</td><td><%=totalNo %>(100%)</td></tr>
			</table>
			
			<%if (counter>0) {%>
			<br>
			<h3 style='color: white;'>Visiting users distribution</h3>
			<br>
			<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
				<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;*&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Dynamic&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Stable&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Indifferent&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Total&nbsp;</h3></td>
				</tr>
			</thead>
				<tr style='color: white' align='center'><td>Group 0 - static</td>		<td><%=options[0] %>(<%=options[0]*100/(options[0]+options[1]+options[2]) %>%)</td><td><%=options[1] %>(<%=options[1]*100/(options[0]+options[1]+options[2]) %>%)</td><td><%=options[2] %>(<%=options[2]*100/(options[0]+options[1]+options[2]) %>%)</td><td><%=options[0]+options[1]+options[2] %>(<%=(options[0]+options[1]+options[2])*100/(options[0]+options[1]+options[2]) %>%)</td></tr>
				<tr style='color: white' align='center'><td>Group 1 - single genre</td>	<td><%=options[3] %>(<%=options[3]*100/(options[3]+options[4]+options[5]) %>%)</td><td><%=options[4] %>(<%=options[4]*100/(options[3]+options[4]+options[5]) %>%)</td><td><%=options[5] %>(<%=options[5]*100/(options[3]+options[4]+options[5]) %>%)</td><td><%=options[3]+options[4]+options[5] %>(<%=(options[3]+options[4]+options[5])*100/(options[3]+options[4]+options[5]) %>%)</td></tr>
				<tr style='color: white' align='center'><td>Group 2 - multiple genre</td><td><%=options[6] %>(<%=options[6]*100/(options[6]+options[7]+options[8]) %>%)</td><td><%=options[7] %>(<%=options[7]*100/(options[6]+options[7]+options[8]) %>%)</td><td><%=options[8] %>(<%=options[8]*100/(options[6]+options[7]+options[8]) %>%)</td><td><%=options[6]+options[7]+options[8] %>(<%=(options[6]+options[7]+options[8])*100/(options[6]+options[7]+options[8]) %>%)</td></tr>
				<tr style='color: white' align='center'><td>Total:</td><td><%=options[0]+options[3]+options[6] %>(<%=(options[0]+options[3]+options[6])*100/counter %>%)</td><td><%=options[1]+options[4]+options[7] %>(<%=(options[1]+options[4]+options[7])*100/counter %>%)</td><td><%=options[2]+options[5]+options[8] %>(<%=(options[2]+options[5]+options[8])*100/counter %>%)</td><td><%=counter %>(100%)</td></tr>
			</table>
			
			<%
			}
		}
		
		%>
