<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@page import="myData.Profile"%>

<%!
public String getCompanyName(String CN){
	String name = "";
	int startPos = CN.indexOf("@")+1;
	int endPos = CN.indexOf(".", startPos);
	if (startPos==-1 || endPos==-1 || startPos>=endPos)
			return("UNKNOWN_EMAIL_FORMAT");
	name = CN.substring(startPos, endPos).toUpperCase();
	return name;
}
%>
<h2 style='color: white;'><U>9.	Total number of participants by company</U></h2>
<br>
<%
		List temp = myData.CommonFunctions.allProfiles();
	if (temp.size()==0){
		out.println("<h3 align='center' style='color: white'>No users found!</h3>");
	}else{
		int[] counters = new int[8];
		for(int i=0;i<temp.size();i++){
			String compName = getCompanyName(((Profile)temp.get(i)).getProfileId());
	 			 if (compName.equals("BITBAND")) 	 counters[0]++;
			else if (compName.equals("COMVERSE")) 	 counters[1]++;
			else if (compName.equals("MOBIXELL")) 	 counters[2]++;
			else if (compName.equals("OPTIBASE")) 	 counters[3]++;
			else if (compName.equals("ORCA") || compName.equals("ORCAINTERACTIVE")) counters[4]++;
			else if (compName.equals("SCOPUS")) 	 counters[5]++;
			else if (compName.equals("SINTECMEDIA")) counters[6]++;
			else 									 counters[7]++;
		}
%>
		<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
			<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;Company&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Registered users&nbsp;</h3></td>
				</tr>
			</thead>
			<tr style='color: white' align='center'><td>BitBand		</td><td><%=counters[0] %></td></tr>
			<tr style='color: white' align='center'><td>Comverse	</td><td><%=counters[1] %></td></tr>
			<tr style='color: white' align='center'><td>Mobixell	</td><td><%=counters[2] %></td></tr>
			<tr style='color: white' align='center'><td>Optibase	</td><td><%=counters[3] %></td></tr>
			<tr style='color: white' align='center'><td>Orca		</td><td><%=counters[4] %></td></tr>
			<tr style='color: white' align='center'><td>Scopus		</td><td><%=counters[5] %></td></tr>
			<tr style='color: white' align='center'><td>SintecMedia	</td><td><%=counters[6] %></td></tr>
			<tr style='color: white' align='center'><td>Other		</td><td><%=counters[7] %></td></tr>
		<tr  style='color: white;background-color: gray;' align='center' ><td><b>Total:</b></td><td><b> <%=temp.size() %></b></td></tr>
		</table>
<%} %>
