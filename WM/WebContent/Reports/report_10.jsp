<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.ArrayList" %>
<%@page import="myData.ComplexQuery"%>

<h2 style='color: white;'><U>10. Participation results</U></h2>
<br>
<%!

public int getDistinctUsersNo(String[][] strArr,int from,int to){
	ArrayList<String> temp = new ArrayList<String>();
	for(int i=from;i<to;i++){
		boolean exist = false;
		for(int j=0;j<temp.size();j++)
			if (strArr[i][1].equals(temp.get(j))){
				exist=true;
				break;
			}
		if(!exist) temp.add(strArr[i][1]);
	}
	return temp.size();
}

public int calcAverageByDays(String[][] strArr,ArrayList<Integer> indexes,int period){
	int sum  = 0;
	for(int i=0;i<indexes.size()-period;i++)
		sum+=getDistinctUsersNo(strArr,indexes.get(i),indexes.get(i+period));
	return sum/(indexes.size()-period);
}

%>


<%

 
	List temp = myData.CommonFunctions.watchDateProfileId();
	String[][] data = new String[temp.size()][2];
	int total = myData.CommonFunctions.totalProfiles();
	
	
	if (temp.size()==0){
		out.println("<h3 align='center' style='color: white'>No users found!</h3>");
	}else{
	
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		String currDate = "";
		for(int i=0;i<temp.size();i++){
			ComplexQuery q = (ComplexQuery)temp.get(i);
			data[i][0]=q.getString_field_0();
			data[i][1]=q.getString_field_1();
			if (!currDate.equals(data[i][0])){
				indexes.add(i);
				currDate=data[i][0];
			}
		}

		int[] resultsArr = new int[7];
		for(int i=0;i<7;i++)
			resultsArr[i]=calcAverageByDays(data,indexes,i+1);
		
		
%>
		<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
			<thead style="background: gray; color: white">
				<tr>
				<td align='center'><h3>&nbsp;Period&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Average number of participants&nbsp;</h3></td>
				<td align='center'><h3>&nbsp;Percent of total&nbsp;</h3></td>
				</tr>
			</thead>

			<tr style='color: white' align='center'><td>Each day</td>		<td><%=resultsArr[0] %></td><td><%=resultsArr[0]*100/total %>%</td></tr>			
			<tr style='color: white' align='center'><td>Each two days</td>	<td><%=resultsArr[1] %></td><td><%=resultsArr[1]*100/total %>%</td></tr>			
			<tr style='color: white' align='center'><td>Each three days</td><td><%=resultsArr[2] %></td><td><%=resultsArr[2]*100/total %>%</td></tr>			
			<tr style='color: white' align='center'><td>Each four days</td>	<td><%=resultsArr[3] %></td><td><%=resultsArr[3]*100/total %>%</td></tr>			
			<tr style='color: white' align='center'><td>Each five days</td>	<td><%=resultsArr[4] %></td><td><%=resultsArr[4]*100/total %>%</td></tr>			
			<tr style='color: white' align='center'><td>Each six days</td>	<td><%=resultsArr[5] %></td><td><%=resultsArr[5]*100/total %>%</td></tr>			
			<tr style='color: white' align='center'><td>Each week</td>		<td><%=resultsArr[6] %></td><td><%=resultsArr[6]*100/total %>%</td></tr>			
			<tr style='color: white;background-color: gray' align='center'><td>Total number of users registered</td><td><%=total %></td><td>100%</td></tr>			

		</table>
<%} %>
