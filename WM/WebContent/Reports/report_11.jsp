<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import = "myData.ComplexQuery" %>

<br>
<br>
<h2 style='color: white;'><U>11. Most active users</U></h2>

		<% 
			List temp = null;
			temp= myData.CommonFunctions.profileEntriesGroupsTotal();
			out.println("<h3 align='center' style='color: white'>There are "+temp.size()+" users </h3>");
			
			if (temp.size()>0)
			{ 
				%>
				<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
					<thead style="background: gray; color: white">
					<tr>
					<td align='center'><h3>&nbsp;#&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Email&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Experiment group&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Entries&nbsp;</h3></td>
					</tr>
				</thead>		
				<%
				ComplexQuery cq = null;
				for(int i=0;i<temp.size();i++){
					cq = (ComplexQuery)temp.get(i);
					out.println("<tr style='color: white' align='center'><td>"+(i+1)+"</td><td>"+cq.getString_field_0()+"</td><td>"+cq.getInt_field_0()+"</td><td>"+cq.getInt_field_1()+"</td></tr>");
				}
				out.println("</table>");
			} 
			%>
