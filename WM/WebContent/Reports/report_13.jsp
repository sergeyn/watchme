<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.List" %>
<%@ page import="myData.ComplexQuery"%>
<br>
<br>
<% 
int wNo = 17;
try{wNo=Integer.valueOf(request.getParameter("weekNo"));}
catch(Exception e){}
%>

<h2 style='color: white;'><U>13. Entries per user in a specific week</U></h2>
<br><h3 style='color: white;'><U>Week number <%=wNo-16 %></U></h3>

		<% 
		List temp = null;
		int totalNo= myData.CommonFunctions.totalProfiles();
		temp = myData.CommonFunctions.profileEntriesGSWeekly(wNo);

			
			if (temp.size()>0)
			{ 
				%>
				<table align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="black">
					<thead style="background: gray; color: white">
					<tr>
					<td align='center'><h3>&nbsp;User&nbsp;</h3></td>
					<td align='center'><h3>&nbsp;Entries&nbsp;</h3></td>
					</tr>
				</thead>		
				<%
				int sumOfEntries = 0;
				int sumOfEntriesNonZero = 0;
				int nonZero = 0;
				
				for(int i=0;i<temp.size();i++){
				ComplexQuery cq = (ComplexQuery)temp.get(i);
				sumOfEntries+=cq.getInt_field_0();
				if (cq.getInt_field_0()>0){
					sumOfEntriesNonZero += cq.getInt_field_0();
					nonZero ++;
				}
				out.println("<tr style='color: white' align='center'><td>&nbsp;"+cq.getString_field_0() +"&nbsp;</td><td>"+cq.getInt_field_0()+"</td></tr>");
				}	
			    out.println("<tr style='background-color:white;height:5px;' align='center'><td colspan=2 ></td></tr>");
				if (temp.size()>0) out.println("<tr style='color: white' align='center'><td colspan=2><h3>Average: "+(sumOfEntries*100/temp.size())/100.0+ " times.</h3></td></tr>");
				if (nonZero>0) out.println("<tr style='color: white' align='center'><td colspan=2><h3>Average without non-active users: "+(sumOfEntriesNonZero*100/nonZero)/100.0+ " times.</h3></td></tr>");
				out.println("</table>");
			} 
			%>