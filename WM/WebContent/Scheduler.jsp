<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Scheduler</title>
<script type="text/javascript">
function command(str){window.location="Scheduler.jsp?command="+str;}
</script>
</head>
<jsp:useBean class = "myData.Scheduler" id = "s" scope = "application"></jsp:useBean>
<body>

<%

// TO DO: change to normal time
int periodInSecs = 1800; // interation perion in seconds

String command = request.getParameter("command");


if (command!=null){
	if (command.equals("Start")){
		s.start(periodInSecs);
		response.sendRedirect("Scheduler.jsp");
	}	
	else 
	if (command.equals("Stop")){
		if (s!=null) s.cancel();
		response.sendRedirect("Scheduler.jsp");
	}	
}


if (s == null){
	out.println("Scheduler stopped!");
	out.println("<br><input type='button' onclick='command(\"Start\")' value='Start'>");
	}
else 
	{
	if (s.isStopped())
		{
		out.println("Scheduler stopped!");
		out.println("<br><input type='button' onclick='command(\"Start\")' value='Start'>");
		}
		else 
		{
		out.println("Scheduler started!");
		out.println("<br><input type='button' onclick='command(\"Stop\")' value='Stop'>");
		}
	}
%>
</body>
</html>