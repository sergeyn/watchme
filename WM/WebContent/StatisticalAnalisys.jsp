<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Statistical reports</title>
<script type = "text/javascript" src = "ajax.js"></script>
<script type="text/javascript"> 
function ajaxReturned(val){
document.getElementById('ajaxWait').style.visibility='hidden';
document.getElementById('select_01').disabled='';
if (document.getElementById('weeksOpt')!=null)
	document.getElementById('weeksOpt').disabled='';
document.getElementById('resultDiv').innerHTML=val;
}

var lastRunnedRep="";

function runReport(no){

if (no!="xx")
	document.getElementById("calendar").style.visibility="hidden";

if (no=="00") {
	document.getElementById('resultDiv').innerHTML="<br><img src=\"images/reports.gif\"><br>";
	return;
}

if (no=="xx") no=lastRunnedRep;
lastRunnedRep=no;	

var params="";

if (no=="08" || no=="13" || no=="14"){
	document.getElementById('resultDiv').innerHTML="<br><img src=\"images/reports.gif\"><br>";
	showWeekSelection("weekCell",no);
	return;
}else document.getElementById("weekCell").innerHTML="";


if (no=="06" || no=="07"){

	if (document.getElementById("calendar").style.visibility=="hidden"){
		document.getElementById("calendar").style.visibility="visible";
		document.getElementById('resultDiv').innerHTML="<br><img src=\"images/reports.gif\"><br>";
		return;
		}
		
	var selectedDate = document.getElementById("f_date_c").value;

	if (selectedDate=="Select date...")
		{
		window.alert("Please select date!");
		return;
		}
		else
		params="date="+selectedDate;
}



if (no!="06" && no!="07") document.getElementById("calendar").style.visibility="hidden";
document.getElementById('ajaxWait').style.visibility='visible';
document.getElementById('select_01').disabled='disabled';
postDataReturnText("Reports/report_"+no+".jsp",params,doit=function(returnedData){ajaxReturned(returnedData);})
}





	function runWeeklyReport(repNo,no){
		if (no==0) return;
		document.getElementById("calendar").style.visibility="hidden";
		document.getElementById('ajaxWait').style.visibility='visible';
		document.getElementById('select_01').disabled='disabled';
		document.getElementById('weeksOpt').disabled='disabled';
		var params = "weekNo="+(no+16);
		postDataReturnText("Reports/report_"+repNo+".jsp",params,doit=function(returnedData){ajaxReturned(returnedData);})
	}
	
	function showWeekSelection(cellId,repNo)	{
	var selection = "<br><b>Choose week : &nbsp;&nbsp;&nbsp;</b>";
	selection = selection +
		"<select id='weeksOpt' onchange=\"runWeeklyReport('"+repNo+"',this.selectedIndex)\">"+
		"<option value=' 0'> -------------------------------------- </option>"+
		"<option value='17'> [27.04.2008] Experiment week number  1 </option>"+
		"<option value='18'> [04.05.2008] Experiment week number  2 </option>"+
		"<option value='19'> [11.05.2008] Experiment week number  3 </option>"+
		"<option value='20'> [18.05.2008] Experiment week number  4 </option>"+
		"<option value='21'> [25.05.2008] Experiment week number  5 </option>"+
		"<option value='22'> [01.06.2008] Experiment week number  6 </option>"+
		"<option value='23'> [08.06.2008] Experiment week number  7 </option>"+
		"<option value='24'> [15.06.2008] Experiment week number  8 </option>"+
		"<option value='25'> [22.06.2008] Experiment week number  9 </option>"+
		"<option value='26'> [29.06.2008] Experiment week number 10 </option>"+
		"<option value='27'> [06.07.2008] Experiment week number 11 </option>"+
		"<option value='28'> [13.07.2008] Experiment week number 12 </option>"+
		"<option value='29'> [20.07.2008] Experiment week number 13 </option>"+
		"<option value='30'> [27.07.2008] Experiment week number 14 </option>"+
		"<option value='31'> [03.08.2008] Experiment week number 15 </option>"+
		"<option value='32'> [10.08.2008] Experiment week number 16 </option>"+
		"<option value='33'> [17.08.2008] Experiment week number 17 </option>"+
		"<option value='34'> [24.08.2008] Experiment week number 18 </option>"+
		"<option value='35'> [31.08.2008] Experiment week number 19 </option>"+
		"<option value='36'> [07.09.2008] Experiment week number 20 </option>"+
		"<option value='37'> [14.09.2008] Experiment week number 21 </option>"+
		"<option value='38'> [21.09.2008] Experiment week number 22 </option>"+
		"<option value='39'> [28.09.2008] Experiment week number 23 </option>"+
		"<option value='40'> [05.10.2008] Experiment week number 24 </option>"+
		"<option value='41'> [12.10.2008] Experiment week number 25 </option>"+
		"<option value='42'> [19.10.2008] Experiment week number 26 </option>"+
		"<option value='43'> [26.10.2008] Experiment week number 27 </option>"+
		"</select><br><br>";
	var tdCell = document.getElementById(cellId);
	tdCell.innerHTML = selection;
	}
		





</script>

  <link rel="stylesheet" type="text/css" media="all" href="Reports/Calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
  <script type="text/javascript" src="Reports/Calendar/calendar.js"></script>
  <script type="text/javascript" src="Reports/Calendar/calendar-en.js"></script>
  <script type="text/javascript" src="Reports/Calendar/calendar-setup.js"></script>


</head>
<body dir="ltr">

<div style="visibility:hidden; position: absolute;top: 300;left: 450" id="ajaxWait" >
<table border="5" bordercolor='navy'  style="border-style: outset; " >
<tr><td><img src="images/Loading.gif"></td></tr>
</table>
</div>


<table  border="0" cellpadding="0" cellspacing="0" border="0">
<tr><td><img src="images/tv_header.jpg"></td></tr>
<tr>
	<td colspan="10" style='background-image: url("images/tv_middle.jpg");background-repeat: repeat-y;'>
		<table align="center" border="0" cellpadding="0" cellspacing="0" style="color: white;">
			
			<tr align="center"><td ><h1><u>Statistical reports</u></h1></td></tr>
			<tr>
				<td>
					<select onchange="runReport(this.options[this.selectedIndex].value);" id="select_01">
						<option value="00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Choose report</option>		
						<option value="01">&nbsp;1.&nbsp;Todays watchers</option>		
						<option value="02">&nbsp;2.&nbsp;Participants with all the genres in future interest</option>		
						<option value="03">&nbsp;3.&nbsp;Non active users</option>		
						<option value="04">&nbsp;4.&nbsp;All registered users</option>		
						<option value="05">&nbsp;5.&nbsp;Users per strategy and group</option>		
						<option value="06">&nbsp;6.&nbsp;Active users per specific day</option>		
						<option value="07">&nbsp;7.&nbsp;Inactive users per specific day</option>		
						<option value="08">&nbsp;8.&nbsp;Percentage of visiting users in a specific week per group</option>		
						<option value="09">&nbsp;9.&nbsp;Total number of participants by company</option>		
						<option value="10">     10.&nbsp;Participation results</option>		
						<option value="11">     11.&nbsp;Most active users</option>		
						<option value="12">     12.&nbsp;Number of watched trailers and entries by each profile</option>		
						<option value="13">     13.&nbsp;Entries per user in a specific week</option>		
						<option value="14">     14.&nbsp;Number of users that didn't enter the system at least one week</option>		
					</select>
			
				</td>
			</tr>
			<tr><td>&nbsp;<br></td></tr>
			<tr align="center" ><td>
			<div id="weekCell" ></div>
			<table style="visibility: hidden;color: white" id="calendar" >
					<tr>
						
						<td> Click button to get report on date -   <input id="f_date_c" type="button" value="Show report ->" onclick="runReport('xx');">
						</td>
					</tr>
					<tr>
						<td align="center">
							<br>
								Use calendar to change date - <img src="Reports/Calendar/calendar.jpg" id="f_trigger_c"  title="Calendar" style="cursor: pointer;">
						</td>
						
					</tr>
			</table>

			<script type="text/javascript">
			var currDate = new Date();
			document.getElementById("f_date_c").value = currDate.getDate()+"/"+(currDate.getMonth()+1)+"/"+currDate.getFullYear();
			</script>

<script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_date_c",     // id of the input field
        ifFormat       :    "%d/%m/%Y",    // format of the input field
        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>
			
			</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center" style='background-image: url("images/tv_middle.jpg");background-repeat: repeat-y;' id='resultDiv'>
	<br><img src="images/reports.gif"><br>
	</td>
</tr>
<tr><td> <img src="images/tv_footer.jpg"></td></tr>
</table>
</body>
</html>