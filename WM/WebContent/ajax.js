function GetXmlHttpObject()
{
var xmlHttptemp=null;
try
  {
  // Firefox, Opera 8.0+, Safari
  xmlHttptemp=new XMLHttpRequest();
  }
catch (e)
  {
  // Internet Explorer
  try
    {
    xmlHttptemp=new ActiveXObject("Msxml2.XMLHTTP");
    }
  catch (e)
    {
    try
      {
      xmlHttptemp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    catch (e)
      {
      alert("Your browser does not support AJAX!");
      return false;
      }
    }
  }

return xmlHttptemp;
}

function getDataReturnText(url, callback)
{
var XMLHttpRequestObject = null;
url += '&httpat=' + new Date().getTime();
if (!(XMLHttpRequestObject=GetXmlHttpObject())){alert ("Your browser does not support AJAX technology!");return;} 

XMLHttpRequestObject.open("GET",url,true);
XMLHttpRequestObject.setRequestHeader("charset","UTF-8"); 
//myReq.setRequestHeader(�If-Modified-Since�, �Thu, 1 Jan 1970 00:00:00 GMT�);
//myReq.setRequestHeader(�Cache-Control�, �no-cache�);

XMLHttpRequestObject.onreadystatechange=function()
{
	if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status == 200)
	{
// start response code
		var theResponse=XMLHttpRequestObject.responseText;
		callback(theResponse); 
		delete XMLHttpRequestObject;
		XMLHttpRequestObject = null;
// end response code
	}
	else
	{
// start response code
          callback("<!--loading-data--><p style='float:center;text-align:center; text-valign:center;'><img style='width:16px; height:16px;' src='progress_blue_gray_circle.gif' border='0'></p>"); 
// end response code
	}
}

XMLHttpRequestObject.send(null);

}

function postDataReturnText(url, data, callback)
{
var XMLHttpRequestObject = null;

if (!(XMLHttpRequestObject=GetXmlHttpObject())){alert ("Your browser does not support AJAX technology!");return;} 

XMLHttpRequestObject.open("POST",url,true);
XMLHttpRequestObject.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
XMLHttpRequestObject.setRequestHeader("Content-length",data.length); 
XMLHttpRequestObject.setRequestHeader("charset","UTF-8"); 

XMLHttpRequestObject.onreadystatechange=function()
{
	if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status == 200)
	{
// start response code
		var theResponse=XMLHttpRequestObject.responseText;
		callback(theResponse); 
		delete XMLHttpRequestObject;
		XMLHttpRequestObject = null;
// end response code
	}
}

XMLHttpRequestObject.send(data);

}




function Get_Cookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f
	
	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );
		
		
		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
	
		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found ) 
	{
		return null;
	}
}

function Set_Cookie( name, value, expires, path, domain, secure ) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );
	// if the expires variable is set, make the correct expires time, the
	// current script below will set it for x number of days, to make it
	// for hours, delete * 24, for minutes, delete * 60 * 24
	if ( expires )
	{
		expires = expires * 1000 * 60 * 60 * 24;
	}
	//alert( 'today ' + today.toGMTString() );// this is for testing purpose only
	var expires_date = new Date( today.getTime() + (expires) );
	//alert('expires ' + expires_date.toGMTString());// this is for testing purposes only

	document.cookie = name + "=" +escape( value ) +
		( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + //expires.toGMTString()
		( ( path ) ? ";path=" + path : "" ) + 
		( ( domain ) ? ";domain=" + domain : "" ) +
		( ( secure ) ? ";secure" : "" );
}

function Delete_Cookie( name, path, domain ) {
	if ( Get_Cookie( name ) ) document.cookie = name + "=" +
			( ( path ) ? ";path=" + path : "") +
			( ( domain ) ? ";domain=" + domain : "" ) +
			";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

