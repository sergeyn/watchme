<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" %>
<%@ page import = "java.util.*" %>
<%@ page import = "java.lang.Math.*" %>
<%@page import="myData.CommonFunctions"%>
<%@page import="myData.Conversions"%>
<%@page import="myData.Movielens_data"%>
<%@page import="myData.Movielens_statistics"%>
<%@page import="myData.Demographics"%>

<%
	List CombinationProfiles= null; 
	Movielens_data MD = new Movielens_data();
	Movielens_data tempMD = new Movielens_data();
	Movielens_statistics MS = null;
	Demographics demographics = new Demographics();
	
	int i=0, j=0, z=0, t=0, k=0;
	String genreName = "";
	double genreSD = 0;
	double genreMean = 0;
	double currSD = 0;
	double currDistance = 0;//help variable for sd calc
	double sumSquares = 0;//help viriable for sd calc
	double currMean = 0;
	int age= 0;
	int occupation = 0;
	int gender = 0;
	
//demographocs calculation
	for( ;i<11;i++){//run through genres
		
		genreName=myData.Conversions.genreNameFromIndexToString(i);
		MS= myData.CommonFunctions.selectGenreSDandMD(genreName);
		genreSD=MS.getSd();
		genreMean=MS.getMean();
		System.out.println("for genre: " + genreName + " the sd is: " + genreSD);
		
		for(j=0;j<2;j++){// run through gender
	
			gender=j;
		
			for(z=0;z<7;z++){//run through age
		
				age=myData.Conversions.ageFromIndexToAgeGoup(z);
	
				for(t=0;t<21;t++){//run through occupation
			
					occupation=t;
			
					MD.setAge(age);
					MD.setGender(gender);
					MD.setGenreName(genreName);
					MD.setOccupation(occupation);
	
					CombinationProfiles=myData.CommonFunctions.selectCombinationProfiles(MD);
					//combination sd and mean calculation
					sumSquares = 0;
					currMean = 0;
					currSD = 0;
					
					for(k=0; k<CombinationProfiles.size(); k++){
						tempMD=(Movielens_data)CombinationProfiles.get(k);
						currDistance=Math.pow((tempMD.getMean()- genreMean),2);
						sumSquares+=currDistance;
						currMean+=tempMD.getMean();
					}
					if (CombinationProfiles.size()!=0){
						currSD=Math.pow((sumSquares/CombinationProfiles.size()),0.5);
						currMean/=CombinationProfiles.size();
						//should recommend check
						if ((currSD > genreSD) && (currMean>genreMean))
							demographics.setShouldRecommend("Yes");
						else if ((currSD > genreSD) && (currMean<genreMean))
							demographics.setShouldRecommend("No");
						else if ((currSD <= genreSD) && (currMean>=genreMean))
							demographics.setShouldRecommend("RY");
						else if ((currSD <= genreSD) && (currMean<=genreMean) )
							demographics.setShouldRecommend("RN");
												
					}
					else demographics.setShouldRecommend("M");
					
					demographics.setGenreName(genreName);
					demographics.setGender(Conversions.genderFromIntToString(gender));
					demographics.setAge(age);
					demographics.setOccupation(Conversions.occupationFromIntToString(occupation));
				
					System.out.println("Before update:");
					System.out.println("CurrentSD is: " + currSD + " GenreSD: " + genreSD);
					System.out.println("CurrentMean is: " + currMean + " currMean: " + genreMean);
					CommonFunctions.selectDemographicRecommender(demographics);
					myData.CommonFunctions.updateDemographicShouldRecommend(demographics);
					System.out.println("genre name is: " + genreName);
					System.out.println("for gender= " + gender);
					System.out.println(", age= " + age);
					System.out.println(", occupation= " + occupation);
					System.out.println("we updated: " + demographics.getShouldRecommend());
					
					String test = CommonFunctions.selectDemographicRecommender(demographics);
					System.out.println("After update: " + test);
				}
			
			}
		}
	}
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Insert title here</title>
</head>
<body>

</body>
</html>