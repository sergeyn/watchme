<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page import = "java.util.*" %>
<%@ page session="false" %>
<%@ page import="myData.Profile_genres"%>	
<%@ page import="myData.Profile"%>
<%@ page import="myData.Conversions"%>
<%@page import="myData.Demographics"%>

<%
//This page assignes demographic rules to new users

	List profiles = null;
	//select profiles without demographic recommender
	profiles = myData.CommonFunctions.selectProfiles();
 
	myData.Demographics demographic = new myData.Demographics() ;
	myData.Profile_genres profileGenres = new Profile_genres();
	myData.Profile profile = null;
	int i=0, j=0;
	String genreName = "", tempRecommender = "";
	
	//update DemographicRecommender in profile_genres
	for(; i<profiles.size();i++){//run through profiles
		
		profile= (Profile)profiles.get(i);
		
		for(j=0;j<11;j++){//run through genres
			demographic.setAge(Conversions.ageFromSpecificToGroup(profile.getAge()));
		
			if(profile.getGender().equals("Male"))
				demographic.setGender("M");
			else if(profile.getGender().equals("Female"))
				demographic.setGender("F");
			
			demographic.setOccupation(Conversions.occupationFromIntToString(profile.getOccupation()));
			switch(j){
			case 0: genreName = "Action";
			break;
			case 1: genreName = "Thriller";
			break;
			case 2: genreName = "Crime";
			break;
			case 3: genreName = "Drama";
			break;
			case 4: genreName = "Horror";
			break;
			case 5: genreName = "Adventure";
			break;
			case 6: genreName = "Comedy";
			break;
			case 7: genreName = "Romance";
			break;
			case 8: genreName = "Family";
			break;
			case 9: genreName ="Sci-Fi";
			break;
			case 10: genreName ="Animation";
			}
			demographic.setGenreName(genreName);
	
			tempRecommender=myData.CommonFunctions.selectDemographicRecommender(demographic);
			
			profileGenres.setGenreId(Conversions.genreFromStringToInt(genreName));
			profileGenres.setProfileId(profile.getProfileId());
			profileGenres.setDemographicRecommender(tempRecommender);
			profileGenres.setDemographicWeight(Conversions.DemographicToWeight(tempRecommender));
			
			myData.CommonFunctions.updateDRecommenderInProfileGenres(profileGenres);
			
			
			
		}
		
	}
	

%>	
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Insert title here</title>
</head>
<body>

</body>
</html>