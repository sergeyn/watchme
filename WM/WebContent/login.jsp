<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import = "javax.servlet.http.HttpSession" %> 
<%@ page session="false" %>
<% 
	int MOVIES_TO_SEE = 3; // Movies amount for everyday watching
	response.setContentType("text/html");
	HttpSession http_session = request.getSession(false);
	if (http_session!=null)	http_session.invalidate();
	
	String idStr = request.getParameter("id");
	String password = request.getParameter("pass");
	String checked = request.getParameter("checked");	

	boolean notFound = false;	
	
	if (idStr!=null && password!=null){
		if (!idStr.equals("") && !password.equals("")){
		// Saving cookies on local machine
			if (checked != null && checked.equals("on")) {
				checked = "checked";
				Cookie cookie = null;
				cookie = new Cookie("id",idStr);
				cookie.setSecure(false);
				cookie.setPath(request.getContextPath());
				cookie.setMaxAge(3153600); // year
				response.addCookie(cookie);
				cookie = new Cookie("password", password);
				cookie.setSecure(false);
				cookie.setPath(request.getContextPath());
				cookie.setMaxAge(3153600); // year
				response.addCookie(cookie);
			}else {
				checked = "";
				Cookie[] cookies = request.getCookies();
				if (cookies != null)
					for (int i = 0; i < cookies.length; i++) {
						cookies[i].setMaxAge(0);
						cookies[i].setPath(request.getContextPath());
						response.addCookie(cookies[i]);
					}
				}   
			}
		
		myData.Profile p = new myData.Profile();
		p.setProfileId(idStr.trim());
		p.setPassword(password.trim());
		p = myData.CommonFunctions.selectProfileByIdAndPass(p);
		
			notFound = true;
			if (p!=null) {
				http_session = request.getSession(true);
				http_session.setAttribute("MOVIES_TO_SEE",MOVIES_TO_SEE);				
				http_session.setAttribute("currentUser",p);		 		
				http_session.setAttribute("Seen",myData.CommonFunctions.getSeenTrailersPerTodayByProfile(p.getProfileId()));
				response.sendRedirect("registrationForm_p1.jsp?email="+p.getProfileId());
				return;
			}
		}else{
			idStr="";
			password="";
			Cookie[] cookies=request.getCookies(); 
			if (cookies!=null)
				for(int i=0;i<cookies.length;i++){
					if (cookies[i].getName().equals("id")){idStr=cookies[i].getValue();checked="checked";}
						else if (cookies[i].getName().equals("password"))password=cookies[i].getValue();
				}
		}	 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Wellcome to "Watchme"!</title>
<script type="text/javascript">
function submitForm(){
	idF = document.getElementById("id1");
	psF = document.getElementById("id2");
	
	if (idF.value.length<6 || idF.value.length>50 || psF.value.length<1 || psF.value.length>20) 
	{
	window.alert("Wrong data! Check again!");
	return;
	}

document.getElementById("wImg").style.visibility='visible';
document.getElementById("sb_1").disabled='disabled';
document.getElementById("myform").submit();
}

//---------------------------- enter press
document.onkeypress = checkEnter;
function checkEnter(e){if (event.keyCode==13)submitForm();}
//----------------------------
</script>
</head>

<body vlink="blue" alink="white">
<img src="images/thpx600plasma_c8.jpg"  style="position: absolute; left: 10px; top: 10px; z-index: 0">
<img src="images/myss_logo_300x300_8.jpg"  style="position: absolute; left: 120px; top: 70px; z-index: 1" width="141" height="141">
<p align="center">
<span style="position: absolute; left: 350px; top: 100px; z-index: 2" >
<b>
<font face="Tahoma" size="7" color="#D9FFFF">Watchme</font>
<br><br><br><br>
<font face="Traditional Arabic" size="5" color="#D9FFFF">Please log in</font>
<br><br><br>
<b dir="ltr" id="wImg" style="visibility: hidden;color: red;">Please wait...</b>
<br>
</b>
</span>
</p>
<form action="login.jsp" method="post" id="myform">
<table style="position: absolute; left: 280px; top: 350px; z-index: 2; color: white;">
<tr><td>Email:</td><td>&nbsp;<input type="text" tabindex="1" name = "id" id = "id1" value="<%=idStr %>"></td><td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="registrationForm_p0.jsp" style="color: white">New user!? Click here</a></td></tr>
<tr><td>Password:</td><td>&nbsp;<input type="password" tabindex="2" name = "pass" id = "id2" value="<%=password %>"></td><td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="passRecovery.jsp" style="color: yellow">Forgot password!? Click here</a></td></tr>
<tr><td>Remember me:</td><td><input type="checkbox" name="checked" <%=checked %>></td><td>&nbsp;</td></tr>
<tr align="right"><td colspan="2"><input type="button" tabindex="3" id="sb_1" value="Submit" onclick="submitForm();"></td></tr>
<% if (notFound){ %> <tr><td colspan='2' style='color: red'> Wrong id or password!</td></tr> <% } %>


</table>
</form>
</body>
</html>