<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "javax.servlet.http.HttpSession" %>
<%@ page session="false" %>
<%
HttpSession user_session = request.getSession(false);
if (user_session==null){
	response.sendRedirect("login.jsp");
	return;
}  
//TODO: do adaptation on day end [P&S] - call adapt on each genre in a loop and save/...


int MOVIES_TO_SEE = (Integer)user_session.getAttribute("MOVIES_TO_SEE");
int seenTrailers = (Integer)user_session.getAttribute("Seen");
myData.Profile currentUser = (myData.Profile)user_session.getAttribute("currentUser");

int availableTrailers= CommonFunctions.availableTrailersPerProfile(currentUser.getProfileId());
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="myData.CommonFunctions"%>
<%@page import="myData.Adaptation"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>"Watchme"</title>
</head>
<body >
<b onclick="window.location='login.jsp'"><img title="LOGOUT" style="color: white; position: absolute;top: 32px;left: 861px;z-index: 2;cursor: pointer;" src="images/logout.png" ></b>
<span style="color: white; position: absolute;top: 32px;left: 130px;z-index: 2"><b>Logged as: <%=currentUser.getProfileId() %></b></span>
<img src="images/thpx600plasma_c8.jpg"  style="position: absolute; left: 10px; top: 10px; z-index: 0">
<img src="images/myss_logo_300x300_8.jpg"  style="position: absolute; left: 120px; top: 70px; z-index: 1" width="141" height="141">
<p align="center">
<span style="position: absolute; left: 270px; top: 100px; z-index: 2" >
<b>
<font face="Tahoma" size="7" color="#D9FFFF">Watchme</font>
<br><br>
<font face="Traditional Arabic" size="4" color="#D9FFFF">&nbsp;</font>
<br><br><br><br>
</b>
	<% 	if (availableTrailers==0){	user_session.invalidate(); 	%>
	<i>
	<font face="Traditional Arabic" color="#D9FFFF" size="5">
	You watched all trailers!
	<br>
	Thank you for participation!
	</font>
	</i>
	<% 	}else if (MOVIES_TO_SEE-seenTrailers>0) {%>	
	<i>

	<font face="Traditional Arabic" color="#D9FFFF" size="5">
	<br><br>
	Press start to begin watching!
	</font>
	</i>
	<%}else{ 
		
		//adaptation
		
		Adaptation.DailyUpdate(currentUser.getProfileId());
		// user_session.invalidate(); 
		%>
	<i>
	<font face="Traditional Arabic" color="#D9FFFF" size="5">
	Thank you for watching!<br>
	<% 
	
	myData.ComplexQuery c = CommonFunctions.entriesPerProfile(currentUser.getProfileId());
	int visits = c.getInt_field_0();
	
	if (visits>44){
	out.println("<br>Congratulations!<br>This was your final visit!");
	out.println("<br><br>No more reminders would be sent to your email<br>");
	out.println("<br>Watchme Team thanks you for your participation in our experiment");
	
	}
	else 
	{
	out.println("This was your visit No "+visits+" out of 45");
	out.println("<br>Please visit us tomorrow!");
	}	
	%>
	

	</font>
	</i>
	<%} %>
</span>
</p>
<table style="position: absolute; left: 510px; top: 470px; z-index: 2; color: white;">
<tr>
	<td>
	<% if (MOVIES_TO_SEE-seenTrailers>0 && availableTrailers>0) {%>	
	<input type="button" value="start" onclick="window.location = 'page_2.jsp'" >
	<%} %>
	</td>
</tr>
</table>
</body>
</html>