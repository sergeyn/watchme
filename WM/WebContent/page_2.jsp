<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import = "javax.servlet.http.HttpSession" %>

<%@ page session="false" %>
<%
HttpSession user_session = request.getSession(false);
if (user_session==null){
	response.sendRedirect("login.jsp");
	return;
}
int MOVIES_TO_SEE = (Integer)user_session.getAttribute("MOVIES_TO_SEE");
int seenTrailers = (Integer)user_session.getAttribute("Seen");
myData.Profile currentUser = (myData.Profile)user_session.getAttribute("currentUser"); 

//int trailersLeft = (((List)myData.CommonFunctions.getNewTrailerByProfile(currentUser.getProfileId())).size());
//int randomTrailer = (int)(Math.random()*(trailersLeft*1.0));
myData.Trailers trailer = myData.Recommender.Recommend(currentUser) ; //(myData.Trailers)(((List)myData.CommonFunctions.getNewTrailerByProfile(currentUser.getProfileId())).get(randomTrailer));
String trailerGenre = myData.CommonFunctions.selectGenreNameByTrailerId(trailer.getTrailerId());

int maxLength = trailer.getPlot().length();
if (maxLength>850) maxLength = 850;

%> 
<%@page import="myData.Recommender;"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ratings</title>
<script type = "text/javascript" src = "ajax.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript">
// -------- Movie data ------------------------------------
	var movieHeight = 350;
	var movieWidth  = 400;
	var movieName   = "<%=trailer.getTrailerId()%>.flv";
	//var movieName   = "Ac1002.flv";

// ---------------------------------------------------------
	function submitFrom(){

		var rating = -1;
		var seen   = -1;
		var rec    = -1;
			
		for(i=0;i<5;i++) if (document.getElementById('r'  +i).checked) rating = i+1;
		for(i=0;i<2;i++) if (document.getElementById('s'  +i).checked)   seen = i;
		for(i=0;i<2;i++) if (document.getElementById('rec'+i).checked)    rec = i;

		if (rating == -1 || seen == -1 || rec == -1){
			window.alert("Please check all!");
			return;
			}

	endTime = setTime();
	document.getElementById("st").value = startTime;
	document.getElementById("et").value = endTime;
	document.getElementById("rt").value=(rec*100+seen*10+rating);
	document.getElementById("sendB").disabled=true;
	document.getElementById("ratingForm").submit();
	}

	function setMovie(){
	var s1 = new SWFObject("flvplayer.swf","flashClip",movieWidth,movieHeight,"7");
	s1.addParam("allowfullscreen","true");
	s1.addVariable("file","Trailers/"+movieName);
	s1.addVariable("image","images/myss_logo_300x300_8.jpg");
	s1.addVariable("width",movieWidth);
	s1.addVariable("height",movieHeight);
	s1.write("playerZone");
	}

/*
'2004.12.12 10:22:59'
# getSeconds() - Number of seconds (0-59)
# getMinutes() - Number of minutes (0-59)
# getHours() - Number of hours (0-23)
# getDay() - Day of the week(0-6). 0 = Sunday, ... , 6 = Saturday
# getDate() - Day of the month (0-31)
# getMonth() - Number of month (0-11)
# getFullYear() - The four digit year (1970-9999)
*/
	
	var startTime = ""
	var endTime = "";
	
	function setTime(){

		var Today = new Date();
		var currYear = Today.getYear();
		if (navigator.appName!='Microsoft Internet Explorer') currYear+=1900;
		var Time = "" +
			currYear+"."+
			(Today.getMonth()+1)+"."+
			Today.getDate()+" "+
			Today.getHours()+":"+
			Today.getMinutes()+":"+
			Today.getSeconds();
		return (Time);	
	}
	
	startTime = setTime();

function ajaxReturned(val){
document.getElementById('ajaxWait').style.visibility='hidden';
alert("Thank you for reporting!\n ");
window.location="page_2.jsp";
}


function mdPlay(){
	if (!confirm('Are you sure?!')) return false;
	document.getElementById('bcb').disabled='disabled'
	document.getElementById('mdp').disabled='disabled'
	document.getElementById('sendB').disabled='disabled'
	document.getElementById('ajaxWait').style.visibility='visible';
	postDataReturnText('badContentAjax.jsp?mdp=1&pid=<%=currentUser.getProfileId() %>&tid=<%=trailer.getTrailerId() %>&gn=<%=trailerGenre %>','',doit=function(returnedData){ajaxReturned(returnedData);})
}	

	
function badContent(){
	if (!confirm('Are you sure?!')) return false;
	document.getElementById('bcb').disabled='disabled'
	document.getElementById('mdp').disabled='disabled'
	document.getElementById('sendB').disabled='disabled'
	document.getElementById('ajaxWait').style.visibility='visible';
	postDataReturnText('badContentAjax.jsp?mdp=0&pid=<%=currentUser.getProfileId() %>&tid=<%=trailer.getTrailerId() %>&gn=<%=trailerGenre %>','',doit=function(returnedData){ajaxReturned(returnedData);})
}	
	
	
</script>
</head>
<body  onload="setMovie();" >

<b onclick="window.location='login.jsp'"><img title="LOGOUT" style="color: white; position: absolute;top: 32px;left: 861px;z-index: 2;cursor: pointer;" src="images/logout.png" ></b>
<span style="color: white; position: absolute;top: 32px;left: 130px;z-index: 2"><b>Logged as: <%=currentUser.getProfileId() %> </b></span>
<span style="color: white; position: absolute;top: 32px;left: 700px;z-index: 2"><b>Trailer <%=seenTrailers+1 %> of <%=MOVIES_TO_SEE %> </b></span>
<img src="images/PlazmaNew.jpg"  style="position: absolute; left: 10px; top: 10px; z-index: 0">
<table   border=0 width="260" height="350" style="position: absolute; left: 570px; top:  84px; z-index: 2">
<tr align="center" style="color: white;" valign="top"><td><h3>Title</h3><%=trailer.getTitle() %><hr></td></tr>
<tr align="center" style="color: white;" valign="top" ><td style="font-size : 9pt;"><h3>Plot</h3><%=trailer.getPlot().substring(0,maxLength) %></td></tr>
</table>

<p align="center">
<span style="position: absolute; left: 440px; top: 30px; z-index: 2" >
<font face="Tahoma" size="4" color="#D9FFFF">Watchme</font>
</span>
</p>
<table style="position: absolute; left: 130px; top:  80px; z-index: 2">
<tr><td colspan="2" align="left" id="playerZone"></td></tr>

<!-- 
<tr>
	<td colspan="1">
	<br>
	<input id='bcb' type="button" value="Report bad content" style="background-color:  red;color: white;" onclick="badContent();">
	<input id='mdp' type="button" value="Movie doesn't play" style="background-color:  red;color: white;" onclick="mdPlay();">	
	<div id='ajaxWait' style="color: white; visibility: hidden;" align="center" >
	<br>
	<b>Please wait</b>
	<br> 	
	<img src="images/wait.gif">
	</div>
	</td>
</tr>
 -->
 
<tr><td colspan="2"><br><br><br><br><br><br><br><br><br></td></tr>
<tr style="color: white;">
	<td>How much would you like to watch this movie?</td> 
	<td>
	<img src="images/scale.bmp">
	<br>
	(1<input type="radio" name="rating" id="r0">)
	(2<input type="radio" name="rating" id="r1">)
	(3<input type="radio" name="rating" id="r2">)
	(4<input type="radio" name="rating" id="r3">)
	(5<input type="radio" name="rating" id="r4">)
	</td>
</tr>
<tr><td colspan="2"><br></td></tr>
<tr style="color: white;">
	<td>Did you see this movie? &nbsp;</td> 
	<td>
 	      (Yes<input type="radio" name="seen" id="s0">)
	 (&nbsp;No<input type="radio" name="seen" id="s1">)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" id="sendB" value="Watch next ->" onclick="submitFrom();">
	</td>
</tr>
<tr><td colspan="2"><br></tr>
<tr style="color: white;">
	<td>Did somebody recommend you this movie? &nbsp;</td> 
	<td>
 	      (Yes<input type="radio" name="recommend" id="rec0">)
	 (&nbsp;No<input type="radio" name="recommend" id="rec1">)
	</td>
</tr>
</table>
		<form action="saveRating.jsp" method="post" id="ratingForm">
		<input type="hidden" name="TrailerId" value="<%=trailer.getTrailerId() %>">
		<input type="hidden" name="Rating"    value="" id = "rt">
		<input type="hidden" name="startTime" value="" id = "st">
		<input type="hidden" name="endTime"   value="" id = "et">
		</form>
</body>
</html>