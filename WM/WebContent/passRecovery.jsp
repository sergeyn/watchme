<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page session="false" %>
<% 
String errMessage = "";
String wrongEmail = "";
if (request.getParameter("wrongEmail")!=null){
	errMessage = "This email doesn't exist in system!";
	wrongEmail = request.getParameter("wrongEmail");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Password recovery!</title>
</head>
<body >
<img src="images/thpx600plasma_c8.jpg"  style="position: absolute; left: 10px; top: 10px; z-index: 0">
<img src="images/myss_logo_300x300_8.jpg"  style="position: absolute; left: 120px; top: 70px; z-index: 1" width="141" height="141">
<form action="passwordRec.jsp" method="post" id="myform" style="position: absolute; left: 370px; top: 100px; z-index: 2; color: white">
<b><font face="Tahoma" size="7" color="#D9FFFF">Watchme</font></b>
<br><br><br><br><br><br>
<h3>Please enter your email and press "send"!</h3>
<table>
<tr><td>Email:</td><td>&nbsp;<input type="text" name = "email" id = "mail" value="<%=wrongEmail %>"></td></tr>
<tr><td>&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" style="color: red;"> &nbsp;<%=errMessage %> </td></tr>
<tr><td>&nbsp;</td><td align="center">&nbsp;<input type="button" value="Send" onclick="submitForm(this);"></td></tr>
</table>
</form>
</body>
<script type="text/javascript">
// --------------------------------------------------------------------------
function submitForm(button){
		
		var mailObject = document.getElementById("mail");
		
		if (!isNumsAndLettersOnly(mailObject.value)){
			mailObject.focus();
			window.alert("!������ �������,������ '@' �'.' ����");
			return;		
		}

		var validExp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
		if (!validExp.test(mailObject.value) || mailObject.value.length<1 || mailObject.value.length>100){
			mailObject.focus();
			window.alert("!����� ����� �����");
			return;		
		}

	button.disabled=true;
	document.forms[0].submit();
}
// --------------------------------------------------------------------------
	function isNumsAndLettersOnly(String){
		var allowedSymbols=".@0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-"
		for(var intLoop=0;intLoop<String.length;intLoop++)
			if (allowedSymbols.indexOf(String.charAt(intLoop))== -1)
				return false;
		return true;
	}
</script>

</html>