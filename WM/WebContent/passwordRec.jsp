<%@page import="myData.CommonFunctions"%>
<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page import = "java.util.List" %>
<%@ page session="false" %>
<%!
	// creating new message function
	private String createMessage(String email){
		List emailsData = null;
		try{emailsData = CommonFunctions.getDataOnEmail(email);}
		catch(Exception e){	return null;}
	 
		if (emailsData==null || emailsData.size()==0) return null;
	
		// main string in returned mail
		String returnString =   "Hello, here is your login data:" + ((char)10)+((char)13)+((char)10)+((char)13);
	
		for(int i=0;i<emailsData.size();i++){
			myData.Profile p = (myData.Profile)emailsData.get(i);
			returnString = returnString + "ID: "+ p.getProfileId() + ((char)10)+((char)13)+ "Password :"+ p.getPassword() +((char)10)+((char)13)+((char)10)+((char)13);
		}
		return(returnString);
	}
%>
<% 
//------ MAILING PROPERTIES ------------------------------------------------------------
// mail to new registrant
String from = "admin.watchme@gmail.com";
String   to = request.getParameter("email");
String subject = "Watch me - password recovery";
String emailContent = createMessage(to);

if (emailContent==null) {
	response.sendRedirect("passRecovery.jsp?wrongEmail="+to);
	return;
}

myData.GMail mail = new myData.GMail();

try {
	mail.sendMessage(from,to, subject, emailContent);
	response.sendRedirect("recOK.jsp?email="+to);
	} 
catch (Exception e)	{
	e.printStackTrace();
	response.sendRedirect("passRecovery.jsp?wrongEmail="+to);
	return;
	}
// ------ END OF MAILING PROPERTIES ------------------------------------------------------------
%>
