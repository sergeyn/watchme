<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String email = request.getParameter("email");
String message = "Please try again later!";
if (email!=null) message = "Address "+email+" already registered!";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration failed!</title>
</head>
<body >
<img src="images/thpx600plasma_c8.jpg"  style="position: absolute; left: 10px; top: 10px; z-index: 0">
<img src="images/myss_logo_300x300_8.jpg"  style="position: absolute; left: 120px; top: 70px; z-index: 1" width="141" height="141">
<span style="position: absolute; left: 370px; top: 100px; z-index: 2; color: white" >
<b><font face="Tahoma" size="7" color="#D9FFFF">Watchme</font>
<br><br><br><br><br><br></b>
<h3 align="center">There were errors during the registration!
<br><%=message %><br>
<br><br><br>
<input type="button" value="     Home     " onclick="window.location='login.jsp'">
</h3>
</span>
</body>
</html>