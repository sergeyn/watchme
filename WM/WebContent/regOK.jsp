<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session="false" %>
<%
String email = request.getParameter("email");
if (email==null){
	response.sendRedirect("login.jsp");
	return;
}

String link = "registrationForm_p1.jsp?email="+email;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration completed!</title>
<script type="text/javascript">
	document.onkeydown = checkKeycode
	var altPressed = false;
	function checkKeycode(e) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	
		 if (keycode==18) altPressed=true;
	else if (keycode!=18 && altPressed) 
		{
				 if (keycode==49) {window.location="<%=link%>";}
	//		else if (keycode==50) {alert("pressed alt+2");}
	//		else if (keycode==51) {alert("pressed alt+3");}
			altPressed=false;
		}		
	else 	altPressed=false;
	}
</script>

</head>
<body >
<img src="images/thpx600plasma_c8.jpg"  style="position: absolute; left: 10px; top: 10px; z-index: 0">
<img src="images/myss_logo_300x300_8.jpg"  style="position: absolute; left: 120px; top: 70px; z-index: 1" width="141" height="141">
<span style="position: absolute; left: 370px; top: 100px; z-index: 2; color: white" >
<b>
<font face="Tahoma" size="7" color="#D9FFFF">Watchme</font>
<br><br><br><br><br><br>
</b>
<h3 align="center">Thank you for registration!<br>Your registration details were sent to:</h3>
<h2 align="center"><%=email %>	<br><br><br><br>
<input type="button" value="     Home     " onclick="window.location='login.jsp'">
<br>
</h2>
</span>
</body>
</html>