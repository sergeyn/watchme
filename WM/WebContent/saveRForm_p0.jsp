<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session="false" %>

<%!
	// creating new message function
	private String createMessage(String Id,String password){
	String line = "================================================" + ((char)10)+((char)13);
	String returnString =   "Hello, here is your login data:" + ((char)10)+((char)13)+((char)10)+((char)13);
	returnString = returnString + "ID: "+ Id + ((char)10)+((char)13)+ "Password :"+ password +((char)10)+((char)13)+((char)10)+((char)13);
	returnString = 
		line + 
		returnString + line +
		"Or go to link: http://pitv.haifa.ac.il/WM/registrationForm_p1.jsp?email="+Id;
	return(returnString);
	}
%>
<%
String yearStr       = request.getParameter("yearF");
String gender        = request.getParameter("genderF");
String educationStr  = request.getParameter("educationF");
String occupationStr = request.getParameter("occupationF");
String email         = request.getParameter("emailF");
String password      = request.getParameter("passF");
	if (
			yearStr == null ||
			gender == null ||
			educationStr == null ||
			email == null 
	){
		response.sendRedirect("registrationForm_p0.jsp");
		return;
	}
int	year = 0;
int edu  = 0;
int occ  = 0;

try {
	year = Integer.parseInt(yearStr);
	edu  = Integer.parseInt(educationStr);
	occ  = Integer.parseInt(occupationStr);
}catch(Exception e){
	System.out.println(e.toString());
	response.sendRedirect("registrationForm_p0.jsp");
	return;
	};
	myData.Profile profile = new myData.Profile();
	profile.setAge(year);
	if (gender.equals("true")) 	profile.setGender("Male");
					else		profile.setGender("Female");
	profile.setEducation(""+edu);
	profile.setOccupation(occ);
	profile.setProfileId(email);
	profile.setCurrentStrategyId(-1);
	profile.setPassword(password);
	profile = myData.CommonFunctions.insertNewProfile(profile);	
	
		if (profile==null) {
		response.sendRedirect("regFailed.jsp?email="+email);
		return;
		}
	// ------ MAILING PROPERTIES ------------------------------------------------------------
	// mail to new registrant
	String from = "admin.watchme@gmail.com";
	String subject = "Watch me - login details";
	String emailContent = createMessage(""+profile.getProfileId(),profile.getPassword());
	myData.GMail mail = new myData.GMail();
	try {
		mail.sendMessage(from,profile.getProfileId(), subject, emailContent);
		response.sendRedirect("regOK.jsp?email="+profile.getProfileId());
		} 
	catch (Exception e)	{
		e.printStackTrace();
		response.sendRedirect("regFailed.jsp");
		return;
		}
	// ------ END OF MAILING PROPERTIES ------------------------------------------------------------
%>
