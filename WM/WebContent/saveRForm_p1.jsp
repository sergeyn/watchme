<%@page import="myData.CommonFunctions"%>
<%@page import="myData.Conversions"%>
<%@page import="myData.Profile "%>
<%@page import="myData.Experiment_groups "%>
<%@page import="java.util.List "%>
<%@page import="java.util.Iterator "%>
<%@page import="java.util.Vector "%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session="false" %>
<%
String answers  = request.getParameter("answersF");
String email    = request.getParameter("emailF");
if (answers == null ){
		response.sendRedirect("login.jsp");
		return;
	}
String[] answersArray = myData.importCSV.organizeString(answers,",");	
//------ answers ------------------------------------------
if (answersArray.length % 2 == 1){
	System.out.println("Wrong data from questions form!");	
	response.sendRedirect("login.jsp");
	return;
	}
int[] answersInt = new int[answersArray.length/2];
for(int i=0;i<answersArray.length;i+=2){
	int qNo = 0;
	int aNo = 0;
	try{
	qNo = Integer.parseInt(answersArray[i+0]);
	aNo = Integer.parseInt(answersArray[i+1]);
	if (qNo!=i/2) throw new Exception("Verification failed!");
	}catch(Exception e){
		System.out.println("Wrong data from questions form!");	
		response.sendRedirect("login.jsp");
		return;
		}
	answersInt[i/2] = aNo;
}
	//--------------
	 List allStrategies = null;
     int[] change = new int[30];
     int[] stability = new int[29];
     double attractionToChange, winceFromChange, attractionToStability, winceFromStability, certainty;
     myData.Strategy currStrategy = new myData.Strategy();
 	 List<Double> x = new Vector<Double>();
    
     allStrategies = CommonFunctions.selectAllStrategies();
     for(int i= 0;i<=29;i++) change[i]=answersInt[i]+1;	
     for(int i=30;i<=58;i++) stability[i-30]=answersInt[i]+1;	
     
     //---------------- findProfileStrategy()
    double min = 10000000;
	List<Double> y = new Vector<Double>();
	
    attractionToChange = calcMean(change, "26,16,17,29,11,7,30,5,9,28,14,24,13,22,18,3,2");
    winceFromChange = calcMean(change, "23,12,27,8,1,4,10,6,25,19,21,15,20");
    attractionToStability = calcMean(stability, "21,26,14,18,7,22,20,4,2,17,23,19");
    winceFromStability = calcMean(stability, "11,6,16,13,1,27,29,3,8,25");
    certainty = calcMean(stability, "28,12,24,5,10,15,9");
    
    x.clear(); //this was causing the trouble 
    x.add(attractionToChange);
    x.add(winceFromChange);
    x.add(attractionToStability);
    x.add(winceFromStability);
    x.add(certainty);
	
    //System.out.println(attractionToChange+" "+winceFromChange+" "+attractionToStability+" "+winceFromStability+" "+certainty);
    
	Iterator iter = allStrategies.iterator();

	while ( iter.hasNext() ) {
		myData.Strategy ai = (myData.Strategy) iter.next();
		y.add(ai.getAttractionToChange());
		y.add(ai.getWinceFromChange());
		y.add(ai.getAttractionToStability());
		y.add(ai.getWinceFromStability());
		y.add(ai.getCertainty());
		double tmpMin = euclideanDistance(x,y);
		if( tmpMin <= min)
		{
			min = tmpMin;
			currStrategy = ai;
		}
		y.clear();
	}
	y = null;
    //---------------- end findProfileStrategy()
    //--------------
	Profile p = new Profile();
	p.setCurrentStrategyId(currStrategy.getStrategyId());
	p.setSw(Conversions.Strategy2Sw(currStrategy.getStrategyId())); //upd. b y [sergey]
	System.out.println("sw: " + p.getSw() );                                                                            
	p.setProfileId(email);
	CommonFunctions.updateStrategyByProfile(p);
	
	//set experiment group
	List experiment_groups = null;
	Experiment_groups experiment_group = null;
	Experiment_groups experiment_group_update = new Experiment_groups();
	
	System.out.println("Profile's strategy " + p.getCurrentStrategyId());
	//
	//experiment_groups = myData.CommonFunctions.selectExperimentGroupsByStrategy(p.getCurrentStrategyId());
	//int minGroup = -1, minValue = 1000;
	
	//if(experiment_groups.isEmpty())
	//	System.out.println("experiment_groups: set experiment group - we are null :-(" );
	//System.out.println("list size : " +experiment_groups.size());
	//for(int i= 0; i<experiment_groups.size();i++){//run through profiles	
	//	experiment_group= (Experiment_groups)experiment_groups.get(i);
	//
	//	System.out.println("experiment group is:" + experiment_group.getGroupId() + " number of participants: " + experiment_group.getNumberOfParticipants());
	//	if(experiment_group.getNumberOfParticipants()<minValue){
	//		minGroup=experiment_group.getGroupId();
	//		minValue=experiment_group.getNumberOfParticipants();
	//	}
//	}
	//p.setExperimentGroupId(minGroup);
	//end set experiment group
	
	//version 2 - explicit set of experiment group == 3
	p.setExperimentGroupId(3);
	CommonFunctions.updateExperimentGroupId(p);
	
	//experiment_group_update.setGroupId(3);
	//experiment_group_update.setNumberOfParticipants(minValue+1);
	//experiment_group_update.setStrategy(p.getCurrentStrategyId());
	//CommonFunctions.updateExperimentGroupCount(experiment_group_update);
	
	
	response.sendRedirect("registrationForm_p2.jsp?email="+email);
	//------------------------------------------------------------------------------------------
%>
<%!
private double calcMean(int[] arr, String indexes)
{
	double result = 0;
	String[] str = indexes.split(",");
	for (int i=0; i<str.length; i++)
	{
		result += arr[Integer.parseInt(str[i])-1];
	}
	return result/str.length;
}

private double euclideanDistance (List x, List y)
{
	double result = 0.00;

	Iterator iterX = x.iterator();
	Iterator iterY = y.iterator(); 
	
	while(iterX.hasNext())
	{
		double arg1 = ((Double)iterX.next()).doubleValue();
		double arg2 = ((Double)iterY.next()).doubleValue();
		result = result + Math.pow( arg1-arg2, 2.00 );
		
	}
	//System.out.println("eucl x="+x+" y="+y+" dist:"+Math.sqrt(result));
	return Math.sqrt(result);
}
%>