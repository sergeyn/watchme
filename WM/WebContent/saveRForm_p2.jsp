<%@page import="myData.Conversions"%>
<%@page import="myData.CommonFunctions"%>
<%@ page import = "java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session="false" %>
<%
String email        = request.getParameter("emailF");
String genres       = request.getParameter("genresF");
	if (myData.CommonFunctions.isExistPGByID(email)){
		response.sendRedirect("page_1.jsp");
		return;
	}

	if (email == null || genres == null){
		response.sendRedirect("login.jsp");
		return;
	}
String[]  genresArray = myData.importCSV.organizeString(genres,",");		
// ------ genres ------------------------------------------
if (genresArray.length % 2 == 1){
	System.out.println("Wrong data from questions form!");	
	response.sendRedirect("questionsForm.jsp");
	return;
	}
int[] genresInt = new int[genresArray.length/2];
for(int i=0;i<genresArray.length;i+=2){
	int qNo = 0;
	int aNo = 0;
	try{
	qNo = Integer.parseInt(genresArray[i+0]);
	aNo = Integer.parseInt(genresArray[i+1]);
	if (qNo!=i/2) throw new Exception("Verification failed!");
	}catch(Exception e){
		System.out.println("Wrong data from questions form!");	
		response.sendRedirect("login.jsp");
		return;
		}
	genresInt[i/2] = aNo;
}
	//------------------------------------------------------------------------------------------
	myData.Demographics demographic = new myData.Demographics() ;
	myData.Profile p = 	CommonFunctions.selectProfileById(email);
	double demographicWeight = -1.0;
	for(int i=0;i<genresInt.length;i++) {
		String demographicRecommenderString ="";
		demographic.setAge(Conversions.ageFromSpecificToGroup(p.getAge()));
		if(p.getGender().equals("Male"))
			demographic.setGender("M");
		else if(p.getGender().equals("Female"))
			demographic.setGender("F");
		demographic.setOccupation(Conversions.occupationFromIntToString(p.getOccupation()));
		demographic.setGenreName(getGenreNameByNo(i));
		int id = CommonFunctions.selectGenreIdByName(getGenreNameByNo(i));
		myData.Profile_genres pg = new myData.Profile_genres();
		pg.setProfileId(email);
		pg.setGenreId(id);
		pg.setDate(new Date());
		pg.setUserWeight(genresInt[i]+1);
		demographicWeight = Conversions.DemographicToWeight(demographicRecommenderString);
		pg.setScore(demographicWeight*Conversions.UserWeightToDouble(genresInt[i]+1));
		System.out.println("demographicWeight"+demographicWeight+",UserWeightToDouble"+Conversions.UserWeightToDouble(genresInt[i]+1));
		String isCurInt = (genresInt[i] > 3)? "Y" : "N" ;
		pg.setIsCurrentInterest(isCurInt);
		demographicRecommenderString = CommonFunctions.selectDemographicRecommender(demographic);
		
		pg.setDemographicWeight(demographicWeight);
		
		double interest = myData.Formulae.interestForOpen(pg.getScore(),0.5,p.getSw(),p.getCurrentStrategyId());
		if (interest==-1) System.out.println("Problem with strategy, interest = -1"); 
		pg.setInterest( interest);
		//TODO: save for each genre its interest and score beta, tn1...

		CommonFunctions.insertPG(pg);
	}
	
	
//	 ------ MAILING PROPERTIES ------------------------------------------------------------
	// mail to new registrant
	String to   = "admin.watchme@gmail.com";
	String from = "admin.watchme@gmail.com";
	String subject = "Watch me (New user added)";
	String emailContent = "We have a new user: " + email;

	myData.GMail mail = new myData.GMail();
	try {
		System.out.println(emailContent);
		mail.sendMessage(from,to, subject, emailContent);
		} 
	catch (Exception e)	{
		e.printStackTrace();
		return;
		}
	// ------ END OF MAILING PROPERTIES ------------------------------------------------------------
	response.sendRedirect("thankYouForReg.jsp");
	//------------------------------------------------------------------------------------------
%>
<%! 

String getGenreNameByNo(int no){
	switch(no){
	case  0: return("Action");
	case  1: return("Adventure");
	case  2: return("Animation");
	case  3: return("Comedy");
	case  4: return("Crime");
	case  5: return("Drama");
	case  6: return("Family");
	case  7: return("Romance");
	case  8: return("Horror");
	case  9: return("Sci-Fi");
	case 10: return("Thriller");
	}
return("");	
}
%>