<%@page import="myData.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "javax.servlet.http.HttpSession" %>
<%@ page session="false" %>
<%
HttpSession user_session = request.getSession(false);
if (user_session==null){
	response.sendRedirect("login.jsp");
	return;
}
int MOVIES_TO_SEE = (Integer)user_session.getAttribute("MOVIES_TO_SEE");
int seenTrailers = (Integer)user_session.getAttribute("Seen");
myData.Profile currentUser = (myData.Profile)user_session.getAttribute("currentUser");
myData.Trailers_of_profile trailer_of_profile = new myData.Trailers_of_profile();


trailer_of_profile.setStartTime(request.getParameter("startTime").trim());
trailer_of_profile.setEndTime(request.getParameter("endTime").trim());

int ratingData = Integer.parseInt(request.getParameter("Rating").trim());
trailer_of_profile.setRating(ratingData % 10);
trailer_of_profile.setWasSeen((ratingData / 10) % 10 == 0);
trailer_of_profile.setWasRecommended((ratingData / 100) == 0);
trailer_of_profile.setTrailerId(request.getParameter("TrailerId").trim());
trailer_of_profile.setProfileId(currentUser.getProfileId());
//TODO: ne 0! [Ronen, blin]
// to check if it a matter of simple offline script or stored procedure
//otherwise - do it here
trailer_of_profile.setActualWatchingTime(0);

String currentTrailerId = request.getParameter("TrailerId").trim(); 
double rating = Conversions.UserRatingToDouble(ratingData % 10);

//Check if there is need in learning:

	if (currentUser.getExperimentGroupId()>0){
		Learning_support learning = new Learning_support(currentUser.getProfileId(),currentTrailerId, rating, currentUser.getSw());
		learning.learnAndUpdateDB();
	}
//TODO: check if Ronen did selects from here [P&S]
//either we write there too or use Profile_Trailer_Genres within the selects                                            
//Save into trailers_of_profile
myData.CommonFunctions.insertTOP(trailer_of_profile);
seenTrailers++;
user_session.setAttribute("Seen",seenTrailers);
if (seenTrailers>=MOVIES_TO_SEE) response.sendRedirect("page_1.jsp");
							else response.sendRedirect("page_2.jsp");
%>