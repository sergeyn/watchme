<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Simulation</title>

<link href="jsSlider/default.css" rel="stylesheet" type="text/css" >
<script type = "text/javascript" src = "jsSlider/slider.js"></script>

<script type="text/javascript">

var lastSelected = 0;

function submitForm(){
document.getElementById('loafingImg').style.visibility='visible';

switch(lastSelected){
case 0: window.location='simulationResultsWGT.jsp';break;
case 1: window.location='simulationResultsCasc.jsp';break;
case 2: window.location='simulationResultsAug.jsp';break;
case 3: window.location='simulationResultsWS.jsp';break;
}


/*
document.getElementById('hybrTypeF').value=lastSelected;
switch(lastSelected){
case 0: document.getElementById('myForm').action='simulationResultsWS.jsp';break;
case 1: document.getElementById('myForm').action='simulationResultsCasc.jsp';break;
case 2: document.getElementById('myForm').action='simulationResultsAug.jsp';break;
case 3: document.getElementById('myForm').action='simulationResultsWS.jsp';break;
}

document.getElementById('sederF').value='';



if (lastSelected==3)
	document.getElementById('afterDays').value=document.getElementById('aftDays').value;


if (lastSelected==0){
	document.getElementById('weighted1F').value = document.getElementById('weightedInp').value;
	document.getElementById('weighted2F').value = document.getElementById('learningInp').value;
	document.getElementById('weighted3F').value = document.getElementById('cascInp').value;
}else if (lastSelected==3){
	document.getElementById('switched1F').value = document.getElementById('weightedInpS').value;
	document.getElementById('switched2F').value = document.getElementById('learningInpS').value;
	document.getElementById('switched3F').value = document.getElementById('cascInpS').value;
}


document.getElementById('myForm').submit();

*/


}




function selectHM(no){

lastSelected = no;
for(i=0;i<4;i++)
	document.getElementById('hm'+i).style.background='rgb(57,79,101)';

document.getElementById('hm'+no).style.background='blue';

	var inputBlock = document.getElementById('inputBlock');

	switch (no) {
		case 0: inputBlock.innerHTML= document.getElementById('weighted').value;	break;
		case 1: inputBlock.innerHTML= document.getElementById('cascade').value;	break;
		case 2: inputBlock.innerHTML= document.getElementById('cascade').value;	break;
		case 3: inputBlock.innerHTML= document.getElementById('switching').value;	break;

	}

}

var selectedOption = 0;

function switchSelector(no){
				
				var sel1 = document.getElementById('sel1');
				var sel2 = document.getElementById('seltd2');
				var sel3 = document.getElementById('seltd3');								
				
			
				if (no==1)
					
					switch (sel1.options[sel1.selectedIndex].value*1) {
						
						case 0: 
							sel2.innerHTML="<select onchange='switchSelector(2);' id='sel2'><option value='1'>Learning</option><option value='2'>Collaborative</option></select>";
							sel3.innerHTML="<select onchange='switchSelector(3);' id='sel3'><option value='2'>Collaborative</option></select>";
							selectedOption = 0;
						break;
						
						case 1: 
							sel2.innerHTML="<select onchange='switchSelector(2);' id='sel2'><option value='0'>Demographic</option><option value='2'>Colaborative</option></select>";
							sel3.innerHTML="<select onchange='switchSelector(3);' id='sel3'><option value='2'>Collaborative</option></select>";
							selectedOption = 1;
						break;
						
						case 2: 
							sel2.innerHTML="<select onchange='switchSelector(2);' id='sel2'><option value='0'>Demographic</option><option value='1'>Learning</option></select>";
							sel3.innerHTML="<select onchange='switchSelector(3);' id='sel3'><option value='1'>Learning</option></select>";
							selectedOption = 2;
						break;
					
					}
				else{
					sel2 = document.getElementById('sel2');;
					
					if (sel1.options[sel1.selectedIndex].value*1==0 && sel2.options[sel2.selectedIndex].value*1==1)
					{sel3.innerHTML="<select ><option>Collaborative</option></select>";
					selectedOption = 0;
					}
					
					if (sel1.options[sel1.selectedIndex].value*1==0 && sel2.options[sel2.selectedIndex].value*1==2)
					{sel3.innerHTML="<select ><option>Learning</option></select>";
					selectedOption = 1;
					}
					
					if (sel1.options[sel1.selectedIndex].value*1==1 && sel2.options[sel2.selectedIndex].value*1==0)
					{sel3.innerHTML="<select ><option>Collaborative</option></select>";
					selectedOption = 2;
					}
										
					if (sel1.options[sel1.selectedIndex].value*1==1 && sel2.options[sel2.selectedIndex].value*1==2)
					{sel3.innerHTML="<select><option>Demographic</option></select>";
					selectedOption = 3;
					}
					
					if (sel1.options[sel1.selectedIndex].value*1==2 && sel2.options[sel2.selectedIndex].value*1==0)
					{sel3.innerHTML="<select ><option>Learning</option></select>";
					selectedOption = 4;
					}
					
					if (sel1.options[sel1.selectedIndex].value*1==2 && sel2.options[sel2.selectedIndex].value*1==1)
					{sel3.innerHTML="<select ><option>Demographic</option></select>";
					selectedOption = 5;
					}
					
					}
			}

</script>

</head>
<body dir="ltr">

<form method="post" id='myForm'>
<input type="hidden" id='hybrTypeF'>
<input type="hidden" id='sederF'>
<input type="hidden" id='weighted1F'>
<input type="hidden" id='weighted2F'>
<input type="hidden" id='weighted3F'>
<input type="hidden" id='switched1F'>
<input type="hidden" id='switched2F'>
<input type="hidden" id='switched3F'>
<input type="hidden" id='afterDaysF'>
</form>


<table  cellpadding="0" cellspacing="0" border="0">
<tr><td><img src="images/tv_header.jpg"></td></tr>

<tr>
	<td   colspan="10" style='background-image: url("images/tv_middle.jpg");background-repeat: repeat-y;'>
	<h1  align="center" style="color: white;"><u>Simulation<br><br></u></h1>		
		<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=1 bordercolor="white" >
			
			
			
			<tr style="font-size: medium; font: bold" align="center">
				<td>
				&nbsp;
				Amount of 
				&nbsp;
				</td>
				<td>
				&nbsp;
				Strategy 1
				&nbsp;
				</td>
				<td>
				&nbsp;
				Strategy 2
				&nbsp;
				</td>
				<td>
				&nbsp;
				<br>
				Strategy 3
				<br>
				&nbsp;
				</td>
			</tr>
			<tr>
				<td align="center">
				Users:
				</td>
				<td>
					<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider1"
			display="display1"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display1"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="10000"
			valuecount="101"
			decimals="0"
			value="1000" />

	</div>
	<!-- end slider -->

				</td>
				<td>
	<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider2"
			display="display2"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display2"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="10000"
			valuecount="101"
			decimals="0"
			value="1000" />

	</div>
	<!-- end slider -->

				</td>
				<td>
	<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider3"
			display="display3"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display3"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="10000"
			valuecount="101"
			decimals="0"
			value="1000" />

	</div>
	<!-- end slider -->

				</td>
				
			</tr>
			
			<tr>
			
				<td align="center">
				&nbsp;
				Current interest genres:
				&nbsp;
				</td>
				<td>
			<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider4"
			display="display4"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display4"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="11"
			valuecount="12"
			decimals="0"
			value="4" />

	</div>
	<!-- end slider -->

				</td>
				<td>
		<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider5"
			display="display5"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display5"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="11"
			valuecount="12"
			decimals="0"
			value="4" />

	</div>
	<!-- end slider -->

				</td>
				<td>
		<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider6"
			display="display6"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display6"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="11"
			valuecount="12"
			decimals="0"
			value="4" />

	</div>
	<!-- end slider -->

				</td>
				
			</tr>
</table>
<br>
<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=1 bordercolor="white" >
			
			
			
			<tr style="font-size: medium; font: bold" align="center">
				<td >
				&nbsp;
				Total amount<br>&nbsp;&nbsp;&nbsp;&nbsp;of partisipants
				&nbsp;
				</td>
				<td>
				&nbsp;
				Days of experiment
				&nbsp;
				</td>
			</tr>
			<tr>
				<td align="center">
					<b>3000</b>

				</td>
				<td>
	<!--  slider -->
	<div class="carpe_horizontal_slider_track" style="background-color: #bcd; border-color: #def #9ab #9ab #def;">

	<div class="carpe_slider_slit" style="background-color: #003; border-color: #99b #ddf #ddf #99b;">&nbsp;</div>
		<!-- Default position: 50px -->
	<div
			class="carpe_slider"
			id="slider8"
			display="display8"
			style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c; height:12px;">&nbsp;</div>
	</div>

	<div class="carpe_slider_display_holder" style=" background-color: #bcd; border-color: #def #9ab #9ab #def;">
		<!-- Default value: 0.2 -->

		<input

			class="carpe_slider_display"
			id="display8"
			style="background-color: #bcd; color: #258; height:12px; width: 45px; "
			type="text"
			from="0"
			to="100"
			valuecount="101"
			decimals="0"
			value="50" />

	</div>
	<!-- end slider -->

				</td>
							
			</tr>
			

</table>
<br><br><br>
<h2  align="center" style="color: white;"><u>Hybridization methods</u></h2>		

		<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=1 bordercolor="white" >
			
			<tr style="font-size: medium; font: bold" align="center">
				<td id='hm0' onclick="selectHM(0);" style="cursor: pointer;background: blue;">
				<br>
				&nbsp;
				Weighted
				&nbsp;
				<br>
				&nbsp;
				<br>
				</td>
				<td id='hm1' onclick="selectHM(1);" style="cursor: pointer;">
				&nbsp;
				Cascade
				&nbsp;
				</td>
				<td id='hm2' onclick="selectHM(2);" style="cursor: pointer;">
				&nbsp;
				Augumentation
				&nbsp;
				</td>
				<td id='hm3' onclick="selectHM(3);"style="cursor: pointer;">
				&nbsp;
				Switching
				&nbsp;
				</td>
			</tr>
			<tr style="height: 6px;">
			<td  colspan="4" style="  background-color: white;"></td>
			</tr>
			<tr>
			<td colspan="4" id='inputBlock'>
			
			
			
			
			<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=1 bordercolor="white" >
					<tr style="font-size: medium; font: bold" align="center">
					<td colspan = '3'>
					<br>
					Adjustable weights
					<br><br> 
					</td>
					</tr>

					<tr align="center">
					<td>
					<br>
					&nbsp;
					Demographic
					&nbsp;
					<br>
					&nbsp;
					<br>
					</td>
					<td>
					&nbsp;
					Learning
					&nbsp;
					</td>
					<td>
					&nbsp;
					Collaborative
					&nbsp;
					</td>
					</tr>
					
					<tr>
					<td >
					<input type="text" value="0.3" readonly>
					</td>
					<td>
					<input type="text" value="0.3" readonly>
					</td>
					<td>
					<input type="text" value="0.4" readonly>
					</td>
					</tr>
					
				</table>
			
			
			
			
			</td>
			
			
			</tr>
						

</table>
<br><br><br>
<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=0 bordercolor="white" >
<tr><td colspan="2" align="center"><img id='loafingImg' src="images/simulationLoading.gif" style="visibility: hidden;";> </td></tr>
<tr>
<td><input type="button" value='Start simulation' onclick="submitForm();"></td>
<td>&nbsp;</td>
</tr>

</table>



	</td>
</tr>

<tr><td> <img src="images/tv_footer.jpg"></td></tr>
</table>

<textarea rows="1" cols="1" style="visibility: hidden;" id='weighted' >
				<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=1 bordercolor="white" >
					<tr style="font-size: medium; font: bold" align="center">
					<td colspan = '3'>
					<br>
					Adjustable weights
					<br><br> 
					</td>
					</tr>

					<tr align="center">
					<td>
					<br>
					&nbsp;
					Demographic
					&nbsp;
					<br>
					&nbsp;
					<br>
					</td>
					<td>
					&nbsp;
					Learning
					&nbsp;
					</td>
					<td>
					&nbsp;
					Collaborative
					&nbsp;
					</td>
					</tr>
					
					<tr>
					<td >
					<input type="text" readonly="readonly" value="0.3" id='weightedInp'>
					</td>
					<td>
					<input type="text" readonly value="0.3" id='learningInp'>
					</td>
					<td> 
					<input type="text" readonly value="0.4" id='cascInp'>
					</td>
					</tr>
					
				</table>

</textarea>

<textarea rows="1" cols="1" style="visibility: hidden;" id='switching'>
				<table align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=1 bordercolor="white" >
					<tr style="font-size: medium; font: bold" align="center">
					<td colspan = '3'>
					<br>
					Adjustable weights
					<br><br> 
					</td>
					</tr>

					<tr align="center">
					<td>
					<br>
					&nbsp;
					Demographic
					&nbsp;
					<br>
					&nbsp;
					<br>
					</td>
					<td>
					&nbsp;
					Learning
					&nbsp;
					</td>
					<td>
					&nbsp;
					Collaborative
					&nbsp;
					</td>
					</tr>
					
					<tr>
					<td >
					<input type="text" readonly value="0.3" id='weightedInp'>
					</td>
					<td>
					<input type="text" readonly value="0.3" id='learningInp'>
					</td>
					<td>
					<input type="text" readonly value="0.4" id='cascInp'>
					</td>
					</tr>
					
					<tr>
					<td colspan="3" align="center"><br> After <Input type="text" style="width: 30px" id='aftDays' value='20' readonly="readonly" > days<br><br></td>
					</tr>
					
					<tr>
					<td >
					<input type="text" readonly value="0.2" id='weightedInpS'>
					</td>
					<td>
					<input type="text" readonly value="0.2" id='learningInpS'>
					</td>
					<td>
					<input type="text" readonly value="0.4" id='cascInpS'>
					</td>
					</tr>
					
				</table>


</textarea>

<textarea rows="1" cols="1" style="visibility: hidden;" id='cascade' >
				<table width="100%" align="center"  cellpadding="0" cellspacing="0" style="color: white;" border=0 bordercolor="white" >
				
				<tr style="font-size: medium; font: bold" align="center">
				<td colspan="6"><h2> Techniques order	</h2></td>
				</tr>
					
					
					
				<tr style="font-size: medium; font: bold" align="center">

				<td> 
				
				<select id='sel1' onchange ='switchSelector(1);'>
					<option value='0'>Demographic</option>
					<option value='1'>Learning</option>
					<option value='2'>Collaborative</option>

				</select>
				
				</td>
				<td> <b style=' font-size: x-large'>&rarr;</b>	 </td>

				<td id='seltd2'> 
				<select>
				<option>Learning</option>
				<option>Collaborative</option>
				</select>
				</td>
				<td><b style=' font-size: x-large'>&rarr;</b> </td>
				<td id='seltd3'> 
				<select>
				<option>Collaborative</option>
				</select>
				
				</td>
				<td>&nbsp;<br><br>&nbsp;</td>
				</tr>
					
					
				</table>


</textarea>



</body>
</html>