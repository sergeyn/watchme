<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Simulation</title>

<link href="jsSlider/default.css" rel="stylesheet" type="text/css" >

</head>
<body dir="ltr">
<table  cellpadding="0" cellspacing="0" border="0">
<tr><td><img src="images/tv_header.jpg"></td></tr>

<tr>
	<td   colspan="10" style='background-image: url("images/tv_middle.jpg");background-repeat: repeat-y;'>
	
	<h2 align="center" style="color: white;">
	<br>
	Simulation of cascade hybridization method.
	<br></h2>		

<%

final int    totalGenres = 11;
final double limit = 0.5;




class interests {
	String genre = "";
	double interest = 0;
	boolean increased = false;
	
	public interests(String genre, double interest){
		this.genre = genre;
		this.interest = interest;
	}

	public void setIncreased(boolean incr){this.increased=incr;}
	public boolean getIncreased(){return increased;}
	public String getGenre(){return genre;}
	public double getInterest(){return interest;}

	public String getInterestStr(){
		String str = String.valueOf(interest+0.00000001);
		int point = str.indexOf(".");
		str = str.substring(0,point+3);
		return (str);
		}
	
	public String getDoubleAsStr(double dbl) {
		String str = String.valueOf(dbl);
		int point = str.indexOf(".");
		str = str.substring(0,point+3);
		return (str);
		}

	
	public void setInterest(double interest){
		if (interest>1) interest=1;
		this.interest=interest;
		}
	
	public void arrangeInterests(interests[] arr){
		boolean	hasChanges=true;
		while (hasChanges){
			hasChanges = false;
			for(int i=0;i<arr.length-1;i++)
				if 	(arr[i].getInterest()<arr[i+1].getInterest()){
					interests temp = arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=temp;
					hasChanges=true;
				}
			
		}
	}
	
}


int option = 0;

String[] methods = new String[3];

switch (option) {
	case 0: {methods[0]="Demographic";methods[1]="Learning";methods[2]="Collaborative"; break;}
	case 1: {methods[0]="Demographic";methods[1]="Collaborative";methods[2]="Learning"; break;}
	case 2: {methods[0]="Learning";methods[1]="Demographic";methods[2]="Collaborative"; break;}
	case 3: {methods[0]="Learning";methods[1]="Collaborative";methods[2]="Demographic"; break;}
	case 4: {methods[0]="Collaborative";methods[1]="Demographic";methods[2]="Learning"; break;}
	case 5: {methods[0]="Collaborative";methods[1]="Learning";methods[2]="Demographic"; break;}

}



// -- create random vectors --

double[] demographicVector = new double[totalGenres];
double[] learningVector = new double[totalGenres];
double[] collaborativeVector = new double[totalGenres];

for(int i=0;i<totalGenres;i++){
	demographicVector[i]=Math.random()+0.5;
	learningVector[i]=Math.random()+0.5;
	collaborativeVector[i]=Math.random()+0.5;
	}
// -----------------------------


// -- filling current interest --

interests[] genresData    = new interests[totalGenres];
genresData[0] = new interests("Action",Math.random());
genresData[1] = new interests("Thriller",Math.random());
genresData[2] = new interests("Crime",Math.random());
genresData[3] = new interests("Drama",Math.random());
genresData[4] = new interests("Horror",Math.random());
genresData[5] = new interests("Adventure",Math.random());
genresData[6] = new interests("Comedy",Math.random());
genresData[7] = new interests("Romance",Math.random());
genresData[8] = new interests("Family",Math.random());
genresData[9] = new interests("Sci-Fi",Math.random());
genresData[10]= new interests("Animation",Math.random());
genresData[0].arrangeInterests(genresData);
//-----------------------------





%>


<table border="1" align="center" width="78%">
<tr>
<td>
<h4 style='color: white' align="center">&nbsp;Current interest&nbsp;</h4>
<table style='color: white' align="center">
<thead align="center">
<tr>
	<td><h3><u>Genre</u></h3></td>
	<td><h3><u>Weight</u></h3></td>
</tr>
</thead>

<tbody align="center">
<% for (int i=0;i<genresData.length;i++) { 
		if (genresData[i].getInterest()<limit) 
			out.println("<tr><td><s style='color:gray;'>"+genresData[i].getGenre()+"</s></td><td><s style='color:gray;'>"+genresData[i]. getInterestStr()+"</s></td></tr>");
			else
			out.println("<tr><td><b>"+genresData[i].getGenre()+"</b></td><td><b>"+genresData[i]. getInterestStr()+"</b><td></tr>");
} %>
</tbody>
<tfoot></tfoot>
</table>



</td>

<td>
<h4 style='color: white' align="center">&nbsp; <%=methods[0] %> <font style='font-size: 20px;'>&rarr;</font>&nbsp;</h4>
<table style='color: white' align="center">
<thead align="center">
<tr>
	<td><h3><u>Genre</u></h3></td>
	<td><h3><u>Weight</u></h3></td>
</tr>
</thead>

<tbody align="center">
<% 
	for (int i=0;i<genresData.length;i++){
		if (genresData[i].getInterest()>limit){
		genresData[i].setInterest(genresData[i].getInterest()*demographicVector[i]);
		genresData[i].setIncreased(demographicVector[i]>1);
		}
	}

	genresData[0].arrangeInterests(genresData);
	
	String arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
	for (int i=0;i<genresData.length;i++) { 
		arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
		if (!genresData[i].getIncreased()) 
			arrow="<b style='color:red;'>&nbsp;&nbsp;&nbsp;&nbsp;&darr;</b>";
		
		if (genresData[i].getInterest()<limit) 
			out.println("<tr><td><s style='color:gray;'>"+genresData[i].getGenre()+"</s></td><td><s style='color:gray;'>"+genresData[i]. getInterestStr()+"</s></td></tr>");
			else
			out.println("<tr><td><b>"+genresData[i].getGenre()+"</b></td><td><b>"+genresData[i]. getInterestStr()+"</b>"+arrow+"<td></tr>");
} %>
</tbody>
<tfoot></tfoot>
</table>


</td>

<td>
<h4 style='color: white' align="center">&nbsp;<%=methods[1] %> <font style='font-size: 20px;'>&rarr;</font> &nbsp;</h4>
<table style='color: white' align="center">
<thead align="center">
<tr>
	<td><h3><u>Genre</u></h3></td>
	<td><h3><u>Weight</u></h3></td>
</tr>
</thead>

<tbody align="center">
<% 
	for (int i=0;i<genresData.length;i++){
		if (genresData[i].getInterest()>limit){
		genresData[i].setInterest(genresData[i].getInterest()*learningVector[i]);
		genresData[i].setIncreased(learningVector[i]>1);
		}
	}

genresData[0].arrangeInterests(genresData);
	

	for (int i=0;i<genresData.length;i++) { 
		arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
		if (!genresData[i].getIncreased()) arrow="<b style='color:red;'>&nbsp;&nbsp;&nbsp;&nbsp;&darr;</b>";
		
		if (genresData[i].getInterest()<limit) 
			out.println("<tr><td><s style='color:gray;'>"+genresData[i].getGenre()+"</s></td><td><s style='color:gray;'>"+genresData[i]. getInterestStr()+"</s></td></tr>");
			else
			out.println("<tr><td><b>"+genresData[i].getGenre()+"</b></td><td><b>"+genresData[i]. getInterestStr()+"</b>"+arrow+"<td></tr>");
} %>
</tbody>
<tfoot></tfoot>
</table>


</td>


<td>
<h4 style='color: white' align="center">&nbsp;<%=methods[2] %> <font style='font-size: 20px;'>&rarr;</font> <b style='color:White; font-size: 25px;'>&nbsp;&nbsp;&radic;</b></h4>
<table style='color: white' align="center">
<thead align="center">
<tr>
	<td><h3><u>Genre</u></h3></td>
	<td><h3><u>Weight</u></h3></td>
</tr>
</thead>

<tbody align="center">
<% 
	for (int i=0;i<genresData.length;i++){
		if (genresData[i].getInterest()>limit){
			genresData[i].setInterest(genresData[i].getInterest()*collaborativeVector[i]);
			genresData[i].setIncreased(collaborativeVector[i]>1);
		}
	}

genresData[0].arrangeInterests(genresData);
	

	for (int i=0;i<genresData.length;i++) { 
		arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
		if (!genresData[i].getIncreased()) arrow="<b style='color:red;'>&nbsp;&nbsp;&nbsp;&nbsp;&darr;</b>";
		
		if (genresData[i].getInterest()<limit) 
			out.println("<tr><td><s style='color:gray;'>"+genresData[i].getGenre()+"</s></td><td><s style='color:gray;'>"+genresData[i]. getInterestStr()+"</s></td></tr>");
			else
			out.println("<tr><td><b>"+genresData[i].getGenre()+"</b></td><td style='background-color: blue;'><b>"+genresData[i]. getInterestStr()+"&radic;</b>"+arrow+"<td></tr>");
} %>
</tbody>
<tfoot></tfoot>
</table>


</td>
<!--     end demographic, learning       -->
</tr>
</table>
<br><br><br><div align="center"><input type="button" value="Back" onclick="window.location='simulation.jsp'"></div>

	</td>
</tr>

<tr><td> <img src="images/tv_footer.jpg"></td></tr>
</table>


</body>
</html>