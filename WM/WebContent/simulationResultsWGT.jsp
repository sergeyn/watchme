<%@ page language="java" contentType="text/html; charset=windows-1255"  pageEncoding="windows-1255"%>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Simulation</title>

<link href="jsSlider/default.css" rel="stylesheet" type="text/css" >

</head>
<body dir="ltr">
<table  cellpadding="0" cellspacing="0" border="0">
<tr><td><img src="images/tv_header.jpg"></td></tr>

<tr>
	<td   colspan="10" style='background-image: url("images/tv_middle.jpg");background-repeat: repeat-y;'>
	
	<h2 align="center" style="color: white;">
	<br>
	Simulation of weighted hybridization method.
	<br></h2>		

<%

final int    totalGenres = 11;
final double limit = 0.5;


class interests {
	String genre = "";
	double interest = 0;
	boolean increased = false;
	
	public interests(String genre, double interest){
		this.genre = genre;
		this.interest = interest;
	}

	public void setIncreased(boolean incr){this.increased=incr;}
	public boolean getIncreased(){return increased;}
	public String getGenre(){return genre;}
	public double getInterest(){return interest;}

	public String getInterestStr(){
		String str = String.valueOf(interest+0.00000001);
		int point = str.indexOf(".");
		str = str.substring(0,point+3);
		return (str);
		}
	
	public String getDoubleAsStr(double dbl) {
		String str = String.valueOf(dbl+0.00000001);
		int point = str.indexOf(".");
		str = str.substring(0,point+3);
		return (str);
		}

	
	public void setInterest(double interest){
		if (interest>1) interest=1;
		this.interest=interest;
		}
	
	public void arrangeInterests(interests[] arr){
		boolean	hasChanges=true;
		while (hasChanges){
			hasChanges = false;
			for(int i=0;i<arr.length-1;i++)
				if 	(arr[i].getInterest()<arr[i+1].getInterest()){
					interests temp = arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=temp;
					hasChanges=true;
				}
			
		}
	}
	
}


// -- create random vectors --

double[] demographicVector = new double[totalGenres];
double[] learningVector = new double[totalGenres];
double[] collaborativeVector = new double[totalGenres];

for(int i=0;i<totalGenres;i++){
	demographicVector[i]=Math.random()+0.5;
	learningVector[i]=Math.random()+0.5;
	collaborativeVector[i]=Math.random()+0.5;
	}
// -----------------------------


// -- filling current interest --

interests[] genresData    = new interests[totalGenres];
genresData[0] = new interests("Action",Math.random());
genresData[1] = new interests("Thriller",Math.random());
genresData[2] = new interests("Crime",Math.random());
genresData[3] = new interests("Drama",Math.random());
genresData[4] = new interests("Horror",Math.random());
genresData[5] = new interests("Adventure",Math.random());
genresData[6] = new interests("Comedy",Math.random());
genresData[7] = new interests("Romance",Math.random());
genresData[8] = new interests("Family",Math.random());
genresData[9] = new interests("Sci-Fi",Math.random());
genresData[10]= new interests("Animation",Math.random());
genresData[0].arrangeInterests(genresData);
//-----------------------------


// -- preparing variables for results of each method -- 
interests[] demographic   = new interests[totalGenres];
interests[] learning 	  = new interests[totalGenres];
interests[] collaborative = new interests[totalGenres];

for(int i=0;i<totalGenres;i++){
	demographic[i]    = new interests(genresData[i].getGenre(),genresData[i].getInterest());
	learning[i] 	  = new interests(genresData[i].getGenre(),genresData[i].getInterest());
	collaborative[i]  = new interests(genresData[i].getGenre(),genresData[i].getInterest());
	}
//-----------------------------

	for (int i=0;i<demographic.length;i++){
		demographic[i].setInterest(demographic[i].getInterest()*demographicVector[i]);
		demographic[i].setIncreased(demographicVector[i]>1);
	}

	genresData[0].arrangeInterests(demographic);
	
	for (int i=0;i<learning.length;i++){
		learning[i].setInterest(learning[i].getInterest()*learningVector[i]);
		learning[i].setIncreased(learningVector[i]>1);
	}

	learning[0].arrangeInterests(learning);
	
	for (int i=0;i<collaborative.length;i++){
		collaborative[i].setInterest(collaborative[i].getInterest()*collaborativeVector[i]);
		collaborative[i].setIncreased(collaborativeVector[i]>1);
	}

	collaborative[0].arrangeInterests(collaborative);
	
 %>
<!--     end learning, collaborative       -->

<br><br>

<table border="1" align="center">
<tr>
<td>
<h2 style='color: white' align="center"><br><br>&nbsp;Current interest&nbsp;<br><br><br><br></h2>
<table style='color: white' align="center">
<thead align="center">
<tr>
	<td><h3><u><br>Genre</u></h3></td>
	<td><h3><u><br>Weight</u></h3></td>
</tr>
</thead>

<tbody align="center">
<% for (int i=0;i<genresData.length;i++) { 
		if (genresData[i].getInterest()<limit) 
			out.println("<tr><td><s style='color:gray;'>"+genresData[i].getGenre()+"</s></td><td><s style='color:gray;'>"+genresData[i]. getInterestStr()+"</s></td></tr>");
			else
			out.println("<tr><td><b>"+genresData[i].getGenre()+"</b></td><td><b>"+genresData[i]. getInterestStr()+"</b><td></tr>");
} %>
</tbody>
<tfoot></tfoot>
</table>



</td>
<td>
<h2 style='color: white' align="center">&nbsp;Hybridization&nbsp;</h2>
<table style='color: white' align="center">
<thead align="center">
<tr>
	<td><h3>D * 0.3 + </h3></td>
	<td><h3>L * 0.3 + </h3></td>
	<td><h3>C * 0.4 </h3></td>
	<td colspan=2><h3>= Weighted</h3></td>

</tr>



<tr>
	<td><h3><u>Demographic<br>weights</u></h3></td>
	<td><h3><u>Learning<br>weights</u></h3></td>
	<td><h3><u>Collaborative<br>weights</u></h3></td>
	<td><h3><u>Recom.<br>weighted</u></h3></td>

</tr>
</thead>

<tbody align="center">
<% 

double demographicW=0;
double learningW=0;
double collaborativeW=0;

double demWW=0.3;
double learWW=0.3;
double colWW=0.3;

double demSW=0.4;
double learSW=0.4;
double colSW=0.2;


interests t = genresData[0];



for(int j=0;j<genresData.length;j++){

	
	out.println("<tr>");
	for(int i=0;i<genresData.length;i++){
		if (demographic[i].getGenre().equals(genresData[j].getGenre())) demographicW=demographic[i].getInterest();
		if (learning[i].getGenre().equals(genresData[j].getGenre())) learningW=learning[i].getInterest();
		if (collaborative[i].getGenre().equals(genresData[j].getGenre())) collaborativeW=collaborative[i].getInterest();
	}


	String arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
	if (genresData[j].getInterest()>demographicW) arrow="<b style='color:red;'>&nbsp;&nbsp;&nbsp;&nbsp;&darr;</b>";

		if (demographicW<limit) 
			out.println("<td><s style='color:gray;'>"+	t.getDoubleAsStr(demographicW)+"</s></td>");
			else
			out.println("<td><b>"+	t.getDoubleAsStr(demographicW)+" "+arrow+"</b></td>");
		
		arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
		if (genresData[j].getInterest()>learningW) arrow="<b style='color:red;'>&nbsp;&nbsp;&nbsp;&nbsp;&darr;</b>";

		if (learningW<limit) 
			out.println("<td><s style='color:gray;'>"+t.getDoubleAsStr(learningW)+"</s></td>");
			else
			out.println("<td><b>"+t.getDoubleAsStr(learningW)+" "+arrow+"</b></td>");
		

		arrow = "<b style='color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;&uarr;<b>";
		if (genresData[j].getInterest()>collaborativeW) arrow="<b style='color:red;'>&nbsp;&nbsp;&nbsp;&nbsp;&darr;</b>";

		
		if (collaborativeW<limit) 
			out.println("<td><s style='color:gray;'>"+t.getDoubleAsStr(collaborativeW)+"</s></td>");
			else
			out.println("<td><b>"+t.getDoubleAsStr(collaborativeW)+" "+arrow+"</b></td>");

		
		double ww = collaborativeW*colWW+demographicW*demWW+learningW*learWW;

		if (ww<limit) 
			out.println("<td><s style='color:gray;'>"+t.getDoubleAsStr(ww)+"</s></td>");
			else
			out.println("<td style='background-color: blue;'><b>"+t.getDoubleAsStr(ww)+"&radic;</b></td>");

	

	out.println("</tr>");
		
}		
%>

</tbody>
<tfoot></tfoot>
</table>


</td>

</tr>

</table>


<br><br><br><div align="center"><input type="button" value="Back" onclick="window.location='simulation.jsp'"></div>

	</td>
</tr>

<tr><td> <img src="images/tv_footer.jpg"></td></tr>
</table>


</body>
</html>