package collaborative;
import java.util.List;
//import java.util.ArrayList;

public class Centroid {
	
	private Double [] m_genreRatings;
	private List <Double> m_distances;
	private List <Boolean> m_areMembers;//for member i, indicates whether user i is in cluster
	/**
	 * @return the m_areMembers
	 */
	public List<Boolean> getAreMembers() {
		return m_areMembers;
	}
	/**
	 * @param members the m_areMembers to set
	 */
	public void setAreMembers(List<Boolean> members) {
		m_areMembers = members;
	}
	/**
	 * @return the m_distances
	 */
	public List<Double> getDistances() {
		return m_distances;
	}
	/**
	 * @param m_distances the m_distances to set
	 */
	public void setDistances(List<Double> m_distances) {
		this.m_distances = m_distances;
	}
	/**
	 * @return the m_genreRatings
	 */
	public Double[] getGenreRatings() {
		return m_genreRatings;
	}
	/**
	 * @param ratings the m_genreRatings to set
	 */
	public void setGenreRatings(Double[] ratings) {
		m_genreRatings = ratings;
	}
	
	
}
