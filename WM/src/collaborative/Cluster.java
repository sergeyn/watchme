package collaborative;
import collaborative.User;
import java.util.*;

public class Cluster {
	
	private int m_numOfUsers=0;
	private int m_clusterNum=0;
	private List<Integer> m_memberIndexesList;
	private Double [] m_genreRatings;
	private List <Double> m_distances;
	
	Cluster(int clusterNumber)
	{
		 m_clusterNum=clusterNumber;
		 m_numOfUsers=0;
	}

 /**
	 * @return the m_clusterNum
	 */
	public int getClusterNum() {
		return m_clusterNum;
	}

	/**
	 * @param num the m_clusterNum to set
	 */
	public void setClusterNum(int num) {
		m_clusterNum = num;
	}

	/**
	 * @return the m_distances
	 */
	public List<Double> getDistances() {
		return m_distances;
	}

	/**
	 * @param m_distances the m_distances to set
	 */
	public void setDistances(List<Double> m_distances) {
		this.m_distances = m_distances;
	}

	/**
	 * @return the m_genreRatings
	 */
	public Double[] getGenreRatings() {
		return m_genreRatings;
	}

	/**
	 * @param ratings the m_genreRatings to set
	 */
	public void setGenreRatings(Double[] ratings) {
		m_genreRatings = ratings;
	}

	/**
	 * @return the m_memberIndexesList
	 */
	public List<Integer> getMemberIndexesList() {
		return m_memberIndexesList;
	}

	/**
	 * @param indexesList the m_memberIndexesList to set
	 */
	public void setMemberIndexesList(List<Integer> indexesList) {
		m_memberIndexesList = indexesList;
	}

	/**
	 * @return the m_numOfUsers
	 */
	public int getNumOfUsers() {
		return m_numOfUsers;
	}

	/**
	 * @param ofUsers the m_numOfUsers to set
	 */
	public void setNumOfUsers(int ofUsers) {
		m_numOfUsers = ofUsers;
	}
 
}
