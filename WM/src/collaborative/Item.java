package collaborative;
import java.util.*;
import collaborative.ClusterGenre;

public class Item {
	
	protected String m_movieName;
	protected int m_year;
	protected List<String> m_itemGenres;
//	private ClusterGenre m_pClusterGenre;
	
	public Item(String movieName, int year, List<String> itemGenres)
	{
		m_movieName = movieName;
		m_year = year;
		m_itemGenres = itemGenres;
	}

	/**
	 * @return the m_itemGenres
	 */
	public List<String> getItemGenres() {
		return m_itemGenres;
	}

	/**
	 * @param genres the itemGenres to set
	 */
	public void setItemGenres(List<String> genres) {
		m_itemGenres = genres;
	}

	/**
	 * @return the m_movieName
	 */
	public String getMovieName() {
		return m_movieName;
	}

	/**
	 * @param name the movieName to set
	 */
	public void setMovieName(String name) {
		m_movieName = name;
	}

	/**
	 * @return the m_year
	 */
	public int getYear() {
		return m_year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int m_year) {
		this.m_year = m_year;
	}

}

