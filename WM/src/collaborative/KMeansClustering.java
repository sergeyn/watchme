package collaborative;
import collaborative.User;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.math.*;

import myData.Profile_genres;

public class KMeansClustering 
{
	public static final int NUMGENRES =  11;
	private int m_k;
	private List <User> m_usersList;
	private List<Centroid> m_centroids;
	private Boolean m_wereClustersModified = false;//flag indicating whether the values of centroids changed
	
	public KMeansClustering(int k, List<User> usersList)
	{
		m_k = k;
		m_usersList = usersList;
	}
	/*this function updates the field m_userCluster for each user in usersList and 
	 * returns list of Clusters with centroid ratings, user members, and all users distances
	 * from it's centroid
	 */
	public List <Cluster> Cluster()
	{
		//first iteration
		InitializeCentroidData(); 
		AddDistances();	
		ClusterUsers();
		
		//clustering loop
		while(m_wereClustersModified)
		{
			fillDistances(CalcNewCentroids());
			ClusterUsers();
		}
		
		return createClustersList();
	}
	/*
	 * creates clusters list from Centroids list
	 */
	private List <Cluster> createClustersList()
	{
		//create output Clusters list
		List <Cluster> clustersList = new LinkedList<Cluster> ();
		int i,j;
		int numOfUsers=0;
		
		for(i =0; i<m_k; i++)
		{
			Cluster cluster = new Cluster(i);
			Centroid centroid = m_centroids.get(i);
			cluster.setDistances(centroid.getDistances());
			cluster.setGenreRatings(centroid.getGenreRatings());
			numOfUsers=0;
			
			for(j=0; j<centroid.getAreMembers().size();j++)
			{
				if(centroid.getAreMembers().get(j)==true)
				{
					numOfUsers++;
					cluster.getMemberIndexesList().add(j);	
				}		
			}
			cluster.setNumOfUsers(numOfUsers);
			clustersList.add(cluster);
		}
		
		return clustersList;
	}
	
	/* Centroids initialization. Note: Distances and Centroid.areMembers lists are not yet filled.
	The first iteration centroids are arbitrary: the first k members of m_usersList */
	private void InitializeCentroidData()
	{
		int i, j,z;
		Iterator <UserGenre> iterator ;
		double rating = -1;
		
		//fill the ratings list for each centroid
		for( i=0; i<m_k ; i++){ 
			
			//create centroid
			Centroid centroid =new Centroid();
			m_centroids.add(centroid);
			
			//create distances list
			 centroid.setDistances(new LinkedList<Double> ());
			
			//create areMembers list and initialize all values to 0
			centroid.setAreMembers(new LinkedList<Boolean> ());
			for(z=0;z<m_usersList.size();z++)
			{
				centroid.getAreMembers().add(false);
			}
			//Create and fill CentroidRatings array
			Double[] CentroidRatings = new Double[NUMGENRES];
			iterator= m_usersList.get(i).getUserGenresList().iterator();
				
			for(j=0; j<NUMGENRES ; j++)
			{
				rating = iterator.next().getGenreRating();
				if(rating == 0)
					CentroidRatings[j] = 3.00;//if no ratings were made, assign the mean
				else CentroidRatings[j] = rating;	
			}
			 
			m_centroids.get(i).setGenreRatings(CentroidRatings);
		}
		
	}
	
	/* Calculation of euclidean distance between user and centroid ratings */
	private double CalcDistance(User user, Double[] centroidGenreRatings)
	{
		double distance = 0;
		
		for(int i=0; i< NUMGENRES; i++)
		{
			distance+= Math.pow((centroidGenreRatings[i] - user.getUserGenresList().get(i).getGenreRating()),2);
		}
		
		return Math.pow(distance, 0.5);
	}
	/* Adding the distances list members of each centroid - used in first iteration*/
	private void AddDistances()
	{
		Iterator <User> Uiterator ;
		Iterator <Centroid> Citerator = m_centroids.iterator();
		
		while(Citerator.hasNext())
		{
			Centroid centroid = Citerator.next();
		
			//initialize user iterator
			Uiterator =  m_usersList.iterator();
			
			while (Uiterator.hasNext())
			{
				centroid.getDistances().add(CalcDistance(Uiterator.next(),centroid.getGenreRatings()));
			}
			
		}
	}
	/*filling distances from centroids that were changed*/
	private void fillDistances(Boolean[] wereCentroidsChanged)
	{
		int i,j;
		Iterator <User> Uiterator ;
		
		for(i=0;i<m_k;i++)
		{
			if(wereCentroidsChanged[i]==true)
			{
				Centroid centroid = m_centroids.get(i);
				//initialize user iterator
				Uiterator =  m_usersList.iterator();
				for(j=0;Uiterator.hasNext();j++)
				{
					centroid.getDistances().set(j, CalcDistance(Uiterator.next(),centroid.getGenreRatings()));
				}
			}		
		}
	}
	
	
	/*this function makes new clustering and updates the flag m_wereClustersModified
	 * in this way: if at least one user changed it's cluster,m_wereClustersModified=true
	 * else m_wereClustersModified=false
	 * this function assumes centriod ratings and distances from centroid are filled*/
	private void ClusterUsers()
	{
		m_wereClustersModified = false;
		Iterator <User> Uiterator = m_usersList.iterator(); ;
		Iterator <Centroid> Citerator;
		Boolean oldIsMember = false;
		double minDistance ; //the minimum distance from all centroids
		int minDistCentroid;//the centroid with the minimum distance
		int i,j;
		
		for(j=0;Uiterator.hasNext();j++)
		{
			User user=Uiterator.next();
			/*TODO: add input checks*/
			
			//calculate new clusters
			Citerator = m_centroids.iterator();
			
			minDistCentroid = 0;
			minDistance=Citerator.next().getDistances().get(0);
			
			for(i=1; Citerator.hasNext();i++)
			{
				double cDistance = Citerator.next().getDistances().get(i); 
				
				if(cDistance<minDistance)
				{
					minDistance = cDistance;
					minDistCentroid = i;
				}
			}	
			//update new clusters
			oldIsMember = m_centroids.get(minDistCentroid).getAreMembers().get(j);
			if(oldIsMember!=true)
			{
				m_wereClustersModified = true;
				if (user.getUserCluster()>=0)
					m_centroids.get(user.getUserCluster()).getAreMembers().set(j,false);
				user.setUserCluster(minDistCentroid);	
				m_centroids.get(minDistCentroid).getAreMembers().set(j, true);
			}
			
		}
	}
	/*updates centroid values, and returns array indicating which centroids were changed.
	If at least one centroid was changed, updates the flag wasCentroidsModified to true*/ 
	private Boolean[]CalcNewCentroids()
	{
		Double[] CentroidRatings = new Double[NUMGENRES];
		int i,j,numOfMembers,centroidNum;
		Iterator <Centroid> Citerator = m_centroids.iterator();
		List<Boolean> AreMembers;
		int numOfUsersForCentroid;
		Boolean wereChangesMade = false;
		Boolean[] wereCentroidsChanged = new Boolean[m_k];
		
		for(centroidNum=0;Citerator.hasNext();centroidNum++)
		{
			numOfUsersForCentroid=0;
			Centroid centroid = Citerator.next();
			AreMembers = centroid.getAreMembers();
			Double [] oldGenreRatings=centroid.getGenreRatings();
			
			Iterator <Boolean> AMiterator= AreMembers.iterator();
			for(j=0; j<NUMGENRES;j++)
				CentroidRatings[j]=0.00;
			//calculation of centroid new genre ratings
			for(i=0,numOfMembers=0;AMiterator.hasNext();i++)
			{
				if(AMiterator.next()==true)//if in this index the user belongs to this centroid - take him into account when calculating the genres mean
				{
					numOfMembers++;
					for(j=0; j<NUMGENRES;j++)
					{
						CentroidRatings[j]+=m_usersList.get(i).getUserGenresList().get(j).getGenreRating();
					}
				}	
			}
			for(j=0; j<NUMGENRES;j++)
			{
				CentroidRatings[j]/=numOfMembers;
			}
			//check if changes were made to centroid genre ratings
			for(j=0; j<NUMGENRES;j++)
			{
				if(CentroidRatings[j]!=oldGenreRatings[j])
				{
					wereCentroidsChanged[centroidNum]=true;
					m_wereClustersModified=true;
					break;
				}
			}
			if(j!=NUMGENRES)
				centroid.setGenreRatings(CentroidRatings);
		
		}
		return wereCentroidsChanged;
		
	}
	

}
