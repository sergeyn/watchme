package collaborative;
import java.util.ArrayList;
import java.util.List;

import collaborative.UserItem;
import collaborative.UserGenre;
import myData.Conversions;
/* class user: contains demographic information, ratings and genre preference info */
public class User {
	
	private String m_userName="";
	private int m_userCluster=-1;
	private int m_age;
	private int m_occupation;
	private String m_gender;
	private ArrayList <UserGenre> m_userGenresList = null;
	private List m_userItems = null;
	
	public User(String userName ,int age, int occupation ,String gender)
	{
		m_userName = userName;
		m_age = age;
		m_occupation = occupation;
		m_gender = gender;
		m_userGenresList = new ArrayList<UserGenre>();
		m_userGenresList.add(new UserGenre("Action"));
		m_userGenresList.add(new UserGenre("Thriller"));
		m_userGenresList.add(new UserGenre("Crime"));
		m_userGenresList.add(new UserGenre("Drama"));
		m_userGenresList.add(new UserGenre("Horror"));
		m_userGenresList.add(new UserGenre("Adventure"));
		m_userGenresList.add(new UserGenre("Comedy"));
		m_userGenresList.add(new UserGenre("Romance"));
		m_userGenresList.add(new UserGenre("Family"));
		m_userGenresList.add(new UserGenre("Sci-Fi"));
		m_userGenresList.add(new UserGenre("Animation"));

	}
	public void AddItem(UserItem userItem)
	{
		String itemGenre = "";
		for(int i = 0; i<userItem.getItemGenres().size();i++)
		{
			itemGenre = userItem.getItemGenres().get(i);
			m_userGenresList.get(Conversions.genreFromStringToIndex(itemGenre)).AddNewItem(userItem);
		}
		
	}
	/**
	 * @return the m_age
	 */
	public int getAge() {
		return m_age;
	}
	/**
	 * @param m_age the m_age to set
	 */
	public void setAge(int m_age) {
		this.m_age = m_age;
	}
	/**
	 * @return the m_gender
	 */
	public String getGender() {
		return m_gender;
	}
	/**
	 * @param m_gender the m_gender to set
	 */
	public void setGender(String m_gender) {
		this.m_gender = m_gender;
	}
	/**
	 * @return the m_occupation
	 */
	public int getOccupation() {
		return m_occupation;
	}
	/**
	 * @param m_occupation the m_occupation to set
	 */
	public void setOccupation(int m_occupation) {
		this.m_occupation = m_occupation;
	}
	/**
	 * @return the m_userName
	 */
	public String getUserName() {
		return m_userName;
	}
	/**
	 * @param name the m_userName to set
	 */
	public void setUserName(String name) {
		m_userName = name;
	}
	/**
	 * @return the m_userGenresList
	 */
	public ArrayList<UserGenre> getUserGenresList() {
		return m_userGenresList;
	}
	/**
	 * @param genresList the m_userGenresList to set
	 */
	public void setUserGenresList(ArrayList<UserGenre> genresList) {
		m_userGenresList = genresList;
	}
	/**
	 * @return the m_userGroup
	 */
	public int getUserCluster() {
		return m_userCluster;
	}
	/**
	 * @param group the m_userGroup to set
	 */
	public void setUserCluster(int group) {
		m_userCluster = group;
	}
}


