package collaborative;
import java.util.*;
import collaborative.UserItem;


public class UserGenre {
	
	private String m_genreName;
	private double m_genreRating;
	private int m_ratingArr[];
	private int m_NumOfRatings=0;
	private List<UserItem> m_ItemsList;
	
	public UserGenre(String genreName)
	{
		m_genreName = genreName;
		m_genreRating = 0;
//		int m_ratingArr[] = new int [5];
		m_NumOfRatings=0;
//		m_ItemsList = new LinkedList<UserItem>();
	}
	
	//calculate genre mean from all list of items	
/*	public double CalcGenreMean()
	{
		
	}
*/
	//update all the member variables
	public void AddNewItem(UserItem newItem)
	{
		m_genreRating = (((m_genreRating*m_NumOfRatings)+ newItem.getRating())/(m_NumOfRatings+1));
// 		m_ratingArr[newItem.getRating()]++;
		m_NumOfRatings++;
//		m_ItemsList.add(newItem);
	}

	/**
	 * @return the m_ratingArr
	 */
	public int[] getRatingArr() {
		return m_ratingArr;
	}

	/**
	 * @param arr the m_ratingArr to set
	 */
	public void setRatingArr(int[] arr) {
		m_ratingArr = arr;
	}

	/**
	 * @return the m_genreRating
	 */
	public double getGenreRating() {
		return m_genreRating;
	}

	/**
	 * @param rating the m_genreRating to set
	 */
	public void setGenreRating(double rating) {
		m_genreRating = rating;
	}
	
}
