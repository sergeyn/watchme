package collaborative;
import java.util.List;
import collaborative.Item;

public class UserItem extends Item
{
	private String m_userName;
	private int m_userCluster;
	private int m_rating;
	
	public UserItem(String movieName, int year, List<String> itemGenres,String userName , int userCluster, int rating)
	{
		super(movieName, year, itemGenres);
		m_userName = userName;
		m_userCluster = userCluster;
		m_rating = rating;
	}

	/**
	 * @return the m_rating
	 */
	public int getRating() {
		return m_rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(int m_rating) {
		this.m_rating = m_rating;
	}

	/**
	 * @return the m_userCluster
	 */
	public int getUserCluster() {
		return m_userCluster;
	}

	/**
	 * @param cluster the userCluster to set
	 */
	public void setUserCluster(int cluster) {
		m_userCluster = cluster;
	}

	/**
	 * @return the m_userName
	 */
	public String getUserName() {
		return m_userName;
	}

	/**
	 * @param name the userName to set
	 */
	public void setUserName(String name) {
		m_userName = name;
	}

}
