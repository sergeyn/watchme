package myData;

import java.sql.SQLException;
import java.util.List;
import java.util.Iterator;

public class Adaptation {
	
	private static double InterestThreshold = 0.5;
	
	public static void DailyUpdate(String profileId){
		try {
			List<Profile_genres> genresOfprofile =  CommonFunctions.getAllGenresofProfile(profileId);
			if (genresOfprofile.size()==0) System.out.println("Erunda v getAllGenresofProfile");
			System.out.println("list size: " + genresOfprofile.size());
			Iterator<Profile_genres> iterator = genresOfprofile.iterator();
			while (iterator.hasNext()){
				Profile_genres updateProfileGenres = iterator.next();
				int genreId = updateProfileGenres.getGenreId();
				System.out.println("genreId " + genreId);
				String isCurrentInterest = updateProfileGenres.getIsCurrentInterest();
				double interest = updateProfileGenres.getInterest();
				System.out.println("interest " + interest);
				System.out.println("negative sequence is: " + updateProfileGenres.getNegativeSequence());
				if (isCurrentInterest.equals("Y") && ((interest<InterestThreshold)|| updateProfileGenres.getNegativeSequence()>=6)){
					isCurrentInterest="N";
					System.out.println("Changing Interest to N ");}
				else if (isCurrentInterest.equals("N") && (interest>InterestThreshold)&& (updateProfileGenres.getPositiveSequence()>=3 )){
					isCurrentInterest="Y";
					System.out.println("Changing Interest to Y ");
				}
					
				System.out.println("before update ");
				updateProfileGenres.setProfileId(profileId);
				updateProfileGenres.setGenreId(genreId);
				updateProfileGenres.setIsCurrentInterest(isCurrentInterest);
				updateProfileGenres.setInterest(interest);
				System.out.println(" " +updateProfileGenres.getGenreId() +" isCurrent: "+ updateProfileGenres.getIsCurrentInterest() +" interest: " + updateProfileGenres.getInterest() );
				CommonFunctions.updateGenreInterestsOfProfile(updateProfileGenres);
				System.out.println("after update ");
			}
			//update 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		
		
	}
	

	
	public static void main(String[] args) {

		//Adaptation.DailyUpdate("bbb@test.xx");
		
	}
	
	
	
}
