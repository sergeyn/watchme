package myData;
import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class CommonFunctions {
	private static SqlMapClient sqlMapper;
	static {
		try {
      Reader reader = Resources.getResourceAsReader("myData//SqlMapConfig.xml");
      sqlMapper = SqlMapClientBuilder.buildSqlMapClient(reader);
      reader.close(); 
    } catch (IOException e) {throw new RuntimeException("Something bad happened while building the SqlMapClient instance." + e, e);}
	}
	
	
	public static int selectGenreIdByName(String gName) throws SQLException {
		Object o = sqlMapper.queryForObject("selectGenreIdByName",gName);
		if (o!=null) return (Integer)o;
		else return(-1);
		
	}
	
	//------------------ reports
	
	public static List rep12() throws SQLException {
		return (List)sqlMapper.queryForList("rep12"); 
	}
	
	public static List graduatedUsers() throws SQLException {
		return (List)sqlMapper.queryForList("graduatedUsers"); 
	}
	
	public static ComplexQuery entriesPerProfile(String pId) throws SQLException {
		Object o = sqlMapper.queryForObject("entriesPerProfile",pId);
		if (o!=null) return (ComplexQuery)o;
		else return(null);
		
	}
	
	
	public static List watchDateProfileId() throws SQLException {
		return (List)sqlMapper.queryForList("WatchDateProfileId"); 
	}
	
	public static List abusiveTotals() throws SQLException {
		return (List)sqlMapper.queryForList("abusiveTotals"); 
	}
	
	
	public static List allUsers_in_future() throws SQLException {
		return (List)sqlMapper.queryForList("allUsers_in_future"); 
	}
	
	public static int totalUsers_in_future() throws SQLException {
		return (Integer)sqlMapper.queryForObject("totalUsers_in_future"); 
	}
	
	public static int totalProfiles() throws SQLException {
		return (Integer)sqlMapper.queryForObject("totalProfiles"); 
	}
	
	public static List reportTblExperiment_groups() throws SQLException {
		return (List)sqlMapper.queryForList("reportTblExperiment_groups"); 
	}
	
	public static List reportActiveUsersPerDay(String date) throws SQLException {
		return (List)sqlMapper.queryForList("reportActiveUsersPerDay",date); 
	}
	
	public static List reportUsersPerStrategy () throws SQLException {
		return (List)sqlMapper.queryForList("reportUsersPerStrategy"); 
	}
	
	public static List reportUsersPerGroup () throws SQLException {
		return (List)sqlMapper.queryForList("reportUsersPerGroup"); 
	}
	
	public static List reportTodaysWatchers () throws SQLException {
		return (List)sqlMapper.queryForList("reportTodaysWatchers"); 
	}
	
	public static List reportNotVisitingUsers () throws SQLException {
		return (List)sqlMapper.queryForList("reportNotVisitingUsers");  
	}
	
	public static List reportUsersByStrategy () throws SQLException {
		return (List)sqlMapper.queryForList("reportUsersByStrategy"); 
	}
	
	public static List reportTotalWatcherReport_P1 () throws SQLException {
		return (List)sqlMapper.queryForList("reportTotalWatcherReport_P1"); 
	}
	
	public static List reportTotalWatcherReport_P2 (myData.ComplexQuery cq) throws SQLException {
		return (List)sqlMapper.queryForList("reportTotalWatcherReport_P2",cq); 
	}
	 
	public static List profileEntriesGroupsTotal() throws SQLException {
		return (List)sqlMapper.queryForList("profileEntriesGroupsTotal"); 
	}
	
	public static List profileEntriesTotal () throws SQLException {
		return (List)sqlMapper.queryForList("profileEntriesTotal"); 
	}
	public static List profileEntriesGSTotal() throws SQLException {
		return (List)sqlMapper.queryForList("profileEntriesGSTotal"); 
	}
	
	
	public static List profileEntriesWeekly (int weekNo) throws SQLException {
		return (List)sqlMapper.queryForList("profileEntriesWeekly",weekNo); 
	}
	
	
	public static List profileEntriesGSWeekly(int weekNo) throws SQLException {
		return (List)sqlMapper.queryForList("profileEntriesGSWeekly",weekNo); 
	}
	

	public static List allUnregisteredProfiles () throws SQLException {
		return (List)sqlMapper.queryForList("allUnregisteredProfiles"); 
	}
	
	public static List unusedProfiles() throws SQLException {
		return (List)sqlMapper.queryForList("unusedProfiles"); 
	}
	
	
	
	//------------------
	
	
	//--------- Days Missed functions
	public static List getGoodUsersPerDay(String s) throws SQLException {
		return (List)sqlMapper.queryForList("getGoodUsersPerDay",s); 
	}
	
	public static List getBadUsersPerDay(String s) throws SQLException {
		return (List)sqlMapper.queryForList("getBadUsersPerDay",s); 
	}
	
	public static void addDaysMissedToBadUsers(String s) throws SQLException {
		sqlMapper.update("addDaysMissedToBadUsers",s);
	}
	
	public static void setDaysMissedToZeroToGoodUsers(String s) throws SQLException {
		sqlMapper.update("setDaysMissedToZeroToGoodUsers",s);
	}
	//---------
	
	public static List allProfiles() throws SQLException {
		return (List)sqlMapper.queryForList("allProfiles");
	}
	
	public static Profile_genres aselectPGbyPG (Profile_genres pg) throws SQLException {
		return (Profile_genres)sqlMapper.queryForObject("selectPGbyPG", pg);
	}
	
	public static boolean isExistPGByID(String pId) throws SQLException {
		List pg = (List)sqlMapper.queryForList("selectPGByID",pId);
		if (pg.size()>0) return(true);
		return(false);
	}
	
	//in Profile 
	public static List selectProfiles() throws SQLException {
		return (List)sqlMapper.queryForList("selectProfiles"); 
	}
	//in movielens_data
	public static List selectCombinationProfiles (Movielens_data md) throws SQLException {
		return (List)sqlMapper.queryForList("selectCombinationProfiles",md); 
	}
	//in Demographics
	public static String selectDemographicRecommender(Demographics d) throws SQLException {
		return (String)sqlMapper.queryForObject("selectDemographicRecommender",d);
	}
	//in Movielens_statistics
	public static Movielens_statistics selectGenreSDandMD(String genreName) throws SQLException {
		return (Movielens_statistics)sqlMapper.queryForObject("selectGenreSDandMD",genreName);
	}
	//in Profile_genres
	public static void updateDRecommenderInProfileGenres(Profile_genres profileGenres) throws SQLException {
		sqlMapper.update("updateDRecommenderInProfileGenres",profileGenres);
	}
	//in Demographics
	public static void updateDemographicShouldRecommend(Demographics d) throws SQLException {
		sqlMapper.update("updateDemographicShouldRecommend",d);
	}
	
	public static Profile selectProfileByIdAndPass (Profile pr) throws SQLException {
		return (Profile)sqlMapper.queryForObject("selectProfileByIdAndPass",pr);
	}
	
	public static Profile selectProfileById(String pId) throws SQLException {
		return (Profile)sqlMapper.queryForObject("selectProfileById",pId);
	}
	
	public static int getSeenTrailersPerTodayByProfile(String profileId) throws SQLException {
		return (Integer)sqlMapper.queryForObject("getSeenTrailersPerTodayByProfile",profileId);
	}
	
	public static List selectAllGenres () throws SQLException {
		return sqlMapper.queryForList("selectAllGenres");
	}
		
	public static void insertPG(Profile_genres pg) throws SQLException {
		sqlMapper.insert("insertPG",pg);
	}
	
	public static Profile insertNewProfile(Profile p) throws SQLException {
		Profile temp = (Profile)sqlMapper.queryForObject("selectProfileById",p.getProfileId());
		if (temp!=null) return(null);
		sqlMapper.insert("insertProfile", p);
		return p; 
	}
	
	public static void updateStrategyByProfile(Profile p) throws SQLException {
		sqlMapper.update("updateStrategyByProfile",p);
	}
	 
	public static int isQuestionFormFilled(String email) throws SQLException {
		Profile temp = (Profile)sqlMapper.queryForObject("selectProfileById",email);
		if (temp != null)
			if (temp.getCurrentStrategyId()==-1) return(0);
			else  return(1);
		return(-1);
	}
	
	public static int availableTrailersPerProfile(String pId) throws SQLException {
		return (Integer)sqlMapper.queryForObject("availableTrailersPerProfile",pId);
	}
	
	public static List getNewTrailerByProfile(String pId) throws SQLException {
		return (List)sqlMapper.queryForList("getNewTrailerByProfile",pId);
	}
	
	public static List selectAllStrategies() throws SQLException {
		return (List)sqlMapper.queryForList("selectAllStrategies");
	}
	
	public static void insertTOP (Trailers_of_profile p) throws SQLException {
	    sqlMapper.insert("insertTOP", p);
	}
	
	public static List getDataOnEmail(String email) throws SQLException {
		return (List)sqlMapper.queryForList("getDataOnEmail",email);
	}
	
	public static void insertTrailer (Trailers trailer) throws SQLException {
		sqlMapper.insert("insertTrailer", trailer);
	}

	public static Genres selectGenreByName (String name) throws SQLException {
		return (Genres)sqlMapper.queryForObject("selectGenreByName",name);
	}

	public static Genres insertNewGenre (String name) throws SQLException {
		Genres g = new Genres();
		int recNumber = (Integer)sqlMapper.queryForObject("getRecordsNumber");
		if (recNumber>0) g.setgenreId((Integer)sqlMapper.queryForObject("generateNewGenreId"));
		g.setgenreName(name);
		sqlMapper.insert("insertGenres", g);
		return g; 
	}
  
	public static void insertGT (Genres_of_trailers gt) throws SQLException {
		sqlMapper.insert("insertGT", gt);
	}
	
	public static List getTrailersList() throws SQLException {
		return (List)sqlMapper.queryForList("getTrailersList");
	}
	
	
	public static void setDurationToTrailer(Trailers t) throws SQLException{
		sqlMapper.update("setDurationToTrailer",t);
	}
	
	public static String selectGenreNameByTrailerId(String trailerId) throws SQLException{
		return (String)sqlMapper.queryForObject("selectGenreNameByTrailerId",trailerId);
	}
	
	//multiGenre
	//first version - gets only one trailer
	public static String getMultiGenreTrailer(Multi_Genre genres) throws SQLException{
		return (String)sqlMapper.queryForObject("getTwoGenreTrailer",genres);
	}

	//second version - gets list of ALL trailers by 2 given genres
	public static List getListofMulitGenreTrailerIDs (Multi_Genre multi) throws SQLException{
		return (List) sqlMapper.queryForList("getTwoGenreTrailersList",multi);
	}
	

	//getSingleGenre
	public static Trailers getSingleGenreTrailer(Profile_genres profile_genres) throws SQLException{
		List trailerIdList = (List) sqlMapper.queryForList("getSingleGenreTrailer",profile_genres);
		String id ="";
		if (trailerIdList.equals(null) || trailerIdList.isEmpty())
			return null;
		if (trailerIdList.size()==1)
			id = (String)trailerIdList.get(0);
		else
		    id = ((String)trailerIdList.get(new Random().nextInt(trailerIdList.size())));
		return (Trailers) (sqlMapper.queryForObject("getTrailerbyId",id));
	}
	//get trailer by id
	
	public static Trailers getTrailerbyId(String id) throws SQLException{
	
		
	try {
		return (Trailers) (sqlMapper.queryForObject("getTrailerbyId",id));
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.getMessage();
	}
	return null;
	
}
	public static List getSeenTrailersbyGenre (Profile_genres profile_genres) throws SQLException{
		return sqlMapper.queryForList("getSeenTrailersbyGenre",profile_genres);
	}
	
	//isCurrentInterestGenres
	public static List getCurrentInterestofProfile(String profileId ) throws SQLException{
		return (List) sqlMapper.queryForList("getCurrentInterestGenres",profileId);
	}
	
	//get future Interests of profile
	public static List getFutureInterestsofProfile(String profileId ) throws SQLException{
		return (List) sqlMapper.queryForList("getFurrentInterestGenres",profileId);
	}
	
	public static List getGenresbyTrailerId (String trailerId) throws SQLException{
		return (List) sqlMapper.queryForList("selectGenresofTrailerbyId",trailerId);
	}
	
	//Back from the Future 4 - Set Y
	public static void ReturntoCurrent(Profile_genres profile_genres){
		try {
			sqlMapper.update("ReturntoCurrent",profile_genres);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}
	
	public static void ReturnSingleGenretoCurrent(Profile_genres profile_genres){
		try {
			sqlMapper.update("ReturnSingleGenretoCurrent",profile_genres);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}
	
	public static Profile_genres selectPGbyPG(Profile_genres profile_genre) throws SQLException{
		return (Profile_genres) sqlMapper.queryForObject("selectPGbyPG",profile_genre);
	}
	
//	public static Integer getTodaysSeenTrailersbyGenre(Profile_genres profile_genre) throws SQLException{
//		return (Integer) sqlMapper.queryForObject("getTodaysSeenTrailersbyGenre",profile_genre);
//	}
	
	//in profile
	public static void updateExperimentGroupId(Profile p) throws SQLException {
		sqlMapper.update("updateExperimentGroupId",p);
	}
	//in profile_genres
	public static void updateSequence(Profile_genres profileGenres) throws SQLException {
		sqlMapper.update("updateSequence", profileGenres);
	}
	//in experiment_groups 
	public static void updateExperimentGroupCount(Experiment_groups experiment_groups) throws SQLException {
		sqlMapper.update("updateExperimentGroupCount", experiment_groups);
	}
	//in experiment_groups
	public static List selectExperimentGroupsByStrategy (int strategy) throws SQLException {
		 return sqlMapper.queryForList("selectExperimentGroupsByStrategy", strategy);
	}
	
	//Learning
	public static void updateLearning(Profile_genres pg) throws SQLException{
		sqlMapper.update("updateLearning",pg);
	}
	//Profile_Trailer_Genres
	
	public static void InsertGenresScore(Profile_Trailer_Genres profile_trailer_genres) throws SQLException{
		sqlMapper.insert("InsertGenreScores",profile_trailer_genres);
	}
	
	public static Integer getTodaysSeenTrailersbyGenre(Profile_Trailer_Genres profile_trailer_genres) throws SQLException{
		System.out.println("In sqlmapper: "+profile_trailer_genres.getGenreId());
		return (Integer)sqlMapper.queryForObject("getTodaysSeenTrailersbyGenre",profile_trailer_genres);
	}
	//insert user into the future 
	public static void insertUserintoTheFuture (Users_in_future user) throws SQLException{
		sqlMapper.insert("insertUsers_in_future",user);
	}
	
	//adaptation
	
	public static void updateGenreInterestsOfProfile(Profile_genres profile_genres) throws SQLException{
		sqlMapper.update("updateInterests",profile_genres);
	}
	
	public static List getAllGenresofProfile (String profileId) throws SQLException{
		return (List) sqlMapper.queryForList("getGenresAndInterests",profileId);
	}
	//TrailersOfProfile
	public static List getTrailersOfProfile (String profileId) throws SQLException{
		return (List) sqlMapper.queryForList("getTrailersOfProfile",profileId);
	}
	
}
