package myData;
public class ComplexQuery {
	private int int_field_0=0;
	private int int_field_1=0;
	private int int_field_2=0;
	private String string_field_0="";
	private String string_field_1="";
	private String string_field_2="";
	
	public int getInt_field_0() {
		return int_field_0;
	}
	public void setInt_field_0(int int_field_0) {
		this.int_field_0 = int_field_0;
	}
	public int getInt_field_1() {
		return int_field_1;
	}
	public void setInt_field_1(int int_field_1) {
		this.int_field_1 = int_field_1;
	}
	public String getString_field_0() {
		return string_field_0;
	}
	public void setString_field_0(String string_field_0) {
		this.string_field_0 = string_field_0;
	}
	public String getString_field_1() {
		return string_field_1;
	}
	public void setString_field_1(String string_field_1) {
		this.string_field_1 = string_field_1;
	}
	public int getInt_field_2() {
		return int_field_2;
	}
	public void setInt_field_2(int int_field_2) {
		this.int_field_2 = int_field_2;
	}
	public String getString_field_2() {
		return string_field_2;
	}
	public void setString_field_2(String string_field_2) {
		this.string_field_2 = string_field_2;
	}
}
