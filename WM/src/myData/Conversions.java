package myData;

public class Conversions {
	
	public final static int dynamicId = 1;
	public final static int stableId = 2;
	public final static int pofigId = 3;
	public final static double dynamic = 0.9583; 	  //1
	public final static double indifferent = 0.8916;  //3
	public final static double stable = 0.4672;		  //2
	
	public static String genreFromIntToString(int genreId){
		
		String genreName = "";
		
		switch(genreId){
			case 1: genreName = "Action";
			break;
			case 2: genreName = "Thriller";
			break;
			case 3: genreName = "Crime";
			break;
			case 4: genreName = "Drama";
			break;
			case 5: genreName = "Horror";
			break;
			case 6: genreName = "Adventure";
			break;
			case 8: genreName = "Comedy";
			break;
			case 13: genreName = "Romance";
			break;
			case 15: genreName = "Family";
			break;
			case 16: genreName ="Sci-Fi";
			break;
			case 21: genreName ="Animation";
		}
		return genreName;
	}
	
	public static String occupationFromIntToString(int occupationId){
		
		String occupation = "";
		
		switch(occupationId){
		
		case 0: occupation = "Other";
		break;
		case 1: occupation = "Academic/Educator";
		break;
		case 2: occupation = "Artist";
		break;
		case 3: occupation = "Clerical/Admin";
		break;
		case 4: occupation = "College/Grad.student";
		break;
		case 5: occupation = "Customer Service";
		break;
		case 6: occupation = "Doctor/Health care";
		break;
		case 7: occupation = "Executive/Managerial";
		break;
		case 8: occupation = "Farmer";
		break;
		case 9: occupation = "Homemaker";
		break;
		case 10: occupation = "K-12 student";
		break;
		case 11: occupation ="Lawyer";
		break;
		case 12: occupation ="Programmer";
		break;
		case 13: occupation ="Retired";
		break;
		case 14: occupation ="Sales/Marketing";
		break;
		case 15: occupation ="Scientist";
		break;
		case 16: occupation ="Self-employed";
		break;
		case 17: occupation ="Technician/Engineer";
		break;
		case 18: occupation ="Tradesman/Craftsman";
		break;
		case 19: occupation ="Unemployed";
		break;
		case 20: occupation ="Writer";
		
	
		}
		return occupation;
	}
	
	public static int genreFromStringToInt(String genreName){
		
		int genreId=0;
		
		if(genreName=="Action")
			genreId=1;
		else if(genreName=="Thriller")
			genreId=2;
		else if(genreName=="Crime")
			genreId=3;
		else if(genreName=="Drama")
			genreId=4;
		else if(genreName=="Horror")
			genreId=5;
		else if(genreName=="Adventure")
			genreId=6;
		else if(genreName=="Comedy")
			genreId=8;
		else if(genreName=="Romance")
			genreId=13;
		else if(genreName=="Family")
			genreId=15;
		else if(genreName=="Sci-Fi")
			genreId=16;
		else if(genreName=="Animation")
			genreId=21;
		
		return genreId;
	}
	
	public static int genreFromStringToIndex(String genreName){
		
		int genreId=0;
		
		if(genreName=="Action")
			genreId=0;
		else if(genreName=="Thriller")
			genreId=1;
		else if(genreName=="Crime")
			genreId=2;
		else if(genreName=="Drama")
			genreId=3;
		else if(genreName=="Horror")
			genreId=4;
		else if(genreName=="Adventure")
			genreId=5;
		else if(genreName=="Comedy")
			genreId=6;
		else if(genreName=="Romance")
			genreId=7;
		else if(genreName=="Family")
			genreId=8;
		else if(genreName=="Sci-Fi")
			genreId=9;
		else if(genreName=="Animation")
			genreId=10;
		
		return genreId;
	}

	public static int ageFromSpecificToGroup(int age){
	
	if(age<=17)
		return 1;
	else if(age>17&& age<=24)
		return 18;
	else if(age>24 && age<=34)
		return 25;
	else if(age>34 && age<=44)
		return 35;
	else if(age>44 && age<=49)
		return 45;
	else if (age>49 && age<=55)
		return 50;
	else return 56;
	
	}
	
	public static String genreNameFromIndexToString(int index)
	{
		String genreName = "";
		
		switch(index){
		case 0: genreName = "Action";
		break;
		case 1: genreName = "Thriller";
		break;
		case 2: genreName = "Crime";
		break;
		case 3: genreName = "Drama";
		break;
		case 4: genreName = "Horror";
		break;
		case 5: genreName = "Adventure";
		break;
		case 6: genreName = "Comedy";
		break;
		case 7: genreName = "Romance";
		break;
		case 8: genreName = "Family";
		break;
		case 9: genreName ="Sci-Fi";
		break;
		case 10: genreName ="Animation";
		}
		return genreName;
	}
	
	public static int ageFromIndexToAgeGoup (int index){
		
		int ageGroup = 0;
		
		switch(index){
		case 0: ageGroup = 1;
		break;
		case 1: ageGroup = 18;
		break;
		case 2: ageGroup = 25;
		break;
		case 3: ageGroup = 35;
		break;
		case 4: ageGroup = 45;
		break;
		case 5: ageGroup = 50;
		break;
		case 6: ageGroup = 56;
		}
		
		return ageGroup;
	}
	
	public static String genderFromIntToString(int i){
		
		if(i==0)
			return "F";
		else return "M";
	}
	
	public static double DemographicToWeight(String demographic){
		
		Double demographicWeight = 1.0;
		
		if (demographic.equals("Yes")){
			demographicWeight=1.2;
			return demographicWeight;
		}
		else if (demographic.equals("No")){
			demographicWeight=0.9;
			return demographicWeight;
		}
		
		else return demographicWeight;
	}
	public static double UserWeightToDouble(int userWeight){
		double uWeight = 1.0;
		switch (userWeight) {
		case 1:
			uWeight = 0.0125;
			break;
		case 2:
			uWeight = 0.025;
			break;
		case 3:
			uWeight = 0.05;
			break;
		case 4:
			uWeight = 0.1;
			break;
		case 5:
			uWeight = 0.2;
			break;
		case 6:
			uWeight = 0.4;
			break;
		case 7:
			uWeight = 0.8;
			break;
		default:
			uWeight = 1.0;
			break;
		}
		return uWeight;
	}
	
	public static double UserRatingToDouble(int UserRating){
		double uRating = 1.0;
		switch (UserRating) {
		case 1:
			uRating = 0.0125;
			break;
		case 2:
			uRating = 0.03;
			break;
		case 3:
			uRating = 0.1;
			break;
		case 4:
			uRating = 0.33;
			break;
		case 5:
			uRating = 0.8;
			break;

		default:
			uRating = 1.0;
			break;
		}
		return uRating;
	}
	
	public static double Strategy2Sw(int strategy) {
		if (strategy == stableId)
			return stable;
		if (strategy == pofigId)
			return indifferent;

		return dynamic;
	}
}




