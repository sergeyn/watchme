package myData;

public class Experiment_groups {
	
	private int groupId = -1;
	private String groupName = "";
	private int strategy = -1;
	private int numberOfParticipants = 0;
	
	public Experiment_groups(int GroupId,int Strategy,int NumberOfParticipants){
		groupId=GroupId;
		strategy=Strategy;
		numberOfParticipants=NumberOfParticipants;
	}
		
	public Experiment_groups(){
		groupId = -1;
		groupName = "";
		numberOfParticipants = 0;
	}
	
	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the numberOfParticipants
	 */
	public int getNumberOfParticipants() {
		return numberOfParticipants;
	}
	/**
	 * @param numberOfParticipants the numberOfParticipants to set
	 */
	public void setNumberOfParticipants(int numberOfParticipants) {
		this.numberOfParticipants = numberOfParticipants;
	}
	/**
	 * @return the strategy
	 */
	public int getStrategy() {
		return strategy;
	}
	/**
	 * @param strategy the strategy to set
	 */
	public void setStrategy(int strategy) {
		this.strategy = strategy;
	}

}
