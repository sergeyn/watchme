package myData;

import java.util.Calendar;
import java.util.Date;

public class Formulae {
	
	static public Double userWeightForTenFirst(Double alfaC, Integer maxCats) {
		return alfaC / Math.max(10,maxCats);
	}
	
	static public Double userWeightForFictive (Integer maxCats) {
		return 1.0 / Math.max(10,maxCats);
	}
	
	
	static public Double interestForOpen(Double userWeight, Double beta, Double Tn1, int strategyId) {
		
		double interest = -1.0;
		switch (strategyId) {
			case 1:
				interest = (((userWeight+0.05)/2.0)*beta) + ((1.0-beta)*Tn1);
				break;
			case 2:
				interest = (((userWeight+1.0)/2.0)*beta) + ((1.0-beta)*Tn1);
				break;
			case 3:
				interest = (((userWeight+0.15)/2.0)*beta) + ((1.0-beta)*Tn1);
				break;
				
		}
				System.out.println("Tn1: " +Tn1+ " strategy: "+strategyId+ " interest"+interest +" beta " +beta);
		return interest;
		
	}
	
	static public Double interestForUpdateCategory(Double userWeight, Double score, Double beta, Double Tn1) {
		return ((score+userWeight)/2.0)*beta + ((1.0-beta)*Tn1);
	}
	
	static public Double calcDocScore(Double tune, Double gradual, Double sequence) {
		return (tune*gradual) + ((1.0-tune)* sequence);
	}

	static public Double calcCategoryScore(int tune, Double pos, int countPos,  Double neg, int countNeg) {
		assert(tune>9 && tune < 100);
		int posT = tune / 10;
		int negT = tune % 10;
		if (countPos == 0)
			++countPos;
		if (countNeg == 0)
			++countNeg;
		
		return ( posT*(pos/countPos) - (negT * (neg/countNeg) ) ) / Math.max(posT,negT);
	}
	
	/**
	 * Given that the experiment is relatively short - the best choice of diff
	 * !!! returns 1 for same day, 2 for today/tomorrow etc.
	 * @param d1
	 * @param d2
	 * @return days from d1 to d2
	 */
	public static int datesDelta(Date d1, Date d2) {
	  if (d1.after(d2))
		  return datesDelta(d2,d1);
	  Calendar c1 = Calendar.getInstance();
	  Calendar c2 = Calendar.getInstance();
	  c1.setTime(d1);
	  c2.setTime(d2);
	  int i=0;
	  while ( c1.before(c2)) {
		c1.add(Calendar.DATE,1);
		++i;
	  }
	  return i;
	}
	
	public static double gradualForgetting(Date tLast) {
		Date tNow = new Date();
		int delta = datesDelta(tNow,tLast);
		if (delta > 100) 
			delta = 1;
		System.out.println("delta: " + delta);
		return 1.0 / (0.0 + delta);
	}
}

