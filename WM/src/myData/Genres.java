package myData;
public class Genres {

	private int genreId=0;
	private String genreName="";
	
	public int getgenreId() {
		return genreId;
	}
	public void setgenreId(int genreId) {
		this.genreId = genreId;
	}
	public String getGenreName() {
		return genreName;
	}
	public void setgenreName(String genreName) {
		this.genreName = genreName;
	}	 
}
