package myData;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;


public class Learning {

	public class RetTuple {
		public double trailerScore; //mandatory
		public double genreInterest; //mandatory
		public double genreScore; //optional	
	}	

	/**
	 * 
	 * Globals:
		 * 
		 * @param beta The tune of user weight against score - hardcoded from Rafi 
		 * @param Tn1  The magic number of the strategy (note: there are 3) - hardcoded from Rafi
		 * @param gstune The tune of gradual or sequence - hardcoded from Rafi
		 * @param posNegTune  9<tune<100 The tune of positive or negative - hardcoded from Rafi
		 * 
		 * 
	*/	 
	
	private double beta;
	private double Tn1;
	private double gstune;
	private int	   posNegTune;
	

	public Learning(
			double ubeta,
			double uTn1, 
			double ugstune,
			int uposNegTune) /* !!! 9<tune<100 !!! */
    {
		super();
		beta = ubeta;
		Tn1 = uTn1;
		gstune = ugstune;
		posNegTune = uposNegTune;
	}

	/**
	 * 
	 * Single step
	 * -----------
	 *
	 * @param rating Rating of the trailer - normalized to [0..1]
	 * @param todaydocsOfGenreCount	How many movies of this genre today? 
	 * @param totalTodaydocsCount	How many movies today?
	 * @param genre The genre of the trailer 
	 * @param allTrailersInGenre Vector of all movies of this genre watched by user
	 * 
	 * 
	 * @return results tuple
	 * 
	 * @author Sergey
	 *
	 */
 
	public RetTuple learn(
			double rating,
			myData.Profile_genres genre,
			int todaydocsOfGenreCount, 
			int totalTodaydocsCount,
			Collection<myData.Profile_Trailer_Genres> allTrailersInGenre ) {
		
		System.out.println("todaycountgenre: " + todaydocsOfGenreCount);
		System.out.println("totaltodaydocs: " + totalTodaydocsCount);
		
		assert(rating <= 1.0 && rating >= 0.0);
		assert(totalTodaydocsCount >= todaydocsOfGenreCount);
		//assert( totalTodaydocsCount > 0);

		double posScore = 0.0;
		double negScore = 0.0;
		int posCount = 0;
		int negCount = 0;
		Iterator<myData.Profile_Trailer_Genres> it = allTrailersInGenre.iterator();
		
		RetTuple retVals = new RetTuple();
		
		Date lastDoc = (isRatingPos(rating)) ? genre.getDateOfLastPosMovie() : genre.getDateOfLastNegMovie();
		double gradual = Formulae.gradualForgetting(lastDoc); 
		double sequence = (double)((todaydocsOfGenreCount+0.0) / (totalTodaydocsCount+0.0));
		//System.out.println("grad: " + gradual);
		//System.out.println("seq: " + sequence); 
		while (it.hasNext()) { //sum all docs' scores 
			myData.Profile_Trailer_Genres item = it.next();
			if ( item.getIsPositive().equals("Y") ) {
				posScore += item.getGenreScore();
				++posCount;
			}
			else {
				negScore += item.getGenreScore();
				++negCount;
			}
		}
		retVals.trailerScore = Formulae.calcDocScore(gstune,gradual,sequence);
		retVals.genreScore =  Formulae.calcCategoryScore(posNegTune,posScore,posCount,negScore,negCount);
		//System.out.println("score: " + retVals.genreScore);
		retVals.genreInterest = Formulae.interestForUpdateCategory(genre.getScore(),retVals.genreScore,beta,Tn1);
		return retVals;
	}

	/**
	 * 
	 * Adapts the genres vector of a user
	 * 
	 * @param genres Vector of profile_genres
	 * @param interestThreshold Upon what threshold we want to move to future
	 * @param maxDelta The maximal time of last trailer we allow
	 * 
	 * @return Altered genres vector
	 * @author Sergey
	 *
	 */
		public static Collection<myData.Profile_genres> adapt(Collection<myData.Profile_genres> genres, 
				double interestThreshold, int delta) {


			Iterator<myData.Profile_genres> it = genres.iterator();
			myData.Profile_genres entry;
			//Go over all genres
			while (it.hasNext()) { 
				entry = it.next();
				if (! entry.isIncurrent())
					continue;
				// if there were trailers "lately" 
				if ( Formulae.datesDelta(entry.getDateOfLastMovie(), 
						/*today*/ new Date()) < delta ) {
					//only then you can be sure your interest is correct
					if ( entry.getInterest() < interestThreshold ) {
						entry.moveToFuture();
					}
				}
				else {
					entry.moveToFuture();
				}	

			} //end while

			return genres;
		}

		private boolean isRatingPos(double rating) {
			return Trailers_of_profile.isDocumentPositive(rating);
		}
}
