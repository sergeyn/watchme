package myData;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Learning_support {
	private String profileId;
	private String trailerId;
	private double rating;
	private double Tn1; 

	private List<Integer> genres_of_trailer = null;
	Collection<Profile_Trailer_Genres> allTrailersInGenre = null;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public Learning_support(String profileId, String trailerId, double rating, double Tn1) {
		super();
		this.profileId = profileId;
		this.trailerId = trailerId;
		this.rating = rating;
		this.Tn1 = Tn1;
	}

	public void learnAndUpdateDB() {

		// This is what we want to store in the db
		Profile_Trailer_Genres TrailerScoreUpdater = new Profile_Trailer_Genres();
		String isPosString = (Trailers_of_profile.isDocumentPositive(rating)) ? "Y" : "N";
		Iterator<Integer> genresIt = null;

		// Set the duplicate details of Profile_Trailer_Genres entries of this
		// trailer:

		TrailerScoreUpdater.setProfileId(profileId);
		TrailerScoreUpdater.setTrailerId(trailerId);
		TrailerScoreUpdater.setIsPositive(isPosString);
		TrailerScoreUpdater.setViewingDate(new Date());

		// now we want all the genres of this trailer
		try {
			genres_of_trailer = CommonFunctions.getGenresbyTrailerId(trailerId);
			if ((genres_of_trailer == null) || (genres_of_trailer.isEmpty())) {
				System.out.println("List of genres is Null or empty");
				return;
			}
			
			// loop over genres of this trailer
			genresIt = genres_of_trailer.iterator();
			while (genresIt.hasNext()) {
				Integer genreId = genresIt.next();
				TrailerScoreUpdater.setGenreId(genreId);
				updateOneGenre(genreId, TrailerScoreUpdater);
			}
			
		} // WTF: a trailer with no genre?
		catch (SQLException se) {
			System.out.println("List of genres is Null or empty"
					+ se.getMessage());
			return;
		}
	}
//================================================================================
	private void updateOneGenre(Integer singleGenre, Profile_Trailer_Genres TrailerScoreUpdater) {
		
		int todaycountgenre;
		myData.Profile_genres profileGenre = null;

		// get profile_genre from profile by genre id

		// get a list of trailers_of_profile where profile... and genreID ==
		// singleGenre
		Profile_genres newPG = new Profile_genres();
		newPG.setProfileId(profileId);
		newPG.setGenreId(singleGenre);
		//TODO: set dates
		// 3 * profileGenre.setTimeOfLastMovie(timeOfLastMovie)
		

		try {

			// System.out.println("Profile_Genres Id " +
			// newPG.getProfileId() + "genre " + newPG.getGenreId());
			System.out.println("profile genre is " + newPG.getProfileId() + "genre is " + newPG.getGenreId() );
			if (null == (profileGenre = CommonFunctions.selectPGbyPG(newPG))) {
				System.out.println("Achtung - if (profileGenre==null)");return;
			}
			//update sequences
			UpdateSequence(profileGenre.getNegativeSequence(), profileGenre.getPositiveSequence(),profileGenre.getProfileId(),profileGenre.getGenreId());
			
			todaycountgenre = CommonFunctions.getTodaysSeenTrailersbyGenre(TrailerScoreUpdater);
			System.out.println("todaycountgenre from db: " + todaycountgenre);
			if (null == (allTrailersInGenre = CommonFunctions.getSeenTrailersbyGenre(profileGenre)))
					{System.out.println("Achtung - if (Alltrailers(null))");return;}
		} catch (SQLException se) {
			System.out.println("List of genres is Null or empty"
					+ se.getMessage());
			return;
		}


		Learning learning = new Learning(0.3,// double ubeta,
				Tn1,// double uTn1,
				0.5,// double ugstune,
				21// int uposNegTune
		);


		Learning.RetTuple score_interest = learning.learn(rating, profileGenre,
				todaycountgenre + 1, 3, allTrailersInGenre);
		//System.out.println("interest: " + score_interest.genreInterest);
		//System.out.println("score: " + score_interest.trailerScore);
		// temp.setScore(score_interest.trailerScore);
		profileGenre.setInterest(score_interest.genreInterest);
		profileGenre.setNoOfMovies(profileGenre.getNoOfMovies() + 1);

		String todayAsString = sdf.format(new Date());

		profileGenre.setTimeOfLastMovie(todayAsString);
		
		if (Trailers_of_profile.isDocumentPositive(rating))
			profileGenre.setTimeOfLastPositive(todayAsString);
		else
			profileGenre.setTimeOfLastNegative(todayAsString);

		TrailerScoreUpdater.setGenreId(singleGenre);
		TrailerScoreUpdater.setGenreScore(score_interest.trailerScore);
		System.out.println("genreScore " + TrailerScoreUpdater.getGenreScore());

		try {
			CommonFunctions.InsertGenresScore(TrailerScoreUpdater); // save in tralier_profile_genre table
			CommonFunctions.updateLearning(profileGenre); // update the relevant genre
		} catch (SQLException e) {
			System.out.println("problem updating tables in learnAndUpdateDB");
			System.out.println(e.getMessage());
		}

	}
/*updating the new positive and negative sequence*/	
	void UpdateSequence(int nSequence, int pSequence, String profileId, int genreId)
	{
		if (rating>2){
			pSequence++;
			nSequence=0;
			System.out.println(" genre " + genreId + "nSequence=0, pSequence= " + pSequence );
		}
		else {
			nSequence++;
			pSequence=0;
			System.out.println(" genre " + genreId + "pSequence=0, nSequence= " + nSequence );
		}
		myData.Profile_genres profileGenre = new myData.Profile_genres();
		profileGenre.setGenreId(genreId);
		profileGenre.setProfileId(profileId);
		profileGenre.setNegativeSequence(nSequence);
		profileGenre.setPositiveSequence(pSequence);
		
		try {
			myData.CommonFunctions.updateSequence(profileGenre);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
