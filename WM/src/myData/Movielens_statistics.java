package myData;

public class Movielens_statistics {
	
	private String genreName = "";
	private double mean = 0;
	private double sd = 0;
	/**
	 * @return the genreName
	 */
	public String getGenreName() {
		return genreName;
	}
	/**
	 * @param genreName the genreName to set
	 */
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
	/**
	 * @return the mean
	 */
	public double getMean() {
		return mean;
	}
	/**
	 * @param mean the mean to set
	 */
	public void setMean(double mean) {
		this.mean = mean;
	}
	/**
	 * @return the sd
	 */
	public double getSd() {
		return sd;
	}
	/**
	 * @param sd the sd to set
	 */
	public void setSd(double sd) {
		this.sd = sd;
	}
	
	
}
