package myData;
public class Multi_Genre {

	private String trailerId = "";
	private int genreId1=0;
	private int genreId2=0;
	private String profileId = "";
	
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getTrailerId() {
		return trailerId;
	}
	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}
	public int getGenreId1() {
		return this.genreId1;
	}
	public void setGenreId1(int genreId1) {
		this.genreId1 = genreId1;
	}
	public int getGenreId2() {
		return this.genreId2;
	}
	public void setGenreId2(int genreId2) {
		this.genreId2 = genreId2;
	}
	
}