package myData;
public class Profile {

	private String  profileId = "0";
	private String  password = "";
	private int currentStrategyId = 2;
	private double sw = -2;
	private String isFormNeeded = "Y";
	private int age = 0;
	private	String gender = "";
	private	String education = "";
	private	int occupation = 0;
	private int daysMissed = 0;
	private int experimentGroupId = 0;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getCurrentStrategyId() {
		return currentStrategyId;
	}
	public void setCurrentStrategyId(int currentStrategyId) {
		this.currentStrategyId = currentStrategyId;
	}
	public int getDaysMissed() {
		return daysMissed;
	}
	public void setDaysMissed(int daysMissed) {
		this.daysMissed = daysMissed;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIsFormNeeded() {
		return isFormNeeded;
	}
	public void setIsFormNeeded(String isFormNeeded) {
		this.isFormNeeded = isFormNeeded;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public double getSw() {
		return sw;
	}
	public void setSw(double sw) {
		this.sw = sw;
	}
	public int getOccupation() {
		return occupation;
	}
	public void setOccupation(int occupation) {
		this.occupation = occupation;
	}
	public int getExperimentGroupId() {
		return experimentGroupId;
	}
	public void setExperimentGroupId(int experimentGroupId) {
		this.experimentGroupId = experimentGroupId;
	}
}
