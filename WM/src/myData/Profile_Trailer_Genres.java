package myData;

import java.util.Date;

public class Profile_Trailer_Genres {
	
	private String profileId = "";
	private String trailerId = "";
	private int genreId = 0;
	private String isPositive = "N";
	private double genreScore = -1.0;
	Date viewingDate = new Date ();
	
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getTrailerId() {
		return trailerId;
	}
	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	public String getIsPositive() {
		return isPositive;
	}
	public void setIsPositive(String isPositive) {
		this.isPositive = isPositive;
	}
	public double getGenreScore() {
		return genreScore;
	}
	public void setGenreScore(double genreScore) {
		this.genreScore = genreScore;
	}
	public Date getViewingDate() {
		return viewingDate;
	}
	public void setViewingDate(Date viewingDate) {
		this.viewingDate = viewingDate;
	}
	

}
