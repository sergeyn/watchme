package myData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Profile_genres {

	private String profileId = "";
	private int genreId = 0;
	private Date date = new Date();
	private int noOfMovies = 0;
	//TODO: [P|R] real dates...
	private String timeOfLastMovie = "2005-01-01";
	private double userWeight = 0;
	private String isCurrentInterest = "";
	private double  score = 0;
	private double interest = 0;
	private int positiveSequence = 0;
	private int negativeSequence = 0;
	private String timeOfLastPositive = "2005-01-01";
	private String timeOfLastNegative = "2005-01-01";
	private String demographicRecommender = "";
	private double demographicWeight = 1.0;
	private String wasRecommended = "N";
	
	/**
	 * @return the wasRecommended
	 */
	public String getWasRecommended() {
		return wasRecommended;
	}
	/**
	 * @param wasRecommended the wasRecommended to set
	 */
	public void setWasRecommended(String wasRecommended) {
		this.wasRecommended = wasRecommended;
	}
	/* L&A add */ 
	private Date strToDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 Date d = null;
		 try {
			d =  sdf.parse(date);
		} catch (ParseException e) {
			System.out.println("Not good format: getDateOfLastMovie");
			d = new Date();
		}

		return d; 
	}
	public Date getDateOfLastMovie() { 
		return strToDate(timeOfLastPositive);
	}
	
	public Date getDateOfLastPosMovie() { return strToDate(timeOfLastPositive);}
	public Date getDateOfLastNegMovie() { return strToDate(timeOfLastNegative);}
	public boolean isIncurrent() { return isCurrentInterest.equals("Y"); }
	public void moveToFuture() { this.setIsCurrentInterest("N"); }
	/* End L&A */
	public int getPositiveSequence() {
		return positiveSequence;
	}
	public void setPositiveSequence(int positiveSequence) {
		this.positiveSequence = positiveSequence;
	}
	public int getNegativeSequence() {
		return negativeSequence;
	}
	public void setNegativeSequence(int negativeSequence) {
		this.negativeSequence = negativeSequence;
	}
	public String getTimeOfLastPositive() {
		return timeOfLastPositive;
	}
	public void setTimeOfLastPositive(String timeOfLastPositive) {
		this.timeOfLastPositive = timeOfLastPositive;
	}
	public String getTimeOfLastNegative() {
		return timeOfLastNegative;
	}
	public void setTimeOfLastNegative(String timeOfLastNegative) {
		this.timeOfLastNegative = timeOfLastNegative;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public String getIsCurrentInterest() {
		return isCurrentInterest;
	}
	public void setIsCurrentInterest(String isCurrentInterest) {
		this.isCurrentInterest = isCurrentInterest;
	}
	public int getNoOfMovies() {
		return noOfMovies;
	}
	public void setNoOfMovies(int noOfMovies) {
		this.noOfMovies = noOfMovies;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getTimeOfLastMovie() {
		return timeOfLastMovie;
	}
	public void setTimeOfLastMovie(String timeOfLastMovie) {
		this.timeOfLastMovie = timeOfLastMovie;
	}
	public double getUserWeight() {
		return userWeight;
	}
	public void setUserWeight(double userWeight) {
		this.userWeight = userWeight;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the demographicRecommender
	 */
	public String getDemographicRecommender() {
		return demographicRecommender;
	}
	/**
	 * @param demographicRecommender the demographicRecommender to set
	 */
	public void setDemographicRecommender(String demographicRecommender) {
		this.demographicRecommender = demographicRecommender;
	}
	public double getDemographicWeight() {
		return demographicWeight;
	}
	public void setDemographicWeight(double demographicWeight) {
		this.demographicWeight = demographicWeight;
	}
}
