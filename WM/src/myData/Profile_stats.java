package myData;
public class Profile_stats {

	private String profileId = "";
	private String genreId = "";
	private String date = "";
	private float genreInterest = -1;
	private int genrePositiveSequence = 0;
	private int genreNegativeSequence = 0;
	private int totalTrailersPerDay = 0;
	private int newInterestCount = 0;
	private int negativeChangeCount = 0;
	private int interestConfirmCount = 0;
	private float avgChanges = 0;
	private float avgPercentage = 0;
	
	public float getAvgChanges() {
		return avgChanges;
	}
	public void setAvgChanges(float avgChanges) {
		this.avgChanges = avgChanges;
	}
	public float getAvgPercentage() {
		return avgPercentage;
	}
	public void setAvgPercentage(float avgPercentage) {
		this.avgPercentage = avgPercentage;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getInterestConfirmCount() {
		return interestConfirmCount;
	}
	public void setInterestConfirmCount(int interestConfirmCount) {
		this.interestConfirmCount = interestConfirmCount;
	}
	public int getNegativeChangeCount() {
		return negativeChangeCount;
	}
	public void setNegativeChangeCount(int negativeChangeCount) {
		this.negativeChangeCount = negativeChangeCount;
	}
	public int getNewInterestCount() {
		return newInterestCount;
	}
	public void setNewInterestCount(int newInterestCount) {
		this.newInterestCount = newInterestCount;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public int getTotalTrailersPerDay() {
		return totalTrailersPerDay;
	}
	public void setTotalTrailersPerDay(int totalTrailersPerDay) {
		this.totalTrailersPerDay = totalTrailersPerDay;
	}
	public String getGenreId() {
		return genreId;
	}
	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}
	public float getGenreInterest() {
		return genreInterest;
	}
	public void setGenreInterest(float genreInterest) {
		this.genreInterest = genreInterest;
	}
	public int getGenrePositiveSequence() {
		return genrePositiveSequence;
	}
	public void setGenrePositiveSequence(int genrePositiveSequence) {
		this.genrePositiveSequence = genrePositiveSequence;
	}
	public int getGenreNegativeSequence() {
		return genreNegativeSequence;
	}
	public void setGenreNegativeSequence(int genreNegativeSequence) {
		this.genreNegativeSequence = genreNegativeSequence;
	}
}
