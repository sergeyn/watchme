package myData;
public class Profile_strategies_changes {

	private String profileId = "";
	private int strategyId = 0;
	private double attractionToChangeMean = 0;
	private double winceFromChangeMean = 0;
	private double winceFromStabilityMean = 0;
	private double attractionToStabilityMean = 0;
	private double certaintyMean = 0;
	private String date = "0000-00-00";
	
	public double getAttractionToChangeMean() {
		return attractionToChangeMean;
	}
	public void setAttractionToChangeMean(double attractionToChangeMean) {
		this.attractionToChangeMean = attractionToChangeMean;
	}
	public double getAttractionToStabilityMean() {
		return attractionToStabilityMean;
	}
	public void setAttractionToStabilityMean(double attractionToStabilityMean) {
		this.attractionToStabilityMean = attractionToStabilityMean;
	}
	public double getCertaintyMean() {
		return certaintyMean;
	}
	public void setCertaintyMean(double certaintyMean) {
		this.certaintyMean = certaintyMean;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public int getStrategyId() {
		return strategyId;
	}
	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}
	public double getWinceFromChangeMean() {
		return winceFromChangeMean;
	}
	public void setWinceFromChangeMean(double winceFromChangeMean) {
		this.winceFromChangeMean = winceFromChangeMean;
	}
	public double getWinceFromStabilityMean() {
		return winceFromStabilityMean;
	}
	public void setWinceFromStabilityMean(double winceFromStabilityMean) {
		this.winceFromStabilityMean = winceFromStabilityMean;
	}
}
