/**
 * 
 */
package myData;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ibatis.sqlmap.engine.builder.xml.SqlMapParser;

/**
 * 
 *
 */
public class Recommender {

	public static Trailers Recommend(Profile profile){
		Trailers trailer = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String todayAsString = sdf.format(new Date());
		List currentGenres = null;
		
		List listOfMultiGenreTrailerIds =null;
		try {
			currentGenres = CommonFunctions.getCurrentInterestofProfile(profile.getProfileId());
			//System.out.println("size: " +currentGenres.size());
		} catch (SQLException e) {
			//random

		}

		Profile_genres profile_genres = new Profile_genres();
		profile_genres.setProfileId(profile.getProfileId());
		
		int currentInterestsCount = currentGenres.size();
		
		while ( currentGenres.isEmpty()) {
			//Insert that user into the Users_in_future table
			
			Users_in_future newUserinFuture = new Users_in_future();
				newUserinFuture.setProfileId(profile_genres.getProfileId());
				System.out.println(" update future "  );
				newUserinFuture.setDate(todayAsString);
				try {
					CommonFunctions.insertUserintoTheFuture(newUserinFuture);
				} 
				catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.getMessage();
				}
			
			//move from future some to current - and put in List
			System.out.println("opan'ki future\n");
		}
		
		if (currentInterestsCount < 3) returnFromFuture(profile_genres, 3-currentInterestsCount);
			/*CommonFunctions.ReturntoCurrent(profile_genres);
			try {
				currentGenres = CommonFunctions.getCurrentInterestofProfile(profile.getProfileId());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.getMessage();
			} */
		
		// Exp.Group (1) = BaseLine + Suffocate User
/*		if (profile.getExperimentGroupId()==1){
			Iterator genresIterator = currentGenres.iterator();
			while (trailer == null && genresIterator.hasNext()){
				//recommend for genre that has the highest interest
				// recommend( currentGenres[0] ); 

				profile_genres.setGenreId( ((Profile_genres)genresIterator.next()).getGenreId());
				trailer = GetTrailerBySingleGenre(profile_genres);
			}

			if (trailer == null) //one of 10^6
				System.out.println("Zhopa " );//random
		}
		*/
		if (profile.getExperimentGroupId()==0 || profile.getExperimentGroupId()==1){
			//recommend via random over all interesting genres:
			//recommend( currentGenre [ Math.randomInt(currentGenres.size()) ] ;
			while (trailer == null ){
				if (currentGenres.size()==1){
					//disable random and get the first element explicitly
					profile_genres.setGenreId(CurrentInterestSizeIsOne(profile_genres, currentGenres));
					System.out.println("current interest = 1, group "+profile.getExperimentGroupId() +" genreId = " +profile_genres.getGenreId());
				}
				else 
				profile_genres.setGenreId( (
						(Profile_genres) currentGenres.get(new Random().nextInt(currentGenres.size()))).getGenreId());
				trailer = GetTrailerBySingleGenre(profile_genres);
				System.out.println("trailer from group 0/ 1 : " + trailer.getTrailerId());
			}
		}
		//change groupId 2 to 3 
		if (profile.getExperimentGroupId()==3){
			//double recommend using multiple
			//genre2 =  genre1 = currentGenre [ Math.randomInt(currentGenres.size()) ] ;
			//while genre2 == genre1 do genre2 = currentGenre [ Math.randomInt(currentGenres.size()) ] ;
			//return getTrailerByIdFromMulti(getTwoRecommender(genre1.getid,genre2.getid));
			while (trailer == null ){
				Multi_Genre multi_genres = new Multi_Genre();
				int GenresCount = currentGenres.size();
				// In case there is only 1 genre is the Current Interest
				int first = -1;
				int second = -1;
				if (GenresCount==1){
					//first = new Random().nextInt(GenresCount);
					//second = first;
					//first = (Integer)((Profile_genres)currentGenres.get(0)).getGenreId();
					second = first = 0;
					//System.out.println(" only 1 current interest, genreId =  " +first);
				}
				else {	
						first = new Random().nextInt(GenresCount);
						second = getOtherInt(first, GenresCount);
				}
				multi_genres.setGenreId1(((Profile_genres) currentGenres.get(first)).getGenreId());
				multi_genres.setGenreId2(((Profile_genres) currentGenres.get(second)).getGenreId());
				String trailerId ="";
				multi_genres.setProfileId(profile.getProfileId());
				try {
					//trailersIds = CommonFunctions.getMultiGenreTrailer(multi_genres);
					System.out.println(" firstGenreId = "+ multi_genres.getGenreId1() + "secondGenreId = " + multi_genres.getGenreId2());
					
					//get all trailerIds having 2 genreIds from multi_genres
					listOfMultiGenreTrailerIds = CommonFunctions.getListofMulitGenreTrailerIDs(multi_genres);
					
					//Choose random trailer from the above list
					if(listOfMultiGenreTrailerIds.size()==0){
						trailerId=null;
						System.out.println("No movie for the provided genres");
					}					
					if (listOfMultiGenreTrailerIds.size()==1){
						System.out.println("Only 1 trailer in list, group "+profile.getExperimentGroupId() +" genreId = " +profile_genres.getGenreId());
						trailerId = (String) listOfMultiGenreTrailerIds.get(0);
					}
					else if (listOfMultiGenreTrailerIds.size()>1)
						trailerId = (String) listOfMultiGenreTrailerIds.get(new Random().nextInt(listOfMultiGenreTrailerIds.size()));
					//trailerId = CommonFunctions.getMultiGenreTrailer(multi_genres);
					System.out.println("multigenreTrailerId: " + trailerId);
				} catch (SQLException e) {
					e.getMessage();
				}
				//String trailerid = (String) trailersIds.get(new Random().nextInt(trailersIds.size()));
				//TODO: check if trailerId is null
				if ((null==trailerId)|| (trailerId.equals(""))){
					while (trailer == null ){
						if (currentGenres.size()==1){
							//disable random and get the first element explicitly
							profile_genres.setGenreId(CurrentInterestSizeIsOne(profile_genres, currentGenres));
							System.out.println("CurrentInterests = 1, no random, genreId = " +profile_genres.getGenreId() );
						}
						else	
							profile_genres.setGenreId(((Profile_genres) currentGenres.get(new Random().nextInt(currentGenres.size()))).getGenreId());
						trailer = GetTrailerBySingleGenre(profile_genres);
						System.out.println("multi zhopa -> single" + trailer.getTrailerId());
					}
				}
				else try {
					trailer = CommonFunctions.getTrailerbyId(trailerId);
					System.out.println("multi = success" + trailer.getTrailerId());
				} catch (SQLException e) {
					e.getMessage();
				}
			}
		}
		return trailer;
	}
	
	private static int getOtherInt(int first, int max) {
		int retSecond = first;
		Random rnd = new Random();
		while ( retSecond == first ) {
			retSecond = rnd.nextInt(max);
		}
		return retSecond;
	}
	
	private static boolean returnFromFuture (Profile_genres profile_genres,int amountOfGenresToReturn){
		List<Profile_genres> futureInterest=null;
		
		//in case we have nothing to return
		if (amountOfGenresToReturn<=0) return false;
		try {
			futureInterest = CommonFunctions.getFutureInterestsofProfile(profile_genres.getProfileId());
			
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
		}
		
		if (futureInterest.size()<1){
			System.out.println("Future interest problem");
			return false;
		}
		if (futureInterest.size()<amountOfGenresToReturn)
			amountOfGenresToReturn = futureInterest.size();
		ListIterator<Profile_genres> listIterator = futureInterest.listIterator();
		for (int i=0;i<amountOfGenresToReturn;i++){
			
			//List of genres is sorted by the highest weight so each time we return genre with the highest 
			//weight out of the others
			
			Profile_genres updateProfileGenres = listIterator.next();
			try {
				CommonFunctions.ReturnSingleGenretoCurrent(updateProfileGenres);
				System.out.println("Trying to Transfer to current " + " profileId is "+updateProfileGenres.getProfileId() +" genreId is: " +updateProfileGenres.getGenreId());
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Transfer to current has failed," + " profileId is "+updateProfileGenres.getProfileId() +" genreId is: " +updateProfileGenres.getGenreId());
				e.getMessage();
		
			}
			
		}
		
		
		switch (amountOfGenresToReturn){
		case 1:{
			profile_genres.setGenreId(((Profile_genres)futureInterest.get(0)).getGenreId());
			CommonFunctions.ReturnSingleGenretoCurrent(profile_genres);
		}
			
		}
		
		return true;
	}
	
	private static int CurrentInterestSizeIsOne (Profile_genres profile_genres,List currentGenres){
		return (((Profile_genres)currentGenres.get(0)).getGenreId());
	}
	
	private static Trailers GetTrailerBySingleGenre(Profile_genres profile_genres){

		try {
			return CommonFunctions.getSingleGenreTrailer(profile_genres);
		} catch (SQLException e) {
			System.out.println("Zhopa v CommonFunctions.getSingleGenreTrailer" + e.getMessage() + profile_genres.getProfileId());
		}
		return null;
	}


}
