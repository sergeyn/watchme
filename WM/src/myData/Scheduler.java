package myData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Scheduler {
  
	boolean stopped = true;
	Timer timer;
	String prevDate = "";
	
  // constructor
  public Scheduler(){}

  public boolean isStopped(){
	  return stopped;
  }	
  
  public void cancel(){
	  timer.cancel();
	  stopped = true;
  }
  
  public void start(int seconds){
		timer = new Timer();
		timer.schedule(new Tasks(),1000,seconds * 1000);
		stopped = false;
  }
	
  class Tasks extends TimerTask {

	GMail m = new GMail();
	 
	
	public void run() {
		// remove
		List usersList = null;
		List graduatedUsers = null; // users that finished 45 day of ratings
		
		
		
		Profile p = null;
		String tempPrevDate = prevDate;   

		if (!isSendingTime()) return;
		
		
		
		System.out.println("Last checked date:"+tempPrevDate);
		String daylymessage;
		String daymissedmessage;
		SimpleDateFormat sdf = new SimpleDateFormat ("dd.MM");
		String date = sdf.format(new Date());
	
		daylymessage = "Dear participant,"+((char)10)+((char)13)+((char)10)+((char)13)+"There are three trailers waiting for you at http://pitv.haifa.ac.il/WM/login.jsp"+((char)10)+((char)13)+
			"Please visit our site by clicking on the link above."+((char)10)+((char)13)+((char)10)+((char)13)+"In case of any problems don't hesitate to tell us about it by replying to this email."+
			"Thank you for cooperation,patience and support,"+((char)10)+((char)13)+"Watchme team";

		try 
		{
			CommonFunctions.addDaysMissedToBadUsers(tempPrevDate);
			usersList = CommonFunctions.getBadUsersPerDay(tempPrevDate);
			graduatedUsers = CommonFunctions.graduatedUsers();
			
			// 	REMOVE GRADUATED USERS --------------------------------------------
			for(int i=0;i<usersList.size();i++){
				String pId = ((Profile)usersList.get(i)).getProfileId();
				for(int j=0;j<graduatedUsers.size();j++)
					if (pId.equalsIgnoreCase((String)graduatedUsers.get(j))){
						usersList.remove(i);
						i--;
						break;
					}
			}
			// ---------------------------------------------------------------------
			for(int i=0;i<usersList.size();i++){
				p = (Profile)usersList.get(i);
				if (p.getDaysMissed()>3)
					
					
					// Mail to us that user hasn't entered the watchme for 4 and more days 
					{
					daymissedmessage ="Dear participant,"+((char)10)+((char)13)+((char)10)+((char)13)+"You've not entered the system for " +p.getDaysMissed()+ " days" +((char)10)+((char)13)+"There are three trailers waiting for you at http://pitv.haifa.ac.il/WM/login.jsp"+((char)10)+((char)13)+
					"Please visit our site by clicking on the link above."+((char)10)+((char)13)+((char)10)+((char)13)+"In case of any problems don't hesitate to tell us about it by replying to this email."+
					"Thank you for cooperation,patience and support,"+((char)10)+((char)13)+"Watchme team";
					
					System.out.println("We have user ["+p.getProfileId()+"] that hasn't entered "+p.getDaysMissed()+ " days!");
					m.sendMessage("admin.watchme@gmail.com", "admin.watchme@gmail.com", "Very BAD user, doesn't want to enter the system!", "ID=["+p.getProfileId()+"] DaysMissed="+p.getDaysMissed());
					m.sendMessage("admin.watchme@gmail.com", p.getProfileId(), "Watchme Experiment Reminder", daymissedmessage);
					
					}
				else if (p.getDaysMissed()>1)
				// if user hasnt entered for more than one day.	
				{
					daymissedmessage ="Dear participant,"+((char)10)+((char)13)+((char)10)+((char)13)+"You've not entered the system for " +p.getDaysMissed()+ " days" +((char)10)+((char)13)+"There are three trailers waiting for you at http://pitv.haifa.ac.il/WM/login.jsp"+((char)10)+((char)13)+
					"Please visit our site by clicking on the link above."+((char)10)+((char)13)+((char)10)+((char)13)+"In case of any problems don't hesitate to tell us about it by replying to this email."+
					"Thank you for cooperation,patience and support,"+((char)10)+((char)13)+"Watchme team";
					m.sendMessage("admin.watchme@gmail.com", p.getProfileId(), "Watchme Experiment - Invitation to watch trailers today "+ date, daymissedmessage);
				}
					else{
				// user missed only one day... means still a good user	
				//m.sendMessage("admin.watchme@gmail.com", "admin.watchme@gmail.com", "Mail was sent to", "ID=["+p.getProfileId()+"] DaysMissed="+p.getDaysMissed());
				m.sendMessage("admin.watchme@gmail.com", p.getProfileId(), "Watchme Experiment - Invitation to watch trailers today "+ date, daylymessage);	
					}
			}
			
			CommonFunctions.setDaysMissedToZeroToGoodUsers(tempPrevDate);
			usersList = CommonFunctions.getGoodUsersPerDay(tempPrevDate);
			
			//	 REMOVE GRADUATED USERS --------------------------------------------
			for(int i=0;i<usersList.size();i++){
				String pId = ((Profile)usersList.get(i)).getProfileId();
				for(int j=0;j<graduatedUsers.size();j++)
					if (pId.equalsIgnoreCase((String)graduatedUsers.get(j))){
						usersList.remove(i);
						i--;
						break;
					}
			}
			// ---------------------------------------------------------------------

			
			for(int i=0;i<usersList.size();i++){
				p = (Profile)usersList.get(i);
				// good users
				m.sendMessage("admin.watchme@gmail.com", p.getProfileId(), "Watchme Experiment - Invitation to watch trailers today "+ date, daylymessage);
				//m.sendMessage("admin.watchme@gmail.com", "admin.watchme@gmail.com", "Scheduler works", "ID=["+p.getProfileId());
			}
		} 
		catch (SQLException e) {e.getMessage();}  
		
      // timer.cancel(); //Not necessary because we call System.exit
      // System.exit(0); //Stops the AWT thread (and everything else)
    }
	
	private boolean isSendingTime(){
		Calendar now = Calendar.getInstance();  
		
		int month = now.get(Calendar.MONTH)+1;
		int day  = now.get(Calendar.DATE);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		
		if (now.get(Calendar.DAY_OF_WEEK)==7) return(false); 	// shabat
		if (day==23 && month == 5) return(false); 				// lag ba omer
		if (day==9  && month == 6) return(false); 				// shavuot
		if (hour<1) 	return(false); 							// too early
		
		String currDate = day + "/" + month + "/2008";
		if (prevDate.equals("")) {prevDate=currDate;return(false);} // initializing at start
		if (prevDate.equals(currDate)) return(false); 				// this day is closed 
		prevDate = currDate;
		return(true);
	}
	 
  }
 // public static void main(String args[]){new Scheduler(5); } // once in 5 secs
}

