package myData;
public class Strategy {

	private int strategyId = 0;
	private String typeDescription ="";
	private double attractionToChange  = 0;
	private double winceFromChange  = 0;
	private double  winceFromStability  = 0;
	private double attractionToStability  = 0;
	private double certainty  = 0;
	
	public double getAttractionToChange() {
		return attractionToChange;
	}
	public void setAttractionToChange(double attractionToChange) {
		this.attractionToChange = attractionToChange;
	}
	public double getAttractionToStability() {
		return attractionToStability;
	}
	public void setAttractionToStability(double attractionToStability) {
		this.attractionToStability = attractionToStability;
	}
	public double getCertainty() {
		return certainty;
	}
	public void setCertainty(double certainty) {
		this.certainty = certainty;
	}
	public int getStrategyId() {
		return strategyId;
	}
	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}
	public String getTypeDescription() {
		return typeDescription;
	}
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}
	public double getWinceFromChange() {
		return winceFromChange;
	}
	public void setWinceFromChange(double winceFromChange) {
		this.winceFromChange = winceFromChange;
	}
	public double getWinceFromStability() {
		return winceFromStability;
	}
	public void setWinceFromStability(double winceFromStability) {
		this.winceFromStability = winceFromStability;
	}
}
