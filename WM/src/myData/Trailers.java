package myData;
public class Trailers {

	private String trailerId = "";
	private String title = "";
	private int year = 1900;
	private String youTubeLink = "xxxxxxxxxxx";
	private String plot = "";
	private String studio = "";
	private String director ="";
	private int youTubeRating = 0;
	private String stems = "";
	private double duration = 0;
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public String getPlot() {
		return plot;
	}
	public void setPlot(String plot) {
		this.plot = plot;
	}
	public String getStems() {
		return stems;
	}
	public void setStems(String stems) {
		this.stems = stems;
	}
	public String getStudio() {
		return studio;
	}
	public void setStudio(String studio) {
		this.studio = studio;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTrailerId() {
		return trailerId;
	}
	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getYouTubeLink() {
		return youTubeLink;
	}
	public void setYouTubeLink(String youTubeLink) {
		this.youTubeLink = youTubeLink;
	}
	public int getYouTubeRating() {
		return youTubeRating;
	}
	public void setYouTubeRating(int youTubeRating) {
		this.youTubeRating = youTubeRating;
	}
}
