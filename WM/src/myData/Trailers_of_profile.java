package myData;
public class Trailers_of_profile {
	
	private String profileId = "";
	private String TrailerId = "";
	private int Rating = 0;
	private String RDate = "0000-00-00 00:00:00";
	private boolean wasSeen = false;
	private boolean wasRecommended = false;
	private String startTime = "0000-00-00 00:00:00";
	private String endTime = "0000-00-00 00:00:00";
	private int actualWatchingTime=0;
	
	/* Start L&A */
	private double score;
	public double getScore() {	return score;	}
	public void setScore(double score) { this.score = score; 	}
	public boolean isDocumentPos() { return Rating >= 3; }
	 
	/* End L&A */ 
	
	public int getActualWatchingTime() {
		return actualWatchingTime;
	}
	public void setActualWatchingTime(int actualWatchingTime) {
		this.actualWatchingTime = actualWatchingTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public int getRating() {
		return Rating;
	}
	public void setRating(int rating) {
		Rating = rating;
	}
	public String getRDate() {
		return RDate;
	}
	public void setrDate(String date) {
		RDate = date;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getTrailerId() {
		return TrailerId;
	}
	public void setTrailerId(String trailerId) {
		TrailerId = trailerId;
	}
	public boolean isWasRecommended() {
		return wasRecommended;
	}
	public void setWasRecommended(boolean wasRecommended) {
		this.wasRecommended = wasRecommended;
	}
	public boolean isWasSeen() {
		return wasSeen;
	}
	public void setWasSeen(boolean wasSeen) {
		this.wasSeen = wasSeen;
	}
	static public boolean isDocumentPositive(double rating) {
		
		return rating >= 0.1;
	}
}
