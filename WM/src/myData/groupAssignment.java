package myData;
import java.sql.SQLException;
import java.util.*;


public class groupAssignment {
	
	private List profiles = null;
	private int group1s1 = 0, group1s2 = 0, group1s3 = 0, group0s1 = 0, group0s2 = 0, group0s3 = 0, group2s1 = 0, group2s2 = 0, group2s3 = 0;
	private int chosenGroup = -1;
	private int strategy = -1;
	private myData.Profile profile = null;
	
	public void assignToGroups(){
		
		int i = 0;
	//	myData.Experiment_groups experiment_group = new myData.Experiment_groups();
		
		//	select all profiles 
		try {
			profiles = myData.CommonFunctions.selectProfiles();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//assign groups 0-2
		for(; i<profiles.size();i++){//run through profiles	
			profile= (Profile)profiles.get(i);
			strategy=profile.getCurrentStrategyId();
			chosenGroup=findMinGroupForStrategy(strategy);
			if(chosenGroup!=-1){
				profile.setExperimentGroupId(chosenGroup);
				try {
					myData.CommonFunctions.updateExperimentGroupId(profile);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else System.out.println("user " + profile.getProfileId() + "has strategy " + profile.getCurrentStrategyId());
		}
		
		
		//update experiment_groups table
		int j = 1;
		
		
		myData.Experiment_groups experiment_group1= new Experiment_groups(0,1,group0s1);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group2= new Experiment_groups(0,2,group0s2);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group3= new Experiment_groups(0,3,group0s3);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group3);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group4= new Experiment_groups(1,1,group1s1);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group4);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group5= new Experiment_groups(1,2,group1s2);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group5);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group6= new Experiment_groups(1,3,group1s3);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group6);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group7= new Experiment_groups(2,1,group2s1);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group7);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group8= new Experiment_groups(2,2,group2s2);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group8);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myData.Experiment_groups experiment_group9= new Experiment_groups(2,3,group2s3);
		try {
			myData.CommonFunctions.updateExperimentGroupCount(experiment_group9);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	
	
	public int findMinGroupForStrategy(int strategy){
		
		if (strategy==1){
			if((group0s1<=group1s1) && (group0s1<=group2s1)){
				group0s1++;
				return 0;
			}
			else if ((group1s1<=group0s1) && (group1s1<=group2s1)){
				group1s1++;
				return 1;
			}
			else {
				group2s1++;
				return 2;		
			}
		}
		
		if (strategy==2){
			if((group0s2<=group1s2) && (group0s2<=group2s2)){
				group0s2++;
				return 0;
			}
			else if ((group1s2<=group0s2) && (group1s2<=group2s2)){
				group1s2++;
				return 1;
			}
			else {
				group2s2++;
				return 2;		
			}
		}
		
		if (strategy==3){
			if((group0s3<=group1s3) && (group0s3<=group2s3)){
				group0s3++;
				return 0;
			}
			else if ((group1s3<=group0s3) && (group1s3<=group2s3)){
				group1s3++;
				return 1;
			}
			else {
				group2s3++;
				return 2;		
			}
		}
		else return -1;	
	}
	
}

	
