package myData;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.ArrayList;
public class importCSV {
	
	/*  >>Prefixes<<
	 
	    Actions – Ac
		Adventures – Adv
		Animation – a
		Comedy – c
		Crime – cr
		Drama – dr
		Family – fm
		Romance \ Horror – r
		Sci-fi – sf
		Thriller – t
	 */

	public static String prefix = "dr";
	public static String fileName = "C:\\Documents and Settings\\Ronen\\Desktop\\Trailers_15_3_200\\csv\\" +
						 "Ac.csv";// ok
//						  "a.csv";// ok
//						  "c.csv";// ok
//						 "cr.csv";// ok
//						 "dr.csv";// ok
//						 "fm.csv";// ok
//						  "r.csv";// ok
//						 "sf.csv";// ok
//					      "t.csv";// ok
	
	
	
	
	public static void main(String[] args) {
		
		prefix = fileName.substring(63, fileName.length()-4);
		//System.out.println(">"+prefix+"<");
		//if (true)return;
		
		ArrayList<String[]> fileArr =fillStringArrayFromFile(fileName);
		Trailers trailer = new Trailers();
		Genres g = null;
		
		for(int i=0;i<fileArr.size();i++){
			String[] temp = fileArr.get(i);
			int year = 0;
			int rating = 0;
			int id = 0;
			
			try{year  =Integer.parseInt(temp[2].trim());}catch(Exception e){}
			try{rating=Integer.parseInt(temp[9].trim());}catch(Exception e){}
			try{id    =Integer.parseInt(temp[0].trim());}catch(Exception e){}
			
			try {
				
				trailer.setTrailerId(prefix+(1000+id));
				trailer.setTitle(temp[1].trim());
				
				trailer.setYear(year);
				trailer.setYouTubeLink(temp[4].trim());
				trailer.setPlot(temp[5].trim());
				trailer.setStudio(temp[7].trim());
				trailer.setDirector(temp[8].trim());
				trailer.setYouTubeRating(rating);
				
				
				CommonFunctions.insertTrailer(trailer);
				
				String[] genres =  organizeString(temp[3]+","+temp[6],",");
				ArrayList<String> copiesChecker = new ArrayList<String>();
				for(int k=0;k<genres.length;k++){
					String t = genres[k].replaceAll(" ", "").toLowerCase();
					if (!t.equals("") && !t.equals("-")){
						
						if (copiesChecker.indexOf(t)>-1) continue;
						copiesChecker.add(t);
						
						g = CommonFunctions.selectGenreByName(t);
					if (g==null) 		
						g=CommonFunctions.insertNewGenre(t);
					
					Genres_of_trailers gt = new Genres_of_trailers();
					gt.setGenreId(g.getgenreId());
					gt.setTrailerId(prefix+(1000+id));
					 
					//CommonFunctions.insertGT(gt);
					}
				}
					
			} catch (SQLException e) {e.printStackTrace();}
		}
}
	
	public static ArrayList<String[]> fillStringArrayFromFile(String filename){

		ArrayList<String[]> fileArr=new ArrayList<String[]>();
		File  fin =new File(filename);

		FileReader fr=null;		
		BufferedReader br=null;


		try{
		
		fr = new FileReader(fin);
		br = new BufferedReader(fr);

		String temp="";
		while ((temp=br.readLine())!=null)	fileArr.add(organizeString(temp,";"));
		
		br.close();
		fr.close();
		
		System.out.println("Records written: <"+fileArr.size()+">");
		}  catch(Exception e){	e.printStackTrace(); return null; } 

		return fileArr;
	}
	
	
	public static String[] organizeString(String s,String delimiter){
	
		String[] arr = new String[200];
		int spos=0;
		int epos=0;
	 
		int counter=0;
	 
	 while (true){
		 
		 spos=s.indexOf(delimiter,spos+1);
		 epos=s.indexOf(delimiter,spos+1);
		 if (spos<0) break;
		     
		 if (counter==0){
			 arr[counter]=s.substring(0,spos);
			 counter++;
		   	}
		  
		  if (epos<0){
			  arr[counter]=s.substring(spos+1);
			  counter++;
			  break;
			  }  else   arr[counter]=s.substring(spos+1,epos);
	counter++; 
	}
 
 if (counter==0) return null;
 
 String[] returnArr=new String[counter];
 for(int i=0;i<counter;i++) returnArr[i]=arr[i];

 return(returnArr);
 }
}
