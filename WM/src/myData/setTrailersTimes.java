package myData;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

public class setTrailersTimes {

	public static boolean showOnlyZeroLengthTrailers = true;	
	public static String path   = "F:\\Youtube\\";
	public static void main(String[] args) {
		
		//System.out.println(getTrailerTime(path + "Ac1003.flv"));
		
		List TrailersList = null;
		try {TrailersList = CommonFunctions.getTrailersList();} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
		for(int i=0;i<TrailersList.size();i++){
			Trailers temp = (Trailers)TrailersList.get(i);
			temp.setDuration(getTrailerTime(path + temp.getTrailerId()));
			try {CommonFunctions.setDurationToTrailer(temp);
				if (!showOnlyZeroLengthTrailers)
					System.out.println("Trailer id:"+temp.getTrailerId()+" duration : "+temp.getDuration());
				else if (temp.getDuration() == 0)
					System.out.println("Trailer id:"+temp.getTrailerId()+" duration : "+temp.getDuration());
				
			} catch (SQLException e) {
				 e.printStackTrace();
				System.out.println("Trailer id : "+temp.getTrailerId()+" ERROR ");
			}
		}
}
	public static double getTrailerTime(String fileName){
		byte[] bytes = new byte[8];
		byte[] bytes_reverse = new byte[8];
		
		bytes_reverse[0] = (byte) 'd';
		bytes_reverse[1] = (byte) 'u';
		bytes_reverse[2] = (byte) 'r';
		bytes_reverse[3] = (byte) 'a';
		bytes_reverse[4] = (byte) 't';
		bytes_reverse[5] = (byte) 'i';
		bytes_reverse[6] = (byte) 'o';
		bytes_reverse[7] = (byte) 'n';
		
		InputStream is = null;
		DataInputStream dis = null;
		
		try {
		   is = new FileInputStream(new File(fileName));
		   dis = new DataInputStream(is);
		   
		   is.skip(44);
		   is.read(bytes,0,8);
		   		
		   		if (!isEqualsArrays(bytes,bytes_reverse)){
		   			dis.close();
		   			is.close();
		   			throw new Exception("There is no data about duration in flv file!");
		   		}
		   		
		   is.skip(1);		
		   is.read(bytes,0,8);
		   dis.close();
		   is.close();
		} catch (Exception e) {
			System.out.print(e.getMessage() + " ->> ");
			return(0);
			}
		
		for(int i=7;i>-1;i--) bytes_reverse[7-i]=bytes[i];
		return(arr2double(bytes_reverse,0));
	}
	
	static boolean isEqualsArrays(byte[] a, byte[] b){
		if (a.length!=b.length) return (false);
		for(int i=0;i<a.length;i++)
			if (a[i]!=b[i]) return (false);
		return(true);
	}
	
	public static double arr2double (byte[] arr, int start) {
		int i = 0;
		int len = 8;
		int cnt = 0;
		byte[] tmp = new byte[len];
		for (i = start; i < (start + len); i++) {
			tmp[cnt] = arr[i];
			cnt++;
		}
		long accum = 0;
		i = 0;
		for ( int shiftBy = 0; shiftBy < 64; shiftBy += 8 ) {
			accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
			i++;
		}
		return Double.longBitsToDouble(accum);
	}
	
}
