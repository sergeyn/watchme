package dbmappings;
import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class DBconnect {
	private static SqlMapClient sqlMapper;
	static {
		try {
		      Reader reader = Resources.getResourceAsReader("dbmappings//SqlMapConfig.xml");
		      sqlMapper = SqlMapClientBuilder.buildSqlMapClient(reader);
		      reader.close(); 
		} catch (IOException e) {throw new RuntimeException("Something bad happened while building the SqlMapClient instance." + e, e);}
	}
	
	
}
