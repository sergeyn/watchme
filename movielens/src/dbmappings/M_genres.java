package dbmappings;

public class M_genres {
	private int genre_id=0;
	private String genre_name;
	
//constructors
	public M_genres() {
		super();
	}
	public M_genres(int genre_id, String genre_name) {
		super();
		this.genre_id = genre_id;
		this.genre_name = genre_name;
	}
//getters/setters	
	public int getGenre_id() {
		return genre_id;
	}
	public void setGenre_id(int genre_id) {
		this.genre_id = genre_id;
	}
	public String getGenre_name() {
		return genre_name;
	}
	public void setGenre_name(String genre_name) {
		this.genre_name = genre_name;
	}
	
	

}
