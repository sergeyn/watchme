package dbmappings;

public class M_imdb_ratings {
	private int movie_id=0;
	private double imdb_rating=0.0;
	
//constructors
	public M_imdb_ratings() {
		super();
	}
	public M_imdb_ratings(int movie_id, double imdb_rating) {
		super();
		this.movie_id = movie_id;
		this.imdb_rating = imdb_rating;
	}
//getters/setters
	public int getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
	public double getImdb_rating() {
		return imdb_rating;
	}
	public void setImdb_rating(double imdb_rating) {
		this.imdb_rating = imdb_rating;
	}
}
