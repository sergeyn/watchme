package dbmappings;

public class M_movies {
	private int movie_id=0;
	private String movie_name = "";
	private String release_date = "0000-00-00";
	private String video_release_date = "0000-00-00";
	private String imdb_url = "http://";
	private int is_unknown=0;
	private int is_action=0;
	private int is_adventure=0; 
	private int is_animation=0;  
	private int is_children=0;
	private int is_comedy=0;
	private int is_crime=0;
	private int is_documentary=0; 
	private int is_drama=0;
	private int is_fantasy=0; 
	private int is_filmnoir=0; 
	private int is_horror=0;
	private int is_musical=0; 
	private int is_mystery=0; 
	private int is_romance=0; 
	private int is_scifi=0;  
	private int is_thriller=0;
	private int is_war=0; 
	private int is_western=0;  
	private double movie_db_average=0.0;

//constructors	
	public M_movies() {
		super();
	}

	public M_movies(int movie_id, String movie_name, String release_date,
			String video_release_date, String imdb_url, int is_unknown,
			int is_action, int is_adventure, int is_animation, int is_children,
			int is_comedy, int is_crime, int is_documentary, int is_drama,
			int is_fantasy, int is_filmnoir, int is_horror, int is_musical,
			int is_mystery, int is_romance, int is_scifi, int is_thriller,
			int is_war, int is_western, double movie_db_average) {
		super();
		this.movie_id = movie_id;
		this.movie_name = movie_name;
		this.release_date = release_date;
		this.video_release_date = video_release_date;
		this.imdb_url = imdb_url;
		this.is_unknown = is_unknown;
		this.is_action = is_action;
		this.is_adventure = is_adventure;
		this.is_animation = is_animation;
		this.is_children = is_children;
		this.is_comedy = is_comedy;
		this.is_crime = is_crime;
		this.is_documentary = is_documentary;
		this.is_drama = is_drama;
		this.is_fantasy = is_fantasy;
		this.is_filmnoir = is_filmnoir;
		this.is_horror = is_horror;
		this.is_musical = is_musical;
		this.is_mystery = is_mystery;
		this.is_romance = is_romance;
		this.is_scifi = is_scifi;
		this.is_thriller = is_thriller;
		this.is_war = is_war;
		this.is_western = is_western;
		this.movie_db_average = movie_db_average;
	}

	public int getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}

	public String getMovie_name() {
		return movie_name;
	}

	public void setMovie_name(String movie_name) {
		this.movie_name = movie_name;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getVideo_release_date() {
		return video_release_date;
	}

	public void setVideo_release_date(String video_release_date) {
		this.video_release_date = video_release_date;
	}

	public String getImdb_url() {
		return imdb_url;
	}

	public void setImdb_url(String imdb_url) {
		this.imdb_url = imdb_url;
	}

	public int getIs_unknown() {
		return is_unknown;
	}

	public void setIs_unknown(int is_unknown) {
		this.is_unknown = is_unknown;
	}

	public int getIs_action() {
		return is_action;
	}

	public void setIs_action(int is_action) {
		this.is_action = is_action;
	}

	public int getIs_adventure() {
		return is_adventure;
	}

	public void setIs_adventure(int is_adventure) {
		this.is_adventure = is_adventure;
	}

	public int getIs_animation() {
		return is_animation;
	}

	public void setIs_animation(int is_animation) {
		this.is_animation = is_animation;
	}

	public int getIs_children() {
		return is_children;
	}

	public void setIs_children(int is_children) {
		this.is_children = is_children;
	}

	public int getIs_comedy() {
		return is_comedy;
	}

	public void setIs_comedy(int is_comedy) {
		this.is_comedy = is_comedy;
	}

	public int getIs_crime() {
		return is_crime;
	}

	public void setIs_crime(int is_crime) {
		this.is_crime = is_crime;
	}

	public int getIs_documentary() {
		return is_documentary;
	}

	public void setIs_documentary(int is_documentary) {
		this.is_documentary = is_documentary;
	}

	public int getIs_drama() {
		return is_drama;
	}

	public void setIs_drama(int is_drama) {
		this.is_drama = is_drama;
	}

	public int getIs_fantasy() {
		return is_fantasy;
	}

	public void setIs_fantasy(int is_fantasy) {
		this.is_fantasy = is_fantasy;
	}

	public int getIs_filmnoir() {
		return is_filmnoir;
	}

	public void setIs_filmnoir(int is_filmnoir) {
		this.is_filmnoir = is_filmnoir;
	}

	public int getIs_horror() {
		return is_horror;
	}

	public void setIs_horror(int is_horror) {
		this.is_horror = is_horror;
	}

	public int getIs_musical() {
		return is_musical;
	}

	public void setIs_musical(int is_musical) {
		this.is_musical = is_musical;
	}

	public int getIs_mystery() {
		return is_mystery;
	}

	public void setIs_mystery(int is_mystery) {
		this.is_mystery = is_mystery;
	}

	public int getIs_romance() {
		return is_romance;
	}

	public void setIs_romance(int is_romance) {
		this.is_romance = is_romance;
	}

	public int getIs_scifi() {
		return is_scifi;
	}

	public void setIs_scifi(int is_scifi) {
		this.is_scifi = is_scifi;
	}

	public int getIs_thriller() {
		return is_thriller;
	}

	public void setIs_thriller(int is_thriller) {
		this.is_thriller = is_thriller;
	}

	public int getIs_war() {
		return is_war;
	}

	public void setIs_war(int is_war) {
		this.is_war = is_war;
	}

	public int getIs_western() {
		return is_western;
	}

	public void setIs_western(int is_western) {
		this.is_western = is_western;
	}

	public double getMovie_db_average() {
		return movie_db_average;
	}

	public void setMovie_db_average(double movie_db_average) {
		this.movie_db_average = movie_db_average;
	} 

}
