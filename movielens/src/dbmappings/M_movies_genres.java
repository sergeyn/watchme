package dbmappings;

public class M_movies_genres {
	private int movie_id = 0;
	private String genre_name ="";
	
	public M_movies_genres() {
		super();
	}

	public M_movies_genres(int movie_id, String genre_name) {
		super();
		this.movie_id = movie_id;
		this.genre_name = genre_name;
	}

	public int getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}

	public String getGenre_name() {
		return genre_name;
	}

	public void setGenre_name(String genre_name) {
		this.genre_name = genre_name;
	}
	
	

}
