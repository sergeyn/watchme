package dbmappings;

public class M_occupation {
	private int occupation_id=0;
	private String occupation_name="none";

//constructors
public M_occupation() {
		super();
	}
public M_occupation(int occupation_id, String occupation_name) {
		super();
		this.occupation_id = occupation_id;
		this.occupation_name = occupation_name;
	}

//getters/setters
	public int getOccupation_id() {
		return occupation_id;
	}
	public void setOccupation_id(int occupation_id) {
		this.occupation_id = occupation_id;
	}
	public String getOccupation_name() {
		return occupation_name;
	}
	public void setOccupation_name(String occupation_name) {
		this.occupation_name = occupation_name;
	}

}
