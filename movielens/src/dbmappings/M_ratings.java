package dbmappings;

public class M_ratings {
	private int user_id=0;
	private int movie_id=0;
	private int user_rating=0;
	private String rating_time="00:00:00";
	private String rating_date="0000-00-00";

//constructors	
public M_ratings(int user_id, int movie_id, int user_rating,
			String rating_time, String rating_date) {
		super();
		this.user_id = user_id;
		this.movie_id = movie_id;
		this.user_rating = user_rating;
		this.rating_time = rating_time;
		this.rating_date = rating_date;
	}

public M_ratings() {
	super();
}

//setters/getters	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
	public int getUser_rating() {
		return user_rating;
	}
	public void setUser_rating(int user_rating) {
		this.user_rating = user_rating;
	}
	public String getRating_time() {
		return rating_time;
	}
	public void setRating_time(String rating_time) {
		this.rating_time = rating_time;
	}
	public String getRating_date() {
		return rating_date;
	}
	public void setRating_date(String rating_date) {
		this.rating_date = rating_date;
	}

}
