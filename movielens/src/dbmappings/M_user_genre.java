package dbmappings;

public class M_user_genre {
	private int user_id =0;
	private String genre_name ="";
	private double genre_weight=0.0;
	private double genre_average=0.0;
	private int positive_sequence=0;
	private int negative_sequence=0;
	private String isCurrent = "";
	
//constructors
public M_user_genre() {
		super();
	}

public M_user_genre(int user_id, String genre_name, double genre_weight,
			double genre_average, int positive_sequence, int negative_sequence,
			String isCurrent) {
		super();
		this.user_id = user_id;
		this.genre_name = genre_name;
		this.genre_weight = genre_weight;
		this.genre_average = genre_average;
		this.positive_sequence = positive_sequence;
		this.negative_sequence = negative_sequence;
		this.isCurrent = isCurrent;
	}

//getters/setters	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getGenre_name() {
		return genre_name;
	}
	public void setGenre_name(String genre_name) {
		this.genre_name = genre_name;
	}
	public double getGenre_weight() {
		return genre_weight;
	}
	public void setGenre_weight(double genre_weight) {
		this.genre_weight = genre_weight;
	}
	public double getGenre_average() {
		return genre_average;
	}
	public void setGenre_average(double genre_average) {
		this.genre_average = genre_average;
	}
	public int getPositive_sequence() {
		return positive_sequence;
	}
	public void setPositive_sequence(int positive_sequence) {
		this.positive_sequence = positive_sequence;
	}
	public int getNegative_sequence() {
		return negative_sequence;
	}
	public void setNegative_sequence(int negative_sequence) {
		this.negative_sequence = negative_sequence;
	}
	public String getIsCurrent() {
		return isCurrent;
	}
	public void setIsCurrent(String isCurrent) {
		this.isCurrent = isCurrent;
	}

}
