package dbmappings;

public class M_users {
	private int user_id = 0;
	private int age = 0;
	private int gender_code = -1;
	private int occupation_code = -1;
	private int zip_code = 0;

//constructors - default	
public M_users() {
		super();
	}
//constructor using fields
public M_users(int user_id, int age, int gender_code, int occupation_code,
			int zip_code) {
		super();
		this.user_id = user_id;
		this.age = age;
		this.gender_code = gender_code;
		this.occupation_code = occupation_code;
		this.zip_code = zip_code;
	}
	//getters/setters	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getGender_code() {
		return gender_code;
	}
	public void setGender_code(int gender_code) {
		this.gender_code = gender_code;
	}
	public int getOccupation_code() {
		return occupation_code;
	}
	public void setOccupation_code(int occupation_code) {
		this.occupation_code = occupation_code;
	}
	public int getZip_code() {
		return zip_code;
	}
	public void setZip_code(int zip_code) {
		this.zip_code = zip_code;
	}

}
