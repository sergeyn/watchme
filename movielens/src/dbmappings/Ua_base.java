package dbmappings;

public class Ua_base {
	
	private int user_id = 0;
	private int movie_id = 0;
	private int user_movie_rating =0;
	public int getUser_id() {
		return user_id;
	}
	
//	default constructor
	public Ua_base() {
		super();
	}
//	constructor with parameters
	public Ua_base(int user_id, int movie_id, int user_movie_rating) {
		super();
		this.user_id = user_id;
		this.movie_id = movie_id;
		this.user_movie_rating = user_movie_rating;
	}
//getters/setters
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
	public int getUser_movie_rating() {
		return user_movie_rating;
	}
	public void setUser_movie_rating(int user_movie_rating) {
		this.user_movie_rating = user_movie_rating;
	}

}
