package dbmappings;

public class Wm_demographics {
	
	private int genreId =8;
	private String genreName= "";
	private String gender = "";
	private int age = 0;
	private String occupation = "";
	private String shouldRecommend = "";
	private double combinationSD = 0;
	private double combinationMean = 0;
	/**
	 * @return the combinationMean
	 */
	public double getCombinationMean() {
		return combinationMean;
	}
	/**
	 * @param combinationMean the combinationMean to set
	 */
	public void setCombinationMean(double combinationMean) {
		this.combinationMean = combinationMean;
	}
	/**
	 * @return the combinationSd
	 */

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the genreName
	 */
	public String getGenreName() {
		return genreName;
	}
	/**
	 * @param genreName the genreName to set
	 */
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * @return the shouldRecommend
	 */
	public String getShouldRecommend() {
		return shouldRecommend;
	}
	/**
	 * @param shouldRecommend the shouldRecommend to set
	 */
	public void setShouldRecommend(String shouldRecommend) {
		this.shouldRecommend = shouldRecommend;
	}
	/**
	 * @return the combinationSD
	 */
	public double getCombinationSD() {
		return combinationSD;
	}
	/**
	 * @param combinationSD the combinationSD to set
	 */
	public void setCombinationSD(double combinationSD) {
		this.combinationSD = combinationSD;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	
}

